# IS-ENES3 C4I Storybook

## Starting developing

Prerequisites: node version specified in `.nvmrc`. Node versions can be managed with nvm: https://github.com/nvm-sh/nvm/blob/master/README.md. After installation of nvm do `nvm install`, to install the version in `.nvmrc`.

1. Clone this repository
2. Install visualstudio code and open this directory
3. The following plugins for vscode are recommended: ESLint, Prettier
4. (for every new terminal) run `nvm use` to start using the specified version
5. Run `npm install -g yarn` to install yarn
4. Install npm dependencies with `yarn install`
5. Run the storybook with `yarn run storybook`

## To develop the main app

`yarn run start` and visit http://localhost:3000

## To build app as static JS/CSS version:

`yarn run build`

After this the static files are available in the dist folder. To serve the static version, you can do `npx serve dist`

# Setup prettier with vscode

1. Download the prettier extension for vscode (https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. Update your settings: File->Preferences->Settings and open the setting.json as text
3. Paste the code below at the right place in settings.json

```
   "editor.formatOnSave": true,
   "[javascript]": {
     "editor.formatOnSave": true
   },
   "[javascriptreact]": {
     "editor.formatOnSave": true
   },
   "[typescript]": {
     "editor.formatOnSave": true
   },
   "[typescriptreact]": {
     "editor.formatOnSave": true
   },
```

To run it on all code do `yarn run prettier`

# Docker

```
docker build -t c4i-storybook .
```

```
docker run \
  -e C4INGINXESGFSEARCH=http://localhost/esgfsearch \
  -e C4IDOCS=https://c4i-frontend-content-master-temp.s3-eu-west-1.amazonaws.com \
  -e C4IFRONTENDPATH=c4i-storybook/ \
  -p 80:80 \
  -it c4i-storybook

```
