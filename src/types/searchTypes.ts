/* Variables, Frequencies, experiments */
export interface FacetType {
  id: string;
  gridMd?: boolean | 'auto' | 2 | 4 | 1 | 6 | 3 | 5 | 7 | 8 | 9 | 10 | 11 | 12;
  title: string;
  config: FacetConfiguration[] /* temperature, pressure, wind, radiation */;
}

/* Temperature, min temp, max temp, wind pressure, ... */
export interface FacetFieldType {
  id: string;
  title: string;
}

/* Configurations for CMIP5, CMIP6, Cordex */
export enum PROJECTNAME {
  CMIP5 = 'CMIP5',
  CMIP6 = 'CMIP6',
  CORDEX = 'CORDEX',
}
export interface ProjectType {
  title: PROJECTNAME;
  facetName: string;
  disabled: boolean;
  description: string;
  thumbnail: string;
  mainFacets: FacetType[]; // Variables, Frequencies, experiments
}

export interface FacetDescription {
  [key: string]: {
    title: string;
  };
}

/* Configuration for the facets, like for temperature it will contain min max average temperature */
export interface FacetConfiguration {
  title: string;
  description: string;
  titleBGColor: string;
  icon?: string;
  tooltip?: string;
  items?: FacetFieldType[]; // temperature, radiation, pressure,
  descriptions?: FacetDescription; // temperature, radiation, pressure
}

export interface QueryItem {
  facetId: string;
  fieldId: string;
  include: boolean;
}

export interface SearchQuery {
  selectedItems: QueryItem[];
}

export interface NotebookProperties {
  processingStatus: NotebookStatus;
  redirectUrl: string;
  dialogOpen: boolean;
  output: OUTPUT;
  message?: string;
  error?: string;
  workerNumber?: number;
}

export type NotebookStatus =
  | Status.IDLE
  | Status.GENERATING
  | Status.ERROR
  | Status.READY;

export enum Status {
  IDLE = 'IDLE',
  GENERATING = 'GENERATING',
  ERROR = 'ERROR',
  READY = 'READY',
}

export type Output = OUTPUT.FILELIST | OUTPUT.NOTEBOOK | OUTPUT.NA;
export enum OUTPUT {
  FILELIST = 'FILELIST',
  NOTEBOOK = 'NOTEBOOK',
  NA = 'NA',
}

export type Operation = OPERATION.DOWNLOAD | OPERATION.SUBSETTING;
export enum OPERATION {
  DOWNLOAD = 'DOWNLOAD',
  SUBSETTING = 'SUBSETTING',
}

export type Subsetting = SUBSETTING.OPENDAP | SUBSETTING.ROOK | SUBSETTING.NONE;
export enum SUBSETTING {
  OPENDAP = 'OPENDAP',
  ROOK = 'ROOK',
  NONE = 'NONE',
}

export type FacetDoc = {
  title: string;
  id: string;
  number_of_files: number;
  version: string;
  size: number;
  citation_url: string[];
  instance_id: string;
  data_node: string;
  datetime_start?: string;
  datetime_stop?: string;
  variable?: string;
  variable_id?: string;
};
export type FacetResponse = {
  response: {
    docs: FacetDoc[];
  };
};

export type ESGFDataset = {
  title: string;
  id: string;
  fileNumber: number;
  version: string;
  size: number;
  citationUrl: string[];
  instanceId: string;
  dataNode: string;
  startTime: string;
  endTime: string;
  variable?: string;
};

export type DatasetResponse = {
  response: {
    docs: DocumentDoc[];
  };
};

export type DocumentDoc = {
  instance_id: string;
  size: number;
  dataNode: string;
  url: string[];
  checksum: string[];
  checksum_type: string[];
  replica: string;
  title: string;
  parent?: string;
  variable?: string;
  variable_id?: string;
};

export type Notebook = {
  notebookStatus: Status;
  volumes: {
    name: string;
    size: number;
  }[];
};

export type DataSizeResponse = {
  response: {
    docs: FacetDoc[];
  };
};

export type Staginghistory = {
  content: {
    stage: Stage;
  };
};

export type FileInfo = {
  filename: string;
  state: string;
  id: string;
  sourceUrl: string;
};

export type Content = {
  stageNumber: string;
  stagePath: string;
  stageCreateTime: string;
  files: FileInfo[];
};

export type Stage = {
  content: Stage[];
  created: string;
  format: string;
  last_modified: string;
  mimetype: string;
  name: string;
  path: string;
  size: number;
  type: string;
  writable: false;
};

export type NotebookSizeProperties = {
  currentSize: number;
  currentStatus: string;
};

export type Graph = {
  'swirrl:serviceId': string;
  'swirrl:sessionId': string;
  '@type': string[];
  person: string;
  '@id': string;
  'prov:endedAtTime': string;
  'prov:startedAtTime': string;
};

export type Activity = {
  '@graph': Graph[];
  '@context': Record<string, string>;
};
