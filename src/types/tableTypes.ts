import {
  Operation,
  ESGFDataset,
  PROJECTNAME,
  SearchQuery,
} from './searchTypes';

export type Order = 'asc' | 'desc';

export type Data = ESGFDataset & {
  checked: boolean;
  projectName: PROJECTNAME;
};

export interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
  badge: boolean;
  description: string;
}

export interface TableProps {
  orderBy: string;
  order: Order;
  classes;
  rows: Data[];
  rowsPerPage: number;
  page: number;
  operation: Operation;
  dense: boolean;
  handleClick?: (name: string) => void;
  onRequestSort?: (property: keyof Data) => void;
  searchQuery: SearchQuery;
  nodes?: string[];
}

export interface EnhancedTableToolbarProps {
  numSelected: number;
}
