export type SubsetParameters = {
  name: string;
  message: string;
  urllist_filename?: string;
  urllist?: {
    class: string;
    path: string;
  };
  minlat: string;
  maxlat: string;
  minlon: string;
  maxlon: string;
  start_time: string;
  stop_time: string;
  runId?: string;
  variable?: string;
  datasetid?: string;
};

export type SubsetTemporalParameters = {
  start_time: string;
  stop_time: string;
};
export type SubsetSpatialParameters = {
  minlat: string;
  maxlat: string;
  minlon: string;
  maxlon: string;
};

export const OPENDAPMETA = {
  name: 'OpenDAP downloader',
  message: 'Download ESGF data via OpenDAP.',
  urllist_filename: 'urllist.txt',
  urllist: {
    class: 'File',
    path: './urllist.txt',
  },
};
export const ROOKMETA = {
  name: 'ROOK WPS downloader',
  message: 'Download ESGF data via ROOK WPS.',
  runId: 'placeholder',
};

export const NODATE = { start_time: '', stop_time: '' };
export const DEFAULTDATE = {
  start_time: '2000-01-01',
  stop_time: '2020-12-31',
};
export const NOCOORDINATES = {
  minlat: '9999',
  maxlat: '9999',
  minlon: '9999',
  maxlon: '9999',
};
export const DEFAULTCOORDINATES = {
  minlat: '-90.0',
  maxlat: '90.0',
  minlon: '-180.0',
  maxlon: '180.0',
};

export type SpatialParameterHook = {
  spatialParameters: SubsetSpatialParameters;
  setSpatialParameter: (spatialParameters: SubsetSpatialParameters) => void;
};
export type TemporalParameterHook = {
  temporalParameters: SubsetTemporalParameters;
  setTemporalParameter: (temporalParameters: SubsetTemporalParameters) => void;
};

export type RookOperationHook = {
  rookOperation: string;
  setRookOperation: (facet: string) => void;
};

export type CombinedParameterHooks = SpatialParameterHook &
  TemporalParameterHook &
  RookOperationHook;

export const COMPLETE = 100;

export type Dataset = {
  title?: string;
  id: string;
  numberOfFiles?: number;
  fileSize?: number;
};

export type OpenidUser = {
  email: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  groups: string[];
  name: string;
  preferred_username: string;
  sub: string;
  registered: boolean;
  // Only if a notebook has been created:
  notebookId?: string;
  notebookURL?: string;
  sessionId?: string;
};
