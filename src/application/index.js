import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from '../components/App/App';
import store from './initializeReduxStore';

const root = ReactDOM.createRoot(
  document.getElementById('app')
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
