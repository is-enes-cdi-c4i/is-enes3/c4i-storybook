import {
  mdiThermometer,
  mdiWeatherPouring,
  mdiWeatherWindy,
  mdiWeatherTornado,
  mdiWeatherSunny,
  mdiWeatherHurricane,
  mdiHairDryer,
} from '@mdi/js';
import { FacetConfiguration } from '../types/searchTypes';

import { CMIP5ModelList } from './customSelections/CMIP5_models';
import {
  CMIP5ExperimentList_rcp,
  CMIP5ExperimentList_historical,
} from './customSelections/CMIP5_experiments';
import { CMIP5EnsembleList } from './customSelections/members';

const mapCollection = (collection: string[]) =>
  collection.map((item: string) => ({
    id: item,
    title: item,
  }));

export const cmip5Variables: FacetConfiguration[] = [
  {
    title: 'Temperature',
    description: 'Parameter title',
    titleBGColor: '#f58d00',
    icon: mdiThermometer,
    items: [
      {
        id: 'tas',
        title: 'Temperature',
      },
      {
        id: 'tasmin',
        title: 'Min. Temperature',
      },
      {
        id: 'tasmax',
        title: 'Max. Temperature',
      },
      {
        id: 'ta',
        title: 'Air temperature',
      },
    ],
  },
  {
    title: 'Precipitation',
    description: 'Parameter title',
    titleBGColor: '#00a8ec',
    icon: mdiWeatherPouring,
    items: [
      {
        id: 'pr',
        title: 'Precipitation',
      },
      {
        id: 'prc',
        title: 'Convective precipitation',
      },
      {
        id: 'prsn',
        title: 'Snow',
      },
    ],
  },
  {
    title: 'Humidity',
    description: 'Parameter title',
    titleBGColor: '#4CAF50',
    icon: mdiWeatherTornado,
    items: [
      {
        id: 'huss',
        title: 'Specific humidity',
      },
      {
        id: 'hurs',
        title: 'Rel. Humidity',
      },
      {
        id: 'rhsmin',
        title: 'Min, Rel. Humidity',
      },
      {
        id: 'rhs',
        title: 'Rel. Humidity',
      },
      {
        id: 'hus',
        title: 'Spec. Humidity',
      },
      {
        id: 'hur',
        title: 'Rel. Humidity',
      },
    ],
  },
  {
    title: 'Wind',
    description: 'Parameter title',
    titleBGColor: '#AEB404',
    icon: mdiWeatherWindy,
    items: [
      {
        id: 'sfcWind',
        title: 'Wind',
      },
      {
        id: 'sfcWindmax',
        title: 'Max Wind',
      },
      {
        id: 'uas',
        title: 'Eastward wind',
      },
      {
        id: 'vas',
        title: 'Northward wind',
      },
    ],
  },
  {
    title: 'Radiation',
    description: 'Parameter title',
    titleBGColor: '#e35c5c',
    icon: mdiWeatherSunny,
    items: [
      {
        id: 'rsds',
        title: 'SW Radiation Dn',
      },
      {
        id: 'rsus',
        title: 'SW Radiation Up',
      },
      {
        id: 'rlds',
        title: 'LW Radiation Dn',
      },
      {
        id: 'rlus',
        title: 'LW Radiation Up',
      },
      {
        id: 'rsdsdiff',
        title: 'Diff. Radiation',
      },
      {
        id: 'clt',
        title: 'Cloud',
      },
    ],
  },
  {
    title: 'Pressure',
    description: 'Parameter title',
    titleBGColor: '#9268FF',
    icon: mdiWeatherHurricane,
    items: [
      {
        id: 'ps',
        title: 'Pressure',
      },
      {
        id: 'psl',
        title: 'Sea level pressure',
      },
      {
        id: 'pfull',
        title: 'Pressure',
      },
    ],
  },
  {
    title: 'Evaporation',
    description: 'Parameter title',
    titleBGColor: '#dda606',
    icon: mdiHairDryer,
    items: [
      {
        id: 'evspsbl',
        title: 'Act. Evap.',
      },
      {
        id: 'evpsblpot',
        title: 'Pot. Evap.',
      },
      {
        id: 'evspsblsoi',
        title: 'Sol Evap.',
      },
      {
        id: 'evspsblveg',
        title: 'Canopy Evap.',
      },
    ],
  },
];

export const cmip5Frequencies: FacetConfiguration[] = [
  {
    title: '3hr',
    description: 'Instantaneous states every 3 hours',
    tooltip:
      'can be used as sub-daily forcing data, or for advanced change assessments',
    titleBGColor: '#9268FF',
    items: [
      {
        id: '3hr',
        title: 'Instantaneous states every 3 hours',
      },
    ],
  },
  {
    title: '6hr',
    description: 'Instantaneous states every 6 hours',
    tooltip: 'mostly used as forcing data, or for advanced change assessments',
    titleBGColor: '#f58d00',
    items: [
      {
        id: '6hr',
        title: 'Instantaneous states every 6 hours',
      },
    ],
  },
  {
    title: 'daily',
    description: 'Daily averages',
    tooltip: 'mostly used as forcing data, or for advanced change assessments',
    titleBGColor: '#d32c2c',
    items: [
      {
        id: 'day',
        title: 'Daily averages',
      },
    ],
  },
  {
    title: 'mon',
    description: 'Monthly averages',
    tooltip: 'can be used for basic change assessments',
    titleBGColor: '#6b6b6b',
    items: [
      {
        id: 'mon',
        title: 'Monthly averages',
      },
    ],
  },
  {
    title: 'yearly',
    description: 'Annual averages',
    tooltip: 'can be for basic change assessments',
    titleBGColor: '#ffc808',
    items: [
      {
        id: 'yr',
        title: 'Annual averages',
      },
    ],
  },
];

export const cmip5SourceIds: FacetConfiguration[] = [
  {
    title: 'Model',
    description: 'Model',
    tooltip: 'Model',
    titleBGColor: '#ffc808',
    items: mapCollection(CMIP5ModelList),
    descriptions: {},
  },
];
export const cmip5Experiments: FacetConfiguration[] = [
  {
    title: 'Historical',
    description: 'Historical',
    tooltip: 'Historical',
    titleBGColor: '#f58d00',
    items: mapCollection(CMIP5ExperimentList_historical),
    descriptions: {},
  },
  {
    title: 'RCP',
    description: 'RCP',
    tooltip: 'RCP',
    titleBGColor: '#00a8ec',
    items: mapCollection(CMIP5ExperimentList_rcp),
    descriptions: {},
  },
];

export const ensemble = CMIP5EnsembleList.map((item) => ({
  id: item,
  title: item,
}));

export const cmip5EnsembleIds: FacetConfiguration[] = [
  {
    title: 'Ensemble',
    description: 'Ensemble',
    tooltip: 'Ensemble',
    titleBGColor: '#9008ff',
    items: ensemble,
    descriptions: {},
  },
];
