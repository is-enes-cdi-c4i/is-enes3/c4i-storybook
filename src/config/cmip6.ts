import {
  mdiThermometer,
  mdiWeatherPouring,
  mdiWeatherWindy,
  mdiWeatherTornado,
  mdiWeatherSunny,
  mdiWeatherHurricane,
  mdiHairDryer,
} from '@mdi/js';
import { FacetConfiguration } from '../types/searchTypes';
import CMIP6SourceID from './CMIP6_CVs-master/CMIP6_source_id.json';
import CMIP6ExperimentID from './CMIP6_CVs-master/CMIP6_experiment_id.json';
import { CMIP6memberList } from './customSelections/members';

export const cmip6Variables: FacetConfiguration[] = [
  {
    title: 'Temperature',
    description: 'Parameter title',
    titleBGColor: '#f58d00',
    icon: mdiThermometer,
    items: [
      {
        id: 'tas',
        title: 'Temperature',
      },
      {
        id: 'tasmin',
        title: 'Min. Temperature',
      },
      {
        id: 'tasmax',
        title: 'Max. Temperature',
      },
      {
        id: 'ta',
        title: 'Air temperature',
      },
    ],
  },
  {
    title: 'Precipitation',
    description: 'Parameter title',
    titleBGColor: '#00a8ec',
    icon: mdiWeatherPouring,
    items: [
      {
        id: 'pr',
        title: 'Precipitation',
      },
      {
        id: 'prc',
        title: 'Convective precipitation',
      },
      {
        id: 'prsn',
        title: 'Snow',
      },
    ],
  },
  {
    title: 'Humidity',
    description: 'Parameter title',
    titleBGColor: '#4CAF50',
    icon: mdiWeatherTornado,
    items: [
      {
        id: 'huss',
        title: 'Specific humidity',
      },
      {
        id: 'hurs',
        title: 'Rel. Humidity',
      },
      {
        id: 'rhsmin',
        title: 'Min, Rel. Humidity',
      },
      {
        id: 'rhs',
        title: 'Rel. Humidity',
      },
      {
        id: 'hus',
        title: 'Spec. Humidity',
      },
      {
        id: 'hur',
        title: 'Rel. Humidity',
      },
    ],
  },
  {
    title: 'Wind',
    description: 'Parameter title',
    titleBGColor: '#AEB404',
    icon: mdiWeatherWindy,
    items: [
      {
        id: 'sfcWind',
        title: 'Wind',
      },
      {
        id: 'sfcWindmax',
        title: 'Max Wind',
      },
      {
        id: 'uas',
        title: 'Eastward wind',
      },
      {
        id: 'vas',
        title: 'Northward wind',
      },
    ],
  },
  {
    title: 'Radiation',
    description: 'Parameter title',
    titleBGColor: '#e35c5c',
    icon: mdiWeatherSunny,
    items: [
      {
        id: 'rsds',
        title: 'SW Radiation Dn',
      },
      {
        id: 'rsus',
        title: 'SW Radiation Up',
      },
      {
        id: 'rlds',
        title: 'LW Radiation Dn',
      },
      {
        id: 'rlus',
        title: 'LW Radiation Up',
      },
      {
        id: 'rsdsdiff',
        title: 'Diff. Radiation',
      },
      {
        id: 'clt',
        title: 'Cloud',
      },
    ],
  },
  {
    title: 'Pressure',
    description: 'Parameter title',
    titleBGColor: '#9268FF',
    icon: mdiWeatherHurricane,
    items: [
      {
        id: 'ps',
        title: 'Pressure',
      },
      {
        id: 'psl',
        title: 'Sea level pressure',
      },
      {
        id: 'pfull',
        title: 'Pressure',
      },
    ],
  },
  {
    title: 'Evaporation',
    description: 'Parameter title',
    titleBGColor: '#dda606',
    icon: mdiHairDryer,
    items: [
      {
        id: 'evspsbl',
        title: 'Act. Evap.',
      },
      {
        id: 'evpsblpot',
        title: 'Pot. Evap.',
      },
      {
        id: 'evspsblsoi',
        title: 'Sol Evap.',
      },
      {
        id: 'evspsblveg',
        title: 'Canopy Evap.',
      },
    ],
  },
];

export const cmip6Frequencies: FacetConfiguration[] = [
  {
    title: '3hr',
    description: 'Instantaneous states every 3 hours',
    tooltip:
      'can be used as sub-daily forcing data, or for advanced change assessments',
    titleBGColor: '#9268FF',
    items: [
      {
        id: '3hr',
        title: 'Instantaneous states every 3 hours',
      },
    ],
  },
  {
    title: '6hr',
    description: 'Instantaneous states every 6 hours',
    tooltip: 'mostly used as forcing data, or for advanced change assessments',
    titleBGColor: '#f58d00',
    items: [
      {
        id: '6hr',
        title: 'Instantaneous states every 6 hours',
      },
    ],
  },
  {
    title: 'daily',
    description: 'Daily averages',
    tooltip: 'mostly used as forcing data, or for advanced change assessments',
    titleBGColor: '#d32c2c',
    items: [
      {
        id: 'day',
        title: 'Daily averages',
      },
    ],
  },
  {
    title: 'mon',
    description: 'Monthly averages',
    tooltip: 'can be used for basic change assessments',
    titleBGColor: '#6b6b6b',
    items: [
      {
        id: 'mon',
        title: 'Monthly averages',
      },
    ],
  },
  {
    title: 'yearly',
    description: 'Annual averages',
    tooltip: 'can be for basic change assessments',
    titleBGColor: '#ffc808',
    items: [
      {
        id: 'year',
        title: 'Annual averages',
      },
    ],
  },
];

export const cmip6SourceIds: FacetConfiguration[] = [
  {
    title: 'Model',
    description: 'Model',
    tooltip: 'Model',
    titleBGColor: '#ffc808',
    items: null,
    descriptions: {},
  },
];

const cmip6SourceIdsKeys = Object.keys(CMIP6SourceID.source_id);
for (let j = 0; j < cmip6SourceIdsKeys.length; j += 1) {
  const sourceId = cmip6SourceIdsKeys[j];
  cmip6SourceIds[0].descriptions[sourceId] = {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment
    title: CMIP6SourceID.source_id[sourceId].label_extended,
  };
}

export const members = CMIP6memberList.map((item) => ({
  id: item,
  title: item,
}));

export const cmip6MemberIds: FacetConfiguration[] = [
  {
    title: 'Member',
    description: 'Member',
    tooltip: 'Member',
    titleBGColor: '#9008ff',
    items: members,
    descriptions: {},
  },
];

export const cmip6Experiments: FacetConfiguration[] = [
  {
    title: 'Historical',
    description: 'Historical',
    tooltip: 'Historical',
    titleBGColor: '#f58d00',
    items: [
      {
        id: 'historical',
        title: 'Historical',
      },
      {
        id: 'esm-hist',
        title: 'CMIP6 historical (CO2 emission-driven)',
      },
    ],
    descriptions: {},
  },
  {
    title: 'Hindcast',
    description: 't',
    tooltip: 'Hindcast',
    titleBGColor: '#00a8ec',
    items: [
      {
        id: 'dcppA-hindcast',
        title: 'Hindcast',
      },
    ],
    descriptions: {},
  },
  {
    title: 'SSP',
    description: 'SSP scenarios',
    tooltip: 'SSP',
    titleBGColor: '#4CAF50',
    items: [
      {
        id: 'ssp585',
        title: 'update of RCP8.5 based on SSP5',
      },
      {
        id: 'ssp585-bgc',
        title: 'biogeochemically-coupled version of RCP8.5',
      },
      {
        id: 'ssp585-withism',
        title: 'ssp585 with interactive ice sheet',
      },
      {
        id: 'ssp126',
        title: 'update of RCP2.6 based on SSP1',
      },
      {
        id: 'ssp126-ssp370Lu',
        title: 'SSP1-2.6 with SSP3-7.0 land use',
      },
      {
        id: 'ssp245',
        title: 'update of RCP4.5 based on SSP2',
      },
      {
        id: 'ssp245-GHG',
        title: 'well-mixed GHG-only SSP2-4.5 run',
      },
      {
        id: 'ssp245-aer',
        title: 'aerosol-only SSP2-4.5 run',
      },
      {
        id: 'ssp245-nat',
        title: 'natural-only SSP2-4.5 run',
      },
      {
        id: 'ssp245-stratO3',
        title: 'stratospheric ozone-only SSP2-4.5 (ssp245) run',
      },
      {
        id: 'ssp370',
        title: 'gap-filling scenario reaching 7.0 based on SSP3',
      },
      {
        id: 'ssp370-lowNTCF',
        title: 'Future SSP3-7.0 with reduced NTCF emissions',
      },
      {
        id: 'ssp370-lowNTCFCH4',
        title: 'SSP3-7.0, with low NTCF emissions and methane concentrations',
      },
    ],
  },
];

const cmip6ExperimentKeys = Object.keys(CMIP6ExperimentID.experiment_id);
for (let j = 0; j < cmip6ExperimentKeys.length; j += 1) {
  const sourceId = cmip6ExperimentKeys[j];
  cmip6Experiments[0].descriptions[sourceId] = {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment
    title: CMIP6ExperimentID.experiment_id[sourceId].description,
  };
}
