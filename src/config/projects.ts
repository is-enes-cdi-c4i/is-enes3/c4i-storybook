import {
  cmip6Variables,
  cmip6Frequencies,
  cmip6SourceIds,
  cmip6Experiments,
  cmip6MemberIds,
} from './cmip6';
import {
  cmip5Variables,
  cmip5Frequencies,
  cmip5SourceIds,
  cmip5Experiments,
  cmip5EnsembleIds,
} from './cmip5';
import { ProjectType, PROJECTNAME } from '../types/searchTypes';

export const defaultProjects: ProjectType[] = [
  {
    title: PROJECTNAME.CMIP6,
    facetName: PROJECTNAME.CMIP6,
    disabled: false,
    description: 'Coupled model intercomparison experiment 6.',
    thumbnail:
      'https://cdn.knmi.nl/system/data_center_projects/image1s/000/000/092/large/CMIP6.jpg?1493730989',
    mainFacets: [
      {
        id: 'variable',
        title: 'Variable',
        config: cmip6Variables,
      },
      {
        id: 'frequency',
        title: 'Frequency',
        gridMd: 2,
        config: cmip6Frequencies,
      },
      {
        id: 'experiment_id',
        title: 'Experiment',
        config: cmip6Experiments,
      },
      {
        id: 'source_id',
        title: 'Model',
        gridMd: 12,
        config: cmip6SourceIds,
      },
      {
        id: 'member_id',
        title: 'Member',
        gridMd: 12,
        config: cmip6MemberIds,
      },
    ],
  },
  {
    title: PROJECTNAME.CMIP5,
    facetName: PROJECTNAME.CMIP5,
    disabled: false,
    description: 'Coupled model intercomparison experiment 5.',
    thumbnail:
      'http://blogs.reading.ac.uk/climate-lab-book/files/2014/01/fig-nearterm_all_UPDATE_2018-panela-1.png',
    mainFacets: [
      {
        id: 'variable',
        title: 'Variable',
        config: cmip5Variables,
      },
      {
        id: 'time_frequency',
        title: 'Frequency',
        gridMd: 2,
        config: cmip5Frequencies,
      },
      {
        id: 'experiment',
        title: 'Experiment',
        gridMd: 4,
        config: cmip5Experiments,
      },
      {
        id: 'model',
        title: 'Model',
        gridMd: 12,
        config: cmip5SourceIds,
      },
      {
        id: 'ensemble',
        title: 'Ensemble',
        gridMd: 12,
        config: cmip5EnsembleIds,
      },
    ],
  },
  {
    title: PROJECTNAME.CORDEX,
    facetName: PROJECTNAME.CORDEX,
    disabled: true,
    description: 'Global',
    thumbnail:
      'http://cordex.org/wp-content/uploads/2018/03/logo_cordex_200dpi.jpg',
    mainFacets: [
      {
        id: 'variable',
        title: 'Variable',
        config: [],
      },
    ],
  },
];
