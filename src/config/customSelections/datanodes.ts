export const ESGFNodeList = [
  'aims3.llnl.gov',
  'cmip.dess.tsinghua.edu.cn',
  'cmip.fio.org.cn',
  'cordexesg.dmi.dk',
  'crd-esgf-drc.ec.gc.ca',
  'data.meteo.unican.es',
  'dataserver.nccs.nasa.gov',
  'dpesgf03.nccs.nasa.gov',
  'esg-cccr.tropmet.res.in',
  'esg-dn1.nsc.liu.se',
  'esg-dn1.ru.ac.th',
  'esg-dn2.nsc.liu.se',
  'esg.camscma.cn',
  'esg.lasg.ac.cn',
  'esg.pik-potsdam.de',
  'esg1.umr-cnrm.fr',
  'esg2.umr-cnrm.fr',
  'esgdata.gfdl.noaa.gov',
  'esgf-cnr.hpc.cineca.it',
  'esgf-data.csc.fi',
  'esgf-data.ucar.edu',
  'esgf-data1.ceda.ac.uk',
  'esgf-data1.diasjp.net',
  'esgf-data1.llnl.gov',
  'esgf-data2.ceda.ac.uk',
  'esgf-data2.diasjp.net',
  'esgf-data2.llnl.gov',
  'esgf-data3.ceda.ac.uk',
  'esgf-data3.diasjp.net',
  'esgf-ictp.hpc.cineca.it',
  'esgf-nimscmip6.apcc21.org',
  'esgf-node.cmcc.it',
  'esgf-node2.cmcc.it',
  'esgf.anl.gov',
  'esgf.apcc21.org',
  'esgf.bsc.es',
  'esgf.dwd.de',
  'esgf.nccs.nasa.gov',
  'esgf.nci.org.au',
  'esgf.rcec.sinica.edu.tw',
  'esgf1.dkrz.de',
  'esgf2.dkrz.de',
  'esgf3.dkrz.de',
  'noresg.nird.sigma2.no',
  'polaris.pknu.ac.kr',
  'vesg.ipsl.upmc.fr',
];
