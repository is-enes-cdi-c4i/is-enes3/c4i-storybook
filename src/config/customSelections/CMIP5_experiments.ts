/* eslint-disable @typescript-eslint/naming-convention */
/* 
 Custom selection of facets for the scoped portal view. 
 Extended view will show all facets available on ESGF.

 */

export const CMIP5ExperimentList_rcp = ['rcp26', 'rcp45', 'rcp60', 'rcp85'];

export const CMIP5ExperimentList_historical = [
  'historical',
  'historicalExt',
  'historicalGHG',
  'historicalMisc',
  'historicalNat',
];
