/* 
 Custom selection of facets for the scoped portal view. 
 Extended view will show all facets available on ESGF.

 */

export const CMIP5ModelList = [
  'BCC-CSM1.1',
  'CCSM4',
  'CMCC-CM',
  'CNRM-CM5',
  'CSIRO-Mk3.6.0',
  'CanCM4',
  'CanESM2',
  'EC-EARTH',
  'FGOALS-g2',
  'GEOS-5',
  'GFDL-CM2.1',
  'GFDL-CM3',
  'GFDL-ESM2M',
  'GISS-E2-H',
  'GISS-E2-R',
  'HadCM3',
  'HadGEM2-ES',
  'IPSL-CM5A-LR',
  'IPSL-CM5A-MR',
  'MIROC-ESM',
  'MIROC4h',
  'MIROC5',
  'MPI-ESM-LR',
  'MPI-ESM-MR',
  'MRI-CGCM3',
  'NorESM1-M',
];
