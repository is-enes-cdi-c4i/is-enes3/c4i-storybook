/* eslint-disable @typescript-eslint/naming-convention */
import React from 'react';
import {
  NewNotebook,
  ResumeNotebook,
} from '../components/ESGFSearch/Processing/NotebookDialog';
import { SubsetParameters } from '../types/dataTypes';
import { NotebookSizeProperties, OPERATION } from '../types/searchTypes';
import { ESGFFileList } from '../utils/getFacetsInfo';

export default { title: 'Notebook Dialog' };

type Args = {
  completed: number;
  fileListObject: ESGFFileList[];
  myQueryLimit: number;
  handleClose: () => void;
  operation: OPERATION;
  subsettingParameters: SubsetParameters;
  serviceUrl: string;
  notebookSize: NotebookSizeProperties;
  stageSize: number;
};

export const IdleDialog_New_Download = (args: Args): React.ReactElement => (
  <NewNotebook {...args} subsettingMode={null} />
);
IdleDialog_New_Download.args = {
  operation: OPERATION.DOWNLOAD,
};
export const IdleDialog_New_Subsetting = (args: Args): React.ReactElement => (
  <NewNotebook {...args} subsettingMode={null} />
);
IdleDialog_New_Subsetting.args = {
  operation: OPERATION.SUBSETTING,
};
export const IdleDialog_Resume_Download = (args: Args): React.ReactElement => (
  <ResumeNotebook {...args} subsettingMode={null} />
);
IdleDialog_Resume_Download.args = {
  operation: OPERATION.DOWNLOAD,
};
export const IdleDialog_Resume_Subsetting = (
  args: Args
): React.ReactElement => <ResumeNotebook {...args} subsettingMode={null} />;
IdleDialog_Resume_Subsetting.args = {
  operation: OPERATION.SUBSETTING,
};
