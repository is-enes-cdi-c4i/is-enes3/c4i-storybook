/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react';
import {
  ResultsWindow,
  ResultsWindowProps,
} from '../components/ESGFSearch/Search/ResultsWindow';
import {
  DatasetSelectionCtx,
  FileSelectionCtx,
  NotebookFeedbackCtx,
  notebookDefaultProperties,
} from '../components/App/ApplicationContext';
import { OPERATION } from '../types/searchTypes';

export default { title: 'ResultsWindow' };

export const ResultWindowSearching = (
  args: ResultsWindowProps
): React.ReactElement => (
  <NotebookFeedbackCtx.Provider
    value={{
      notebookProperties: notebookDefaultProperties,
      setNotebookProperties: null,
    }}
  >
    <DatasetSelectionCtx.Provider
      value={{
        datasetCollection: [],
        selectDatasets: null,
      }}
    >
      <FileSelectionCtx.Provider
        value={{
          fileCollection: [],
          selectFiles: null,
        }}
      >
        <ResultsWindow {...args} />
      </FileSelectionCtx.Provider>
    </DatasetSelectionCtx.Provider>
  </NotebookFeedbackCtx.Provider>
);
ResultWindowSearching.args = {
  searchQuery: {
    selectedItems: [
      { facetId: 'project', fieldId: 'CMIP6', include: true },
      { facetId: 'variable', fieldId: 'ta', include: true },
    ],
  },
  operation: OPERATION.DOWNLOAD,
};
