/* eslint-disable @typescript-eslint/naming-convention */
import React from 'react';
import { MemoryRouter } from 'react-router';
import TailoredSearch from '../components/ESGFSearch/Search/TailoredSearch';
import { ProjectSelector } from '../components/NavigationBar/ProjectSelector/ProjectSelector';
import { SeachCount } from '../components/ESGFSearch/Search/SearchResults';
import { defaultProjects } from '../config/projects';

const mockUser = {
  email: 'abc',
  email_verified: true,
  family_name: 'abc',
  given_name: 'abc',
  groups: [],
  name: 'abc',
  preferred_username: 'Testuser',
  sub: 'abc',
  registered: false,
};

export default { title: 'ESGF Search' };

export const Project_Selector = (): React.ReactElement => (
  <MemoryRouter>
    <ProjectSelector />
  </MemoryRouter>
);

export const Tailored_Search = (): React.ReactElement => (
  <TailoredSearch user={mockUser} project={defaultProjects[0]} />
);

export const Seach_Count = (): React.ReactElement => (
  <SeachCount datasetCount={10} fileCount={5} />
);
