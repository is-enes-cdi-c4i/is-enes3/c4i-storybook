import React from 'react';
import { MemoryRouter, Route, Routes } from 'react-router';
import App from '../components/App/App';
import { LandingPage } from '../components/NavigationBar/LandingPage/LandingPage';
import Register from '../components/NavigationBar/Register';
import { NavigationBar } from '../components/NavigationBar/NavigationBar';
import { OpenidUser } from '../types/dataTypes';

const openidUser: OpenidUser = {
  email: 'ab@cd.ef',
  email_verified: true,
  family_name: 'somename',
  given_name: 'somename',
  groups: [],
  name: 'somename',
  preferred_username: 'Some Name',
  sub: 'sub',
  registered: true,
};

const mockUser = {
  email: 'abc',
  email_verified: true,
  family_name: 'abc',
  given_name: 'abc',
  groups: [],
  name: 'abc',
  preferred_username: 'Testuser',
  sub: 'abc',
  registered: false,
};

export default { title: 'C4I-App' };

export const C4IApp = (): React.ReactElement => <App />;
export const C4ILandingPage = (): React.ReactElement => (
  <MemoryRouter>
    <Routes>
      <Route path='/' element={<LandingPage user={openidUser} />} />
    </Routes>
  </MemoryRouter>
);
export const C4IRegister = (): React.ReactElement => <Register />;
export const C4INavigationBar = (): React.ReactElement => (
  <MemoryRouter>
    <Routes>
      <Route>
        <Route path='/' element={<NavigationBar user={mockUser} />} />
      </Route>
    </Routes>
  </MemoryRouter>
);
