// import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
// import React from 'react';
// import { createStore } from 'redux';
// import App from '../src/components/App/App';
// import { createReducerManager } from '../src/utils/ReducerManager';
// import TailoredSearch from '../src/components/ESGFSearch/Search/TailoredSearch';
// import RenderFacetsBar from '../src/components/ESGFSearch/RenderFacetsBar';
// import FacetSelector from '../src/components/ESGFSearch/FacetSelector';
// import SearchResults from '../src/components/ESGFSearch/Search/SearchResults';
// import GetFacetInfoDemo from '../src/components/ESGFSearch/GetFacetInfoDemo';
// import GetSearchResultsDemo from '../src/components/ESGFSearch/GetSearchResultsDemo';
// import GetFilesForDatasetDemo from '../src/components/ESGFSearch/GetFilesForDatasetDemo';
// import GetFilesForQuery from '../src/components/ESGFSearch/GetFilesForQuery';
// import ProjectSelector, {
//   defaultProjects
// } from '../src/components/ESGFSearch/ProjectSelector';
// import Provider from '../src/utils/Provider';
// import {
//   cmip6Variables,
//   cmip6Frequencies,
//   cmip6SourceIds,
//   cmip6Experiments
// } from '../src/config/cmip6';

// import { getConfig } from '../src/utils/configReader';
// require('./styles.css');

// const config = getConfig();

// // Initialize the store.
// const rootReducer = (state = {}) => {
//   return state;
// };
// const reducerManager = createReducerManager({ root: rootReducer });
// const store = createStore(
//   reducerManager.reduce,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );
// store.reducerManager = reducerManager;
// /* To work properly with npm link we need the store on the window object, see Readme why */
// window.store = store;
// // Add the reducers.
// // reducerManager.add(WEBMAPJS_REDUCERNAME, webMapJSReducer);

// // Add the reducermanager to the window
// window.reducerManager = reducerManager;

// const exampleQuery = {
//   selectedItems: [
//     { facetId: 'project', fieldId: 'CMIP6', include: true },
//     { facetId: 'variable', fieldId: 'tasmax', include: true },
//     { facetId: 'frequency', fieldId: 'mon', include: true }
//     // {
//     //   facetId: 'experiment_id',
//     //   fieldId: 'piClim-2xNOx',
//     //   include: true
//     // }
//     //{ facetId: 'source_id', fieldId: 'GFDL-ESM4', include: true }
//   ]
// };

// const experimentResults = {
//   experiment_id: {
//     historical: 2,
//     testA: 4,
//     testB: 5,
//     testC: 6,
//     testD: 7
//   }
// };

// storiesOf('C4I-ESGFSearch', module)
//   .add('TailoredSearch application', () => (
//     <Provider store={store}>
//       <TailoredSearch />
//     </Provider>
//   ))
//   .add('Project selector', () => (
//     <Provider store={store}>
//       <div>
//         <ProjectSelector />
//       </div>
//     </Provider>
//   ))
//   .add('FacetsBar', () => {
//     const project = {
//       title: 'Test project',
//       description: 'Description of the project',
//       thumbnail: 'https://source.unsplash.com/random',
//       mainFacets: [
//         {
//           id: 'variableA',
//           title: 'ParameterA'
//         },
//         {
//           id: 'variableB',
//           title: 'ParameterB'
//         },
//         {
//           id: 'variableC',
//           title: 'ParameterC'
//         },
//         {
//           id: 'variableD',
//           title: 'ParameterD'
//         }
//       ]
//     };
//     action('onFacetClick OK');
//     console.log('start');
//     return (
//       <Provider store={store}>
//         <div>
//           <RenderFacetsBar
//             projectType={project}
//             onFacetClick={() => {
//               console.log('action = ok');
//               action('onFacetClick');
//             }}
//             searchQuery={{
//               selectedItems: []
//             }}
//           />
//         </div>
//       </Provider>
//     );
//   })
//   .add('FacetSelector CMIP6 Variables', () => (
//     <Provider store={store}>
//       <div>
//         <FacetSelector
//           facetType={{ id: 'Var', title: 'Variable', config: cmip6Variables }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('FacetSelector CMIP6 Frequency', () => (
//     <Provider store={store}>
//       <div>
//         <FacetSelector
//           facetType={{
//             id: 'Var',
//             title: 'Frequency',
//             gridMd: 2,
//             config: cmip6Frequencies
//           }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('FacetSelector CMIP6 Source ID', () => (
//     <Provider store={store}>
//       <div>
//         <FacetSelector
//           facetType={{
//             id: 'source_id',
//             title: 'Model',
//             gridMd: 12,
//             config: cmip6SourceIds
//           }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('FacetSelector CMIP6 Experiment ID', () => (
//     <Provider store={store}>
//       <div>
//         <FacetSelector
//           searchQuery={exampleQuery}
//           searchFacetResults={experimentResults}
//           facetType={{
//             id: 'experiment_id',
//             title: 'Experiment',
//             // gridMd: 12,
//             config: cmip6Experiments
//           }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('getFacetInfoFunction', () => (
//     <Provider store={store}>
//       <div style={{ width: '100%' }}>
//         <GetFacetInfoDemo
//           project={
//             defaultProjects.filter(project => project.facetName === 'CMIP6')[0]
//           }
//           searchQuery={{
//             selectedItems: [
//               { facetId: 'project', fieldId: 'CMIP6', include: true },
//               { facetId: 'variable', fieldId: 'tasmax', include: true },
//               { facetId: 'frequency', fieldId: 'day', include: true },
//               { facetId: 'experiment_id', fieldId: 'historical', include: true }
//             ]
//           }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('getSearchResultsFunction', () => (
//     <Provider store={store}>
//       <div style={{ width: '100%' }}>
//         <GetSearchResultsDemo
//           project={
//             defaultProjects.filter(project => project.facetName === 'CMIP6')[0]
//           }
//           searchQuery={{
//             selectedItems: [
//               { facetId: 'project', fieldId: 'CMIP6', include: true },
//               { facetId: 'variable', fieldId: 'tasmax', include: true },
//               { facetId: 'frequency', fieldId: 'day', include: true },
//               { facetId: 'experiment_id', fieldId: 'historical', include: true }
//             ]
//           }}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('getFilesForDataSetFunction', () => (
//     <Provider store={store}>
//       <div style={{ width: '100%' }}>
//         <GetFilesForDatasetDemo
//           datasets={[
//             {
//               id:
//                 'CMIP6.CMIP.NCC.NorESM2-LM.historical.r3i1p1f1.day.tasmax.gn.v20190920|noresg.nird.sigma2.no'
//             },
//             {
//               id:
//                 'CMIP6.PAMIP.NCC.NorESM2-LM.pdSST-pdSIC.r46i1p1f1.AERmon.mmroa.gn.v20190920|noresg.nird.sigma2.no'
//             }
//           ]}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('getFilesForSearchQuery', () => (
//     <Provider store={store}>
//       <div>
//         <GetFilesForQuery
//           searchQuery={exampleQuery}
//           project={defaultProjects[0]}
//           queryLimit={100}
//         />
//       </div>
//     </Provider>
//   ))
//   .add('Search results Component', () => (
//     <Provider store={store}>
//       <div>
//         <SearchResults
//           searchQuery={exampleQuery}
//           project={defaultProjects[0]}
//         />
//       </div>
//     </Provider>
//   ));
