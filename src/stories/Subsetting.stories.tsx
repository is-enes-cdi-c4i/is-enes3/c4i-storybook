/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable react/jsx-no-constructed-context-values */
import React from 'react';
import { ParameterSelector } from '../components/ESGFSearch/Subsetting/ParameterSelector';
import {
  SubsettingForm,
  SubsetProps,
} from '../components/ESGFSearch/Subsetting/SubsettingForm';
import {
  SubsetTemporalParameters,
  SubsetSpatialParameters,
  DEFAULTCOORDINATES,
  NODATE,
} from '../types/dataTypes';
import {
  DatasetSelectionCtx,
  NotebookFeedbackCtx,
  notebookDefaultProperties,
} from '../components/App/ApplicationContext';
import { SUBSETTING } from '../types/searchTypes';

export default { title: 'Subsetting Components' };

type Args = {
  temporalParameters: SubsetTemporalParameters;
  setTemporalParameter: () => void;
  spatialParameters: SubsetSpatialParameters;
  setSpatialParameter: () => void;
  setRookOperation: () => void;
  rookOperation: string;
  subsettingMode: SUBSETTING;
};

export const ParameterSelector_Window = (args: Args): React.ReactElement => (
  <DatasetSelectionCtx.Provider
    value={{
      datasetCollection: [],
      selectDatasets: null,
    }}
  >
    <ParameterSelector {...args} />
  </DatasetSelectionCtx.Provider>
);
ParameterSelector_Window.args = {
  temporalParameters: NODATE,
  setTemporalParameter: null,
  spatialParameters: DEFAULTCOORDINATES,
  setSpatialParameter: null,
  subsettingMode: SUBSETTING.ROOK,
};
export const OpendapAccessForm_Window = (
  args: SubsetProps
): React.ReactElement => (
  <NotebookFeedbackCtx.Provider
    value={{
      notebookProperties: notebookDefaultProperties,
      setNotebookProperties: null,
    }}
  >
    <DatasetSelectionCtx.Provider
      value={{
        datasetCollection: [],
        selectDatasets: null,
      }}
    >
      <SubsettingForm {...args} />;
    </DatasetSelectionCtx.Provider>
  </NotebookFeedbackCtx.Provider>
);

OpendapAccessForm_Window.args = {
  setAccessFormOpen: null,
  setSubsettingParameters: null,
  subsettingMode: SUBSETTING.ROOK,
};
