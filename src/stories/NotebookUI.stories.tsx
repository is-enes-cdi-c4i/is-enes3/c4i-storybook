/* eslint-disable no-alert */
import React from 'react';
import Typography from '@mui/material/Typography';

import { LinearProgress } from '@mui/material';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import {
  NotebookErrorAlert,
  NotebookGeneratingAlert,
  NotebookReadyAlert,
  NotebookDownloadingAlert,
  NotebookWorkerAlert,
} from '../components/GenericUI/Alerts';
import { DialogWrapper } from '../components/GenericUI/DialogWrapper';

export default { title: 'Notebook UI' };

type Args = {
  title: string;
  buttonTitle: string;
  buttonCallback: () => void;
  buttonDisabled: boolean;
  size: false | 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  children: React.ReactElement;
};

export const Dialog = (args: Args): React.ReactElement => (
  <DialogWrapper {...args} clearOnClose={null} />
);

const renderDummyChild = () => (
  <Typography>
    Praesent commodo cursus magna magna, vel scelerisque nisl consectetur et et
    et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
  </Typography>
);

Dialog.args = {
  title: 'Title',
  buttonTitle: 'Ok',
  buttonCallback: () => alert('Click'),
  children: renderDummyChild(),
};

export const ErrorDialog = (args: Args): React.ReactElement => (
  <DialogWrapper {...args} clearOnClose={null} />
);
ErrorDialog.args = {
  title: 'Error',
  children: <NotebookErrorAlert error='(Code 500)' />,
};

export const GeneratingDialog = (args: Args): React.ReactElement => (
  <DialogWrapper {...args} clearOnClose={null} />
);
const renderGeneratingUI = () => (
  <>
    <LinearProgress />
    <br />
    <NotebookGeneratingAlert />
  </>
);
GeneratingDialog.args = {
  title: 'Generating',
  children: renderGeneratingUI(),
};

export const ReadyDialog = (args: Args): React.ReactElement => (
  <DialogWrapper {...args} clearOnClose={null} />
);
const renderReadyUI = () => (
  <List>
    <Divider component='li' />
    <NotebookReadyAlert />
    <Divider component='li' />
    <NotebookWorkerAlert workerNumber={5} />
    <Divider component='li' />
    <NotebookDownloadingAlert />
    <Divider component='li' />
  </List>
);
ReadyDialog.args = {
  title: 'Ready',
  children: renderReadyUI(),
  buttonTitle: 'Open Notebook',
  buttonCallback: () => alert('click'),
};
