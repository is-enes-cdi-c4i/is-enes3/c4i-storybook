import React from 'react';

import EnhancedTable, {
  NodeSelection,
} from '../components/ESGFSearch/Header/NodeSelection';
import { ESGFNodeList } from '../config/customSelections/datanodes';

export default { title: 'Node Selection' };

export const Table = (): React.ReactElement => (
  <EnhancedTable
    rows={ESGFNodeList}
    localState={ESGFNodeList}
    setLocalState={() => null}
  />
);

export const NodeSelectionStory = (): React.ReactElement => (
  <NodeSelection
    selectedNodes={ESGFNodeList}
    setOpenNodeSelection={() => false}
    setNodes={null}
    openNodeSelection
  />
);
