import React from 'react';
import { ESGFFileProperties } from '../../utils/getFacetsInfo';
import {
  NotebookProperties,
  ESGFDataset,
  Status,
  OUTPUT,
} from '../../types/searchTypes';
import { OpenidUser } from '../../types/dataTypes';

type Props = {
  children: React.ReactElement;
};

export type DatasetCtx = {
  datasetCollection: ESGFDataset[];
  selectDatasets: (datasets: ESGFDataset[]) => void;
};

export type FileCtx = {
  fileCollection: ESGFFileProperties[];
  selectFiles: (files: ESGFFileProperties[]) => void;
};

type NotebookFeedbackCtxType = {
  notebookProperties: NotebookProperties;
  setNotebookProperties: (status: NotebookProperties) => void;
};

export type UserCtxType = {
  user: OpenidUser;
  setUser: (user: OpenidUser) => void;
};

export const DatasetSelectionCtx = React.createContext({} as DatasetCtx);
export const FileSelectionCtx = React.createContext({} as FileCtx);
export const NotebookFeedbackCtx = React.createContext(
  {} as NotebookFeedbackCtxType
);
export const UserCtx = React.createContext({} as UserCtxType);

export const emptyFileSelection: ESGFFileProperties[] = [];
export const emptyDatasetSelection: ESGFDataset[] = [];

export const DatasetSelectionProvider = ({ children }: Props): JSX.Element => {
  const [datasetCollection, selectDatasets] = React.useState(
    emptyDatasetSelection
  );
  const value = React.useMemo(
    () => ({
      datasetCollection,
      selectDatasets,
    }),
    [datasetCollection, selectDatasets]
  );

  return (
    <DatasetSelectionCtx.Provider value={value}>
      {children}
    </DatasetSelectionCtx.Provider>
  );
};

export const FileSelectionProvider = ({ children }: Props): JSX.Element => {
  const [fileCollection, selectFiles] = React.useState(emptyFileSelection);
  const value = React.useMemo(
    () => ({
      fileCollection,
      selectFiles,
    }),
    [fileCollection, selectFiles]
  );

  return (
    <FileSelectionCtx.Provider value={value}>
      {children}
    </FileSelectionCtx.Provider>
  );
};

export const notebookDefaultProperties = {
  dialogOpen: false,
  output: OUTPUT.NA,
  processingStatus: Status.IDLE,
  redirectUrl: '',
  workerNumber: 0,
};

export const NotebookPropertiesProvider = ({
  children,
}: Props): JSX.Element => {
  const [notebookProperties, setNotebookProperties] =
    React.useState<NotebookProperties>(notebookDefaultProperties);
  const value = React.useMemo(
    () => ({
      notebookProperties,
      setNotebookProperties,
    }),
    [notebookProperties, setNotebookProperties]
  );

  return (
    <NotebookFeedbackCtx.Provider value={value}>
      {children}
    </NotebookFeedbackCtx.Provider>
  );
};

export const UserProvider = ({ children }: Props): JSX.Element => {
  const [user, setUser] = React.useState<OpenidUser>(null);
  const value = React.useMemo(
    () => ({
      user,
      setUser,
    }),
    [user, setUser]
  );

  return <UserCtx.Provider value={value}>{children}</UserCtx.Provider>;
};
