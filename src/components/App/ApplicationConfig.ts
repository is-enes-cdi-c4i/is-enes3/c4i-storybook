// Global config settings
import { getConfig } from '../../utils/configReader';

type Config = {
  baseUrl: string;
};
const config: Config = getConfig();

/** SWIRRL API Url */
export const BASEURL = `${config.baseUrl}/relay/swirrl-api/v1.0`;

/** Limit dataset results to the latest instance only
 *
 * Default: LATEST
 * Datasets contain links to files, for non-latest datasets, files are often cleaned and no longer available.
 * Use ALL only if you want to retrieve information about all available instances, but no actual files.
 */

enum INSTANCE {
  ALL = '',
  LATEST = '&latest=true',
}

export const DATASETVERSION = INSTANCE.LATEST;

/** DATASETLIMIT for search results on the ESGF node(s).
 *
 * Default: 100
 * Max: 10000
 * A hardcoded limit on the number of results from the facet query
 * being displayed in the search result table. This is mainly used to
 * prevent users to overload the apis with a massive number of files.
 *
 *
 */
export const DATASETLIMIT = 200;

// The amount of data that a user can download at once in a download wf.
export const STAGELIMIT = 3 * 1000 * 1000 * 1000;

/** FILELIMIT for a single, selected dataset. Swirrl only.
 *
 * Default: 0
 * Datasets often contain more links than Swirrl can process.
 * Set to a meaningful number (best 1 or <5) for demo or test purposes.
 * Setting filelimit to 0  will unset the limit.
 * This parameter is only used for notebooks, not the file list download.
 *
 */
export const FILELIMIT = 0;

/** OPENID configuration. */
const OPENIDBASEURL = `${config.baseUrl}/protocol/openid-connect`;

export const OPENID = {
  authorization_endpoint: `${OPENIDBASEURL}/auth`,
  jwks_uri: `${OPENIDBASEURL}/certs`,
  token_endpoint: `${OPENIDBASEURL}/token`,
  introspection_endpoint: `${OPENIDBASEURL}/token/introspect`,
  userinfo_endpoint: `${OPENIDBASEURL}/userinfo`,
  end_session_endpoint: `${OPENIDBASEURL}/logout`,
};

/** Google Form
 *
 * Feedback link to be used on the C4I Portal
 *
 */
export const GFORM = 'https://forms.gle/m9FRTABa7Xq79Kop8';

/** esmvaltool base url
 *
 * Base url to the model performance pages of the esmvaltool
 *
 */
export const ESMVALBASEURL =
  'https://esmvaltool.dkrz.de/shared/esmvaltool/climate4impact/';
