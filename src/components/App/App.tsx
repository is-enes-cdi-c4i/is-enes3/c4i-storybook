import React, { useContext, useEffect } from 'react';
import { Route, Routes } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, createTheme } from '@mui/material/styles';

import { getConfig } from '../../utils/configReader';
import { NavigationBar } from '../NavigationBar/NavigationBar';
import { Login } from '../NavigationBar/Login';
import Register from '../NavigationBar/Register';
import TailoredSearch from '../ESGFSearch/Search/TailoredSearch';
import { OPENID } from './ApplicationConfig';
import { LandingPage } from '../NavigationBar/LandingPage/LandingPage';
import {
  C4IUserGuide,
  SwirrlUserGuide,
} from '../NavigationBar/Documentation/UserGuide';
import { UserProvider, UserCtx } from './ApplicationContext';
import { ProjectSelector } from '../NavigationBar/ProjectSelector/ProjectSelector';
import { defaultProjects } from '../../config/projects';
import { OpenidUser } from '../../types/dataTypes';

type Config = {
  frontendPath: string;
  frontendContentURL: string;
};

require('./App.css');

const config: Config = getConfig();

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const classes = {
  main: {
    fontSize: '1.0rem',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    lineHeight: 1.334,
    letterSpacing: '0em',
    padding: '20px',
    backgroundColor: '#fafafa',
  },
  mainPaper: {
    padding: '20px',
  },
};

const theme = createTheme({
  palette: {
    primary: {
      main: '#3f51b5',
    },
  },
});

// Extend the Window interface to include the _mtm property
declare global {
  interface Window {
    _mtm: Array<{ [key: string]: unknown }>;
  }
}

const Main: React.FC = () => {
  const { user, setUser } = useContext(UserCtx);
  const selectedProject = defaultProjects.find(
    (project) =>
      project.title ===
      (new URLSearchParams(window.location.search).get('project') || 'CMIP6')
  );

  React.useEffect(() => {
    fetch(OPENID.userinfo_endpoint, {
      method: 'GET',
      credentials: 'include',
    })
      .then((res) => (res.status === 200 ? res.json() : null))
      .then((res) => setUser(res as OpenidUser))
      .catch((err) => console.error(err));
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter basename={config.frontendPath}>
        <div className='MainWrapper'>
          <div className='MainNavBar'>
            <NavigationBar user={user} />
          </div>
          <div className='MainContent'>
            <Routes>
              <Route
                path='/'
                element={
                  <div style={classes.main}>
                    <LandingPage user={user} />
                  </div>
                }
              />
              <Route path='project' element={<ProjectSelector />} />
              <Route
                path='search'
                element={
                  <TailoredSearch user={user} project={selectedProject} />
                }
              />
              <Route
                path='login'
                element={
                  <div style={classes.main}>
                    <Login />
                  </div>
                }
              />
              <Route
                path='register'
                element={
                  <div style={classes.main}>
                    <Register />
                  </div>
                }
              />
              <Route
                path='helpC4I'
                element={
                  <div style={classes.main}>
                    <C4IUserGuide />
                  </div>
                }
              />
              <Route
                path='helpSwirrl'
                element={
                  <div style={classes.main}>
                    <SwirrlUserGuide />
                  </div>
                }
              />
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </ThemeProvider>
  );
};

const App: React.FC = () => {
  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle,no-multi-assign
    const _mtm = (window._mtm = window._mtm || []);
    _mtm.push({ 'mtm.startTime': new Date().getTime(), event: 'mtm.Start' });

    const d = document;
    const g = d.createElement('script');
    const s = d.getElementsByTagName('script')[0];

    g.async = true;
    g.src = 'https://cdn.matomo.cloud/knmi.matomo.cloud/container_5D2g8rf9.js';
    s.parentNode?.insertBefore(g, s);
  }, []);

  return (
    <div className='viewer_wrapper'>
      <div className='viewer_header'>
        <div className='c4i-header-logo'>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a href='#'>
            <img src='./images/IS-ENES3_logo_small.png' alt='is-enes logo' />
          </a>
        </div>
        <div className='c4i-header-text'>Exploring climate model data</div>
      </div>
      <div className='viewer_sidebar'>Sidebar</div>
      <div className='viewer_content'>
        <UserProvider>
          <Main />
        </UserProvider>
      </div>
      <div className='viewer_footer'>
        <img className='c4i-footer-euflag' alt='' src='./images/euflag.png' />
        <span className='c4i-footer-text'>
          This project has received funding from the European Union’s Horizon
          2020 research and innovation programme under grant agreement No 824084
        </span>
      </div>
    </div>
  );
};
export default App;
