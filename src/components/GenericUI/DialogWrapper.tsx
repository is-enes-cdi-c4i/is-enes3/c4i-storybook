import React, { useContext } from 'react';
import Button from '@mui/material/Button';
import Dialog, { dialogClasses } from '@mui/material/Dialog';
import { styled } from '@mui/material/styles';
import MuiDialogTitle from '@mui/material/DialogTitle';
import MuiDialogContent from '@mui/material/DialogContent';
import MuiDialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';

import {
  NotebookFeedbackCtx,
  notebookDefaultProperties,
  DatasetSelectionCtx,
  emptyFileSelection,
  emptyDatasetSelection,
  FileSelectionCtx,
} from '../App/ApplicationContext';

const classes = {
  root: {
    margin: '0px',
    padding: '16px',
    paddingBottom: '0px',
  },
  closeButton: {
    position: 'absolute',
    right: '8px',
    top: '8px',
    color: '#9e9e9e',
  },
};

type Props = {
  title: string;
  buttonTitle: string;
  buttonCallback: () => void;
  buttonDisabled: boolean;
  size: false | 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  clearOnClose: boolean;
  children: React.ReactElement;
};

export interface DialogTitleProps {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = (props: DialogTitleProps) => {
  const { children, onClose, ...other } = props;
  return (
    <MuiDialogTitle sx={classes.root} {...other}>
      <Typography variant='h6' style={{ minHeight: '20px' }}>
        {children}
      </Typography>
      {onClose ? (
        <IconButton
          aria-label='close'
          sx={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

const DialogContent = styled(MuiDialogContent)(() => ({
  paddingRight: '16px',
  paddingLeft: '16px',
  minWidth: '500px',
  minHeight: '50px',
}));

const DialogActions = styled(MuiDialogActions)(() => ({
  margin: 0,
  padding: '8px',
}));

export const DialogWrapper = ({
  title,
  buttonTitle,
  buttonCallback,
  buttonDisabled,
  size = 'sm',
  clearOnClose = false,
  children,
}: Props): React.ReactElement => {
  const [open, setOpen] = React.useState(true);

  const { selectFiles } = useContext(FileSelectionCtx);
  const { selectDatasets } = useContext(DatasetSelectionCtx);
  const { setNotebookProperties } = useContext(NotebookFeedbackCtx);

  const handleClose = () => {
    if (clearOnClose) {
      selectFiles(emptyFileSelection);
      selectDatasets(emptyDatasetSelection);
    }
    setNotebookProperties(notebookDefaultProperties);
    setOpen(false);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='dialog-title'
      open={open}
      maxWidth={size}
      fullWidth
      sx={{ [`& .${dialogClasses.paper}`]: { maxWidth: '960px' } }}
    >
      <DialogTitle id='dialog-title' onClose={handleClose}>
        {title}
      </DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        {buttonTitle ? (
          <Button
            autoFocus
            onClick={buttonCallback}
            color='primary'
            variant='contained'
            disabled={buttonDisabled}
            style={{
              marginBottom: '10px',
              marginRight: '10px',
            }}
          >
            {buttonTitle}
          </Button>
        ) : (
          <div style={{ height: '10px' }} />
        )}
      </DialogActions>
    </Dialog>
  );
};
