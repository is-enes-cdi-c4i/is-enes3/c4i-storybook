import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';

export const CustomizedTooltip = styled(Tooltip)(() => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: 'darkblue',
    color: 'white',
    fontSize: '16px',
    fontWeight: 400,
  },
}));
