// A generic component that lets you conditionally wrap (child) components

type WrapperType = {
  condition: boolean;
  wrapper: (children: JSX.Element) => JSX.Element;
  children: JSX.Element;
};

export const ConditionalWrapper = ({
  condition,
  wrapper,
  children,
}: WrapperType): JSX.Element => (condition ? wrapper(children) : children);
