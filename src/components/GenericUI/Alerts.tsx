import React from 'react';
import { Link } from '@mui/material';
import Alert, { AlertProps, AlertColor } from '@mui/lab/Alert';
import { NotebookSizeProperties, OPERATION } from '../../types/searchTypes';
import { parseSize } from '../../utils/Swirrl/sizeCalculation';
import { STAGELIMIT } from '../App/ApplicationConfig';

interface ResumeProps {
  url: string;
  notebookSize: NotebookSizeProperties;
}

interface StageProps {
  stageSize: number;
  operation: string;
}

interface MessageProps {
  message: string;
}

interface ErrorProps {
  error: string;
}

type StageAlert = {
  severity: AlertColor;
  message: string;
};

const alertStyle = { margin: '10px 0' };

export const NotebookGeneratingAlert = (): JSX.Element => (
  <Alert severity='info' style={alertStyle}>
    SWIRRL is generating your notebook, this may take a while...
  </Alert>
);

export const NotebookReadyAlert = (): JSX.Element => (
  <Alert
    severity='success'
    style={{ backgroundColor: 'white', color: 'black' }}
  >
    Notebook is ready
  </Alert>
);
export const NotebookWorkerAlert = ({
  workerNumber = 0,
}: {
  workerNumber: number;
}): JSX.Element => (
  <Alert
    severity='success'
    style={{ backgroundColor: 'white', color: 'black' }}
  >
    {`${workerNumber} Workers are transferring the data`}
  </Alert>
);

const usageAlert = (currentSize: string): string => {
  if (currentSize === '') return '.';
  return ` current data usage: ${currentSize}.`;
};

export const WorkflowProgressAlert = ({
  currentStatus,
}: NotebookSizeProperties): JSX.Element => {
  if (currentStatus === 'Workflow in progress') {
    return (
      <Alert severity='info'>
        There is already a workflow running on this session, so your new
        workflow will only start after the current one has completed.
      </Alert>
    );
  }
  return null;
};

const stageAlert = (stageSize: number, operation: string): StageAlert => {
  let message = `You have selected ${parseSize(stageSize)} of data.`;
  let severity = 'warning' as AlertColor;
  if (operation === OPERATION.SUBSETTING) {
    message += ` For the subsetting operations, the size of downloaded data based on
      your parameters will be smaller, but cannot be estimated.`;
  }
  if (operation === OPERATION.DOWNLOAD && stageSize > STAGELIMIT) {
    severity = 'error' as AlertColor;
    message += ` You are not allowed to start a download workflow on more than
      ${parseSize(STAGELIMIT)} of data as this can result in the workflow timing
      out. Please consider selecting less data at a time or using a subsetting workflow.`;
  } else {
    message += ` Some ESGF nodes perform worse than others and even the more performant
      ones can experience downtime occasionally. This means that your workflow can time
      out. If this occurs, we advise you to split your workflow up into smaller parts or
      to try a different node.`;
  }
  return {
    severity,
    message,
  };
};

export const NotebookResumeAlert = ({
  url,
  notebookSize,
}: ResumeProps): JSX.Element => (
  <Alert severity='info' style={{ margin: '15px 0' }}>
    <span>
      {' '}
      Existing notebook found{' '}
      <Link
        href={`${url}/lab/tree/Welcome%20to%20SWIRRL.md`}
        target='_blank'
        rel='noopener'
      >
        ({url})
      </Link>
      {notebookSize && usageAlert(parseSize(notebookSize.currentSize))}
    </span>
  </Alert>
);

export const StageSizeAlert = ({
  stageSize,
  operation,
}: StageProps): JSX.Element => {
  const alert = stageAlert(stageSize, operation);
  if (!alert) return null;
  return (
    <Alert severity={alert.severity} style={alertStyle}>
      <span>{alert.message}</span>
    </Alert>
  );
};

export const NotebookDownloadingAlert = (): JSX.Element => (
  <Alert severity='warning'>Download may take a while.</Alert>
);

export const NotebookMessageAlert = ({
  message,
}: MessageProps): JSX.Element => {
  if (message) {
    return (
      <Alert severity='warning' style={alertStyle}>
        {message}
      </Alert>
    );
  }
  return null;
};

export const NotebookErrorAlert = ({ error }: ErrorProps): JSX.Element => (
  <Alert variant='filled' severity='error' style={alertStyle}>
    There was an error generating your notebook {`${error}`}. Retry or select a
    different dataset.
  </Alert>
);

export const DoiErrorAlert = (props: AlertProps): React.ReactElement => {
  return <Alert elevation={6} variant='filled' {...props} />;
};

export const SomeTimespanAlert = (): JSX.Element => (
  <Alert severity='info' style={alertStyle}>
    Datasets without temporal information found in selection. These datasets
    will not be subsetted by date parameters chosen in this dialog.
  </Alert>
);
export const NoTimespanAlert = (): JSX.Element => (
  <Alert severity='warning' style={alertStyle}>
    Selected datasets do not contain start or end dates and will not be
    subsetted by date parameters.
  </Alert>
);
export const ParameterUnsetAlert = (): JSX.Element => (
  <Alert severity='info' style={alertStyle}>
    Subsetting is disabled. The full dataset will be processed.
  </Alert>
);
export const SubsettingAlert = (): JSX.Element => (
  <Alert severity='info' style={{ margin: '20px 20px 5px 20px' }}>
    Some ESGF nodes might not support subsetting. If you face issues with this
    functionality and no data becomes available, you can either try with a
    different node (mirror) or use the &quot;Download&quot; button to download
    the full data locally (via the &quot;Link List&quot;) or to a C4I notebook.
  </Alert>
);

export const DuplicatesInSelectionAlert = ({
  datasetDuplicates,
  fileDuplicates,
}: {
  datasetDuplicates: string[];
  fileDuplicates: string[];
}): JSX.Element => (
  <Alert severity='warning' style={alertStyle}>
    <div>You have selected multiple datasets or files with the same name:</div>
    <ul>
      {datasetDuplicates.map((dataset) => (
        <li key={dataset}>{`Dataset: ${dataset}`}</li>
      ))}
      {fileDuplicates.map((file) => (
        <li key={file}>{`File: ${file}`}</li>
      ))}
    </ul>
  </Alert>
);
