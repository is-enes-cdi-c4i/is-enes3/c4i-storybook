import React from 'react';
import { Container, Grid, Link, Typography } from '@mui/material';
import { LoginClickHandler } from './Login';

const classes = {
  root: {
    flexGrow: 1,
    align: 'center',
    textAlign: 'justify',
  },
  login: {
    marginRight: '0px',
    marginLeft: 'auto',
  },
  registerSteps: {
    border: '1px solid grey',
    padding: '30px',
    backgroundColor: 'white',
  },
};

const Register: React.FC = () => {
  return (
    <div style={classes.root as React.CSSProperties}>
      <Container maxWidth='md'>
        <Grid container direction='column' alignItems='center'>
          <Grid item alignItems='center'>
            <Typography
              variant='h3'
              align='center'
              color='textPrimary'
              gutterBottom
            >
              Register
            </Typography>
          </Grid>
          <Grid item alignItems='center'>
            <Typography variant='body1' color='textSecondary' paragraph>
              <p style={{ textAlign: 'center', fontWeight: 600 }}>
                This is the new release of the Climate4Impact portal.
              </p>
              <p>
                We are extending the portal with new analysis capabilities based
                on reproducible Jupyter Notebooks, and a collection of climate
                related software and workflows. Would you like to access the new
                services and provide us feedback to contribute to their
                improvement?
              </p>
            </Typography>
            <div style={classes.registerSteps}>
              <Typography variant='h5' color='textSecondary' paragraph>
                1. Create an account at one of the supported identity providers
              </Typography>
              <Typography variant='subtitle1' color='textSecondary' paragraph>
                <p style={{ paddingLeft: 10, paddingBottom: 10 }}>
                  To log in to C4I you need to have an account at one of these 3
                  identity providers:{' '}
                  <Link
                    target='_blank'
                    rel='noopener'
                    href='https://accounts.ceda.ac.uk/'
                  >
                    CEDA
                  </Link>
                  ,{' '}
                  <Link
                    target='_blank'
                    rel='noopener'
                    href='https://github.com/signup'
                  >
                    GitHub
                  </Link>{' '}
                  or{' '}
                  <Link
                    target='_blank'
                    rel='noopener'
                    href='https://orcid.org/signin'
                  >
                    ORCID
                  </Link>
                  . If you already have an account you can use it to log in to
                  C4I via the ESGF{' '}
                  {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                  <Link
                    component='button'
                    variant='subtitle1'
                    style={{ paddingBottom: '2.5px' }}
                    onClick={LoginClickHandler}
                  >
                    login page
                  </Link>{' '}
                  (Single Sign On).
                </p>
              </Typography>
              <Typography variant='h5' color='textSecondary' paragraph>
                2. Register for all C4I features
              </Typography>
              <Typography variant='body1' color='textSecondary' paragraph>
                <p style={{ paddingLeft: 10 }}>
                  Send an email to <b>c4i(at)knmi.nl</b> from same email address
                  you have used in the previous step. Please, indicate in the
                  text of the email your affiliation, your role in the
                  organisation and background. Once we have upgraded the
                  account, you will have full access to the portal.
                </p>
              </Typography>
            </div>
            <Typography variant='body1' color='textSecondary' paragraph>
              <p>If you are already registered, click the login button.</p>
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Register;
