import React from 'react';
import { OPENID } from '../App/ApplicationConfig';
import { OpenidUser } from '../../types/dataTypes';

const classes = {
  root: {
    flexGrow: 1,
  },
};

export const LoginClickHandler = (): void => {
  const array = new Uint8Array(30);
  window.crypto.getRandomValues(array);
  let state = '';
  array.forEach((e) => {
    state += (e % 16).toString(16);
  });
  sessionStorage.setItem('openidState', state);
  window.location.href = `${OPENID.authorization_endpoint}?state=${state}`;
};

export const LogoutClickHandler = (): void => {
  window.location.href = OPENID.end_session_endpoint;
};

export const Login: React.FC = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const entries = urlParams.entries();
  const body = {};
  Array.from(entries).forEach((entry: [string, string]) => {
    [, body[entry[0]]] = entry;
  });
  fetch(OPENID.token_endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
    credentials: 'include',
  })
    .then((res) => (res.status === 200 ? res.json() : null))
    .then((res: OpenidUser) => {
      if (res.registered) {
        window.location.replace('search');
      } else {
        window.location.replace('/');
      }
    })
    .catch(() => {
      window.location.replace('/');
    });
  return <div style={classes.root} />;
};
