import React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

export const C4IUserGuide: React.FC = () => {
  const [userguide, setUserguide] = React.useState<string>('');
  React.useEffect(() => {
    fetch('C4IUserguide.md')
      .then((res) => (res.status === 200 ? res.text() : null))
      .then((res) => setUserguide(res))
      .catch((err) => console.error(err));
  }, []);

  return <ReactMarkdown rehypePlugins={[rehypeRaw]}>{userguide}</ReactMarkdown>;
};

export const SwirrlUserGuide: React.FC = () => {
  const [userguide, setUserguide] = React.useState<string>('');
  React.useEffect(() => {
    fetch('SwirrlUserguide.md')
      .then((res) => (res.status === 200 ? res.text() : null))
      .then((res) => setUserguide(res))
      .catch((err) => console.error(err));
  }, []);

  return <ReactMarkdown rehypePlugins={[rehypeRaw]}>{userguide}</ReactMarkdown>;
};
