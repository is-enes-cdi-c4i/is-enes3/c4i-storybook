import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Grid,
  Typography,
  Container,
} from '@mui/material';
import { ProjectType } from '../../../types/searchTypes';
import { defaultProjects } from '../../../config/projects';

const classes = {
  heroContent: {
    padding: '32px 0px 32px',
  },
  cardGrid: {
    paddingTop: '0px',
    paddingBottom: '0px',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
};

export const ProjectSelectionArea = (): JSX.Element => {
  const navigate = useNavigate();

  return (
    <Grid container spacing={6}>
      {defaultProjects.map((card: ProjectType) => (
        <Grid item key={card.title} xs={12} sm={6} md={4}>
          <Card
            sx={{ ...classes.card, opacity: card.disabled ? 0.3 : 1 }}
            raised
          >
            <CardActionArea
              onClick={(): void => {
                if (!card || card.disabled || !card.title) return;
                // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
                navigate(`/search?project=${card.title}`);
                window.location.reload();
              }}
            >
              <CardMedia
                sx={classes.cardMedia}
                image={card.thumbnail}
                title={card.title}
              />
              <CardContent sx={classes.cardContent}>
                <Typography gutterBottom variant='h5' component='h2'>
                  {card.title}
                </Typography>
                <Typography>{card.description}</Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export const ProjectSelector = (): JSX.Element => {
  return (
    <>
      <div style={classes.heroContent}>
        <Container maxWidth='md'>
          <Typography
            variant='h3'
            align='center'
            color='textPrimary'
            gutterBottom
          >
            Select a project
          </Typography>
          <Typography
            variant='h5'
            align='center'
            color='textSecondary'
            paragraph
          >
            You can search the Earth System Grid Federation for various kinds of
            climate data. Please select the project you are looking for.
          </Typography>
        </Container>
      </div>
      <Container sx={classes.cardGrid} maxWidth='md'>
        <ProjectSelectionArea />
      </Container>
    </>
  );
};
export default ProjectSelector;
