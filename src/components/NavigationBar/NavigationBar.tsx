import React from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import { Typography, Link, AppBar, Toolbar } from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Button from '@mui/material/Button';
import Menu, { menuClasses } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { LoginClickHandler, LogoutClickHandler } from './Login';
import { OpenidUser } from '../../types/dataTypes';
import { GFORM } from '../App/ApplicationConfig';

const classes = {
  root: {
    flexGrow: 1,
  },
  menuLink: {
    marginRight: '16px',
  },
  linkTypography: {
    fontSize: '16px',
  },
  login: {
    marginRight: '0px',
    marginLeft: 'auto',
    display: 'flex',
    flexDirection: 'row',
  },
  appBar: {
    minHeight: '40px',
    height: '40px',
  },
  toolBar: {
    minHeight: '40px !important',
    height: '40px',
  },
  menu: {
    marginTop: '45px',
  },
  buttonStyle: {
    color: 'white',
    textTransform: 'none',
    fontSize: '16px',
    fontWeight: 400,
  },
};

type NavBarProps = {
  user: OpenidUser;
};

const LinkBehavior = (to: string) =>
  // eslint-disable-next-line react/display-name,@typescript-eslint/no-explicit-any
  React.forwardRef<any>((props, ref) => (
    <RouterLink ref={ref} to={to} {...props} />
  ));

const navItemLabels = [
  {
    path: '/',
    label: 'Home',
  },
  {
    path: '/project',
    label: 'Data Discovery',
  },
];

type NavLinkProps = {
  navItem: {
    path: string;
    label: string;
  };
};

const AccountMenu = ({ user }: NavBarProps) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const serviceUrl = user.notebookURL;

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  if (!user && !user.preferred_username) {
    return null;
  }

  return (
    <div>
      <Button
        onClick={handleClick}
        startIcon={<AccountCircle style={{ fontSize: 30 }} />}
        endIcon={<ExpandMoreIcon style={{ fontSize: 25 }} />}
        style={{
          marginRight: '5px',
        }}
        sx={classes.buttonStyle}
        variant='text'
      >
        <span>
          {user.preferred_username} {!user.registered && '(unregistered)'}
        </span>
      </Button>
      <Menu
        id='AccountMenu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        sx={{
          ...classes.menu,
          [`& .${menuClasses.paper}`]: { top: '87px !important' },
        }}
      >
        <MenuItem
          disabled={user.registered}
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          component={RouterLink}
          to='/register'
          onClick={handleClose}
        >
          Register account
        </MenuItem>
        <MenuItem
          disabled={!user.notebookId}
          onClick={(): void => {
            window.open(serviceUrl, '_blank');
          }}
        >
          My notebook
        </MenuItem>
        <MenuItem onClick={LogoutClickHandler}>Logout</MenuItem>
      </Menu>
    </div>
  );
};

const NavLink = ({ navItem }: NavLinkProps) => {
  const location = useLocation();
  const [shortPathName, setShortPathName] = React.useState<string>('');
  React.useEffect(() => {
    setShortPathName(location.pathname.replace('/c4i-frontend', ''));
  }, [location.pathname]);
  return (
    <>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <Link
        key={navItem.path}
        color='inherit'
        component={LinkBehavior(navItem.path)}
        underline={
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          (navItem.path === '/' && shortPathName === '') ||
          shortPathName === navItem.path ||
          shortPathName === `${navItem.path}/`
            ? 'always'
            : 'none'
        }
        sx={classes.menuLink}
      >
        <Typography
          onClick={(): void => {
            window.location.hash = '';
          }}
          sx={classes.linkTypography}
        >
          {navItem.label}
        </Typography>
      </Link>
    </>
  );
};

const HelpMenu = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        onClick={handleClick}
        style={{
          marginLeft: '-15px',
        }}
        sx={classes.buttonStyle}
        variant='text'
      >
        Help
      </Button>
      <Menu
        id='HelpMenu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        sx={{ [`& .${menuClasses.paper}`]: { top: '87px' } }}
      >
        <MenuItem onClick={handleClose}>
          <NavLink navItem={{ label: 'C4I Portal', path: '/helpC4I' }} />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink navItem={{ label: 'C4I Workspace', path: '/helpSwirrl' }} />
        </MenuItem>
      </Menu>
    </div>
  );
};

export const NavigationBar: React.FC<NavBarProps> = ({ user }: NavBarProps) => {
  return (
    <div style={classes.root}>
      <AppBar position='static' sx={classes.appBar}>
        <Toolbar sx={classes.toolBar}>
          {navItemLabels.map((navItem) => (
            <NavLink key={navItem.label} navItem={navItem} />
          ))}
          <HelpMenu />
          <Typography sx={classes.linkTypography}>
            <Link
              sx={classes.menuLink}
              href={GFORM}
              target='_blank'
              rel='noopener'
              color='inherit'
              underline='none'
            >
              Feedback
            </Link>
          </Typography>
          <NavLink navItem={{ label: 'Register', path: '/register' }} />
          {user ? (
            <div style={classes.login as React.CSSProperties}>
              <AccountMenu user={user} />
            </div>
          ) : (
            <div style={classes.login as React.CSSProperties}>
              <Button
                size='small'
                sx={classes.buttonStyle}
                variant='text'
                onClick={LoginClickHandler}
              >
                Login
              </Button>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default NavigationBar;
