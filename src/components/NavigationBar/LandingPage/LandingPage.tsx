import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Paper, Grid, Link, Typography, Divider, Button } from '@mui/material';
import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';

import { OpenidUser } from '../../../types/dataTypes';

const classes = {
  root: {
    flexGrow: 1,
  },
  title: {
    color: 'rgba(0, 0, 0, 0.87)',
    marginTop: '5px',
    marginBottom: '-5px',
  },
  subtitle: {
    margin: '10px 0px 5px 5px',
  },
  paragraph: {
    color: 'rgba(0, 0, 0, 0.6)',
    padding: '0 5px',
  },
  card: {
    padding: '10px 40px',
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.6)',
    height: '450px',
    width: '350px',
    position: 'relative',
  },
  cardSubtitle: { marginBottom: '15px', marginTop: '-5px' },
  cardList: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'left',
    padding: '10px 0 10px 20px',
    border: '1px solid #e6e6e6',
  },
  cardRow: { padding: '8px' },
  checkIcon: { verticalAlign: 'middle', paddingRight: '8px' },
};

type LandingPageProps = {
  user: OpenidUser;
};

const Aims = () => {
  return (
    <Typography sx={classes.paragraph}>
      The aim of Climate4impact portal is to enhance the use of climate research
      data. It is currently under development in the European project IS-ENES3.
      Climate4impact is connected to the
      <Link
        href='https://esgf.llnl.gov/mission.html'
        target='_blank'
        rel='noopener'
        style={{ margin: '0 10px' }}
      >
        Earth System Grid Federation (ESGF) infrastructure
      </Link>
      using ESGF search and thredds catalogs. The portal aims to support climate
      change impact modellers, impact and adaptation consultants, as well anyone
      else wanting to use climate change data.
    </Typography>
  );
};

export const LandingPage: React.FC<LandingPageProps> = ({
  user,
}: LandingPageProps) => {
  return (
    <div style={classes.root}>
      <Grid container spacing={5}>
        <Grid item xs={12}>
          <Typography
            sx={classes.title}
            variant='h3'
            align='center'
            gutterBottom
          >
            {user && user.preferred_username ? (
              <span>
                Welcome to Climate4Impact,
                <span
                  style={{ color: '#3E51B5', fontStyle: 'italic' }}
                >{` ${user.preferred_username}`}</span>
              </span>
            ) : (
              <span>Welcome to Climate4Impact!</span>
            )}
          </Typography>
          <Typography variant='h6' sx={classes.subtitle}>
            Aims
          </Typography>
          <Aims />
          <Typography variant='h6' sx={classes.subtitle}>
            Choose your account type
          </Typography>
          <Typography sx={classes.paragraph}>
            Accounts are free of charge. Guest accounts can browse the data and
            download link collections to be used in download managers.
            Registered users have access to the full range of services providing
            a fully managed Jupyter notebook environment for scientific
            research.
          </Typography>
        </Grid>
        <Grid container sx={{ justifyContent: 'center' }} spacing={6}>
          <Grid item xs={4}>
            <Paper sx={classes.card}>
              <Typography sx={classes.title} variant='h6'>
                Guest
              </Typography>
              <Typography sx={classes.cardSubtitle} variant='subtitle1'>
                Limited features
              </Typography>
              <div style={{ margin: '0 auto' }}>
                <div style={classes.cardList as React.CSSProperties}>
                  <div style={classes.cardRow}>
                    <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                    Browse ESGF Data
                  </div>
                  <div style={classes.cardRow}>
                    <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                    Download file links
                  </div>
                </div>
              </div>
              <div
                style={{
                  position: 'absolute',
                  width: 350,
                  bottom: 10,
                }}
              >
                <Button
                  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                  component={RouterLink}
                  to='/search'
                  variant='contained'
                  color='primary'
                >
                  Browse Data
                </Button>
              </div>
            </Paper>
          </Grid>
          <Grid item xs={3}>
            <Paper sx={classes.card}>
              <Typography sx={classes.title} variant='h6'>
                Registered User
              </Typography>
              <Typography sx={classes.cardSubtitle} variant='subtitle1'>
                Full feature access
              </Typography>
              <div style={classes.cardList as React.CSSProperties}>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Browse ESGF Data
                </div>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Download file links
                </div>
                <Divider style={{ margin: 10 }} />

                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Access data in
                  <Link
                    href='https://jupyter.org/'
                    target='_blank'
                    rel='noopener'
                    style={{ margin: '0 4px' }}
                  >
                    Jupyter
                  </Link>
                  workspace
                </div>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Access scientific
                  <Link
                    href='https://gitlab.com/is-enes-cdi-c4i/notebooks'
                    target='_blank'
                    rel='noopener'
                    style={{ margin: '0 4px' }}
                  >
                    notebook presets
                  </Link>
                </div>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Subset large datasets
                </div>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Create
                  <Link
                    href='https://mybinder.org/'
                    target='_blank'
                    rel='noopener'
                    style={{ margin: '0 4px' }}
                  >
                    Binder
                  </Link>
                  snapshots of your work
                </div>
                <div style={classes.cardRow}>
                  <CheckBoxOutlinedIcon sx={classes.checkIcon} />
                  Trace and rollback operations
                </div>
              </div>
              <Button
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                component={RouterLink}
                to='/register'
                variant='contained'
                color='primary'
                style={{ marginTop: 15 }}
              >
                Register
              </Button>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};
