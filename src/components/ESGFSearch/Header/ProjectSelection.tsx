import React from 'react';
import {
  DialogTitle,
  DialogContent,
  Dialog,
  DialogActions,
  Button,
} from '@mui/material';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { ProjectSelectionArea } from '../../NavigationBar/ProjectSelector/ProjectSelector';

const classes = {
  list: {
    width: '250px',
  },
  fullList: {
    width: 'auto',
  },
  iconChecked: {
    color: '#26a22a',
  },
  icon: {
    color: '#bdbdbd',
  },
  buttonGroup: {
    height: '120px',
    overflow: 'hidden',
    padding: '5px 0px',
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    color: '#3E51B5',
  },
  dialogPaper: {
    minHeight: '60vh',
    maxHeight: '60vh',
  },
  dialogActions: {
    display: 'flex',
    justifyContent: 'space-around',
    borderTop: '1px solid #BDBDBD',
    paddingTop: '10px',
  },
  dialogTitle: {
    textAlign: 'center',
    boxShadow: '0px 0px 4px 1px #BDBDBD',
  },
  dialogContent: {
    textAlign: 'center',
    marginTop: '30px',
  },
  menu: {
    minWidth: '250px',
    marginTop: '40px',
  },
  heroContent: {
    padding: '32px 0px 32px',
  },
  cardGrid: {
    paddingTop: '0px',
    paddingBottom: '0px',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    raised: true,
  },
  cardDisabled: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    raised: false,
    opacity: 0.3,
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  closeButton: {
    position: 'absolute',
    right: '8px',
    top: '8px',
    color: '#9e9e9e',
  },
};

type Props = {
  openProjectSelection: boolean;
  setOpenProjectSelection: (open: boolean) => void;
};

export const ProjectSelection = ({
  openProjectSelection,
  setOpenProjectSelection,
}: Props): JSX.Element => {
  return (
    <Dialog
      maxWidth='xl'
      aria-labelledby='confirmation-dialog-title'
      open={openProjectSelection}
      sx={{ '& .MuiDialog-paper': classes.dialogPaper }}
      onClose={() => setOpenProjectSelection(false)}
    >
      <DialogTitle id='dialog-title' sx={classes.dialogTitle}>
        Available Projects
        <IconButton
          aria-label='close'
          sx={classes.closeButton}
          onClick={() => setOpenProjectSelection(false)}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <DialogContent sx={classes.dialogContent}>
        <ProjectSelectionArea />
      </DialogContent>
      <DialogActions sx={classes.dialogActions}>
        <Button
          autoFocus
          onClick={() => setOpenProjectSelection(false)}
          color='secondary'
          variant='text'
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};
