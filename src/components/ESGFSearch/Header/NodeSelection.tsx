import React from 'react';
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  dialogClasses,
  Table,
  Checkbox,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  TableSortLabel,
  Typography,
} from '@mui/material';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import {
  stableSort,
  getComparator,
  Order,
} from '../../../utils/muiTableSorting';
import { ESGFNodeList } from '../../../config/customSelections/datanodes';

const classes = {
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: '8px',
  },
  table: {
    minWidth: '500px',
  },
  visuallyHidden: {
    border: '0px',
    clip: 'rect(0 0 0 0)',
    height: '1px',
    margin: '-1px',
    overflow: 'hidden',
    padding: '0px',
    position: 'absolute',
    top: '20px',
    width: '1px',
  },
  dialogPaper: {
    minHeight: '80vh',
    maxHeight: '80vh',
  },
  dialogActions: {
    display: 'flex',
    justifyContent: 'space-around',
    borderTop: '1px solid #BDBDBD',
    paddingTop: '10px',
  },
  dialogTitle: {
    textAlign: 'center',
    boxShadow: '0px 0px 4px 1px #BDBDBD',
  },
  dialogContent: {
    textAlign: 'center',
  },
  closeButton: {
    position: 'absolute',
    right: '5px',
    top: '5px',
    color: 'grey',
  },
  rookFilter: {
    marginLeft: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
  },
};

type Props = {
  // eslint-disable-next-line react/no-unused-prop-types
  selectedNodes: string[];
  setNodes: (nodes: string[]) => void;
  openNodeSelection: boolean;
  setOpenNodeSelection: (open: boolean) => void;
};

export const rookSupportingNodes = [
  'esgf1.dkrz.de',
  'esgf3.dkrz.de',
  'esgf-node2.cmcc.it',
]; // Register nodes here that support rook wps
export const onlyRookNodesSelected = (selection: string[]): boolean =>
  selection.every((val) => rookSupportingNodes.includes(val));

enum ROW {
  NODE = 'node',
  SUBSETTING = 'subsetting',
}
interface EnhancedTableProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
  order: Order;
  orderBy: string;
  onRequestSort: (event: React.MouseEvent<unknown>, property: ROW) => void;
}

const EnhancedTableHead = (props: EnhancedTableProps) => {
  const {
    onSelectAllClick,
    numSelected,
    rowCount,
    order,
    orderBy,
    onRequestSort,
  } = props;

  const createSortHandler = (property: ROW) => (event) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding='checkbox'>
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            color='primary'
          />
        </TableCell>
        <TableCell align='left'>Node</TableCell>
        <TableCell
          align='left'
          sortDirection={orderBy === ROW.SUBSETTING ? order : false}
        >
          <TableSortLabel
            active={orderBy === ROW.SUBSETTING}
            direction={orderBy === ROW.SUBSETTING ? order : 'asc'}
            onClick={createSortHandler(ROW.SUBSETTING)}
          >
            Subsetting Mode
            {ROW.SUBSETTING ? (
              <span style={classes.visuallyHidden as React.CSSProperties}>
                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
              </span>
            ) : null}
          </TableSortLabel>
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

const EnhancedTable = ({
  rows,
  localState,
  setLocalState,
}: {
  rows: string[];
  localState: string[];
  setLocalState: (nodes: string[]) => void;
}): JSX.Element => {
  const [order, setOrder] = React.useState<Order>('desc');
  const [orderBy, setOrderBy] = React.useState<ROW>(ROW.SUBSETTING);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: ROW
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectionChange = (
    event: React.MouseEvent<unknown>,
    text: string
  ) => {
    if (localState.includes(text)) {
      setLocalState(localState.filter((nodes) => nodes !== text));
    } else {
      setLocalState([...localState, text]);
    }
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      setLocalState(ESGFNodeList);
      return;
    }
    setLocalState([]);
  };

  const isSelected = (name: string) => localState.indexOf(name) !== -1;

  const data = rows.map((row) => ({
    node: row,
    subsetting: rookSupportingNodes.includes(row)
      ? 'Rook WPS'
      : 'Not Available',
  }));

  return (
    <div style={classes.root}>
      <TableContainer>
        <Table
          sx={classes.table}
          aria-labelledby='tableTitle'
          size='medium'
          aria-label='enhanced table'
        >
          <EnhancedTableHead
            numSelected={localState.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={ESGFNodeList.length}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {stableSort(data, getComparator(order, orderBy)).map(
              (row, index) => {
                const isItemSelected = isSelected(row.node as string);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    style={{ backgroundColor: 'white' }}
                    onClick={(event) =>
                      handleSelectionChange(event, row.node as string)
                    }
                    role='checkbox'
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.node as string}
                    selected={isItemSelected}
                  >
                    <TableCell padding='checkbox'>
                      <Checkbox checked={isItemSelected} color='primary' />
                    </TableCell>
                    <TableCell
                      component='th'
                      id={labelId}
                      scope='row'
                      style={{ paddingLeft: 15 }}
                    >
                      {row.node}
                    </TableCell>
                    <TableCell component='th' id={labelId} scope='row'>
                      {row.subsetting}
                    </TableCell>
                  </TableRow>
                );
              }
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export const NodeSelection = ({
  setNodes,
  openNodeSelection,
  setOpenNodeSelection,
}: Props): JSX.Element => {
  const [localState, setLocalState] = React.useState(ESGFNodeList);
  const setRookOnly = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      setLocalState(rookSupportingNodes);
      return;
    }
    setLocalState(ESGFNodeList);
  };

  return (
    <Dialog
      maxWidth='xl'
      aria-labelledby='confirmation-dialog-title'
      open={openNodeSelection}
      sx={{ [`& .${dialogClasses.paper}`]: classes.dialogPaper }}
      onClose={() => setOpenNodeSelection(false)}
    >
      <DialogTitle id='dialog-title' sx={classes.dialogTitle}>
        Available ESGF Nodes
        <IconButton
          aria-label='close'
          sx={classes.closeButton}
          onClick={() => setOpenNodeSelection(false)}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={classes.dialogContent}>
        <div style={classes.rookFilter}>
          <Typography>
            <Checkbox
              checked={onlyRookNodesSelected(localState)}
              color='default'
              onChange={setRookOnly}
            />
            Select & enable Rook WPS subsetting
          </Typography>
        </div>
        <EnhancedTable
          rows={ESGFNodeList}
          {...{
            localState,
            setLocalState,
          }}
        />
      </DialogContent>
      <DialogActions sx={classes.dialogActions}>
        <Button
          onClick={() => {
            setNodes(localState);
            setOpenNodeSelection(false);
          }}
          disabled={localState.length < 1}
          color='primary'
          variant='contained'
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EnhancedTable;
