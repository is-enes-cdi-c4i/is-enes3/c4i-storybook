import React from 'react';
import { Button, FormControlLabel, Switch, Typography } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { ESGFNodeList } from '../../../config/customSelections/datanodes';
import { ProjectType, PROJECTNAME } from '../../../types/searchTypes';
import { CustomizedTooltip } from '../../GenericUI/CustomizedTooltip';

const classes = {
  facetSelectorHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '5px 0px',
    width: '100%',
    position: 'absolute',
    borderBottom: '1px solid #BDBDBD',
    zIndex: 900,
    background: '#fff',
    paddingLeft: '170px',
  },
  menuButton: {
    minWidth: '150px',
    color: '#3E51B5',
  },
  viewSwitch: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    color: '#BDBDBD',
    minWidth: '150px',
    padding: '0 15px',
    marginRight: '190px',
  },
};

type SearchHeaderProps = {
  enableAdvancedView: boolean;
  selectedNodes: string[];
  setEnableAdvancedView: (enableAdvancedView: boolean) => void;
  openProjectSelection: boolean;
  setOpenProjectSelection: (open: boolean) => void;
  openNodeSelection: boolean;
  setOpenNodeSelection: (open: boolean) => void;
  project: ProjectType;
};

export const SearchHeader = ({
  enableAdvancedView,
  setEnableAdvancedView,
  selectedNodes,
  openProjectSelection,
  setOpenProjectSelection,
  openNodeSelection,
  setOpenNodeSelection,
  project,
}: SearchHeaderProps): React.ReactElement => {
  const DEFAULTNODES = ESGFNodeList;

  const projectButtonText = () => {
    switch (project.title) {
      case PROJECTNAME.CMIP5:
        return 'Project: CMIP 5';
      case PROJECTNAME.CMIP6:
        return 'Project: CMIP 6';
      case PROJECTNAME.CORDEX:
        return 'Project: Cordex';
      default:
        return null;
    }
  };
  const nodeButtonText = () => {
    switch (selectedNodes.length) {
      case DEFAULTNODES.length:
        return 'Nodes & Subsetting';
      case 1:
        return `Node: ${selectedNodes[0]}`;
      default:
        return `Nodes: Custom (${selectedNodes.length})`;
    }
  };

  return (
    <div style={classes.facetSelectorHeader as React.CSSProperties}>
      <div>
        <CustomizedTooltip
          title='Switch between projects. Some facets are exclusive to a specific project.'
          placement='bottom'
        >
          <Button
            sx={classes.menuButton}
            onClick={() => setOpenProjectSelection(true)}
            startIcon={
              openProjectSelection ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )
            }
          >
            {projectButtonText()}
          </Button>
        </CustomizedTooltip>
        <CustomizedTooltip
          title='Select the ESGF Nodes to use for data retrieval. Nodes may vary in performance and available data.'
          placement='bottom'
        >
          <Button
            sx={classes.menuButton}
            style={{ marginLeft: 10 }}
            onClick={() => setOpenNodeSelection(true)}
            startIcon={
              openNodeSelection ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )
            }
          >
            {nodeButtonText()}
          </Button>
        </CustomizedTooltip>
      </div>
      <div style={classes.viewSwitch as React.CSSProperties}>
        <CustomizedTooltip
          title='A subset of common facets available on the selected node(s).'
          placement='bottom'
        >
          <Typography
            style={{
              paddingRight: 15,
              color: enableAdvancedView ? '#BDBDBD' : '#3E51B5',
            }}
            variant='button'
          >
            Scoped View
          </Typography>
        </CustomizedTooltip>
        <FormControlLabel
          control={
            <Switch
              checked={enableAdvancedView}
              onChange={(): void => {
                setEnableAdvancedView(!enableAdvancedView);
              }}
              value='checkedB'
              color='primary'
              style={{ fontWeight: 500 }}
            />
          }
          label=''
        />
        <CustomizedTooltip
          title='All facets available on the selected node(s).'
          placement='bottom'
        >
          <Typography
            variant='button'
            style={{
              marginLeft: -15,
              color: enableAdvancedView ? '#3E51B5' : '#BDBDBD',
            }}
          >
            Extended View
          </Typography>
        </CustomizedTooltip>
      </div>
    </div>
  );
};
