import React from 'react';
import { Paper, Tabs, Tab } from '@mui/material';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import {
  ProjectType,
  FacetType,
  SearchQuery,
  QueryItem,
} from '../../../types/searchTypes';
import { ConditionalWrapper } from '../../GenericUI/ConditionalWrapper';

const classes = {
  root: {
    backgroundColor: '#f6f6f6',
  },
  chip: {
    marginRight: '8px',
    marginBottom: '8px',
    backgroundColor: '#f5f5f5',
  },
  tab: {
    borderBottom: '1px solid #BDBDBD',
    minHeight: '100px',
    backgroundColor: '#FBFBFB',
  },
  selectedTab: {
    minHeight: '100px',
    backgroundColor: 'white',
  },
  iconChecked: {
    color: '#26a22a',
  },
  icon: {
    color: '#bdbdbd',
  },
};

interface RenderFacetsBarProps {
  projectType: ProjectType;
  onFacetClick: (facet: FacetType) => void;
  searchQuery: SearchQuery;
}
const FacetSelectionSideBar = ({
  projectType,
  onFacetClick,
  searchQuery,
}: RenderFacetsBarProps): JSX.Element => {
  const [activeTab, setActiveTab] = React.useState(0);

  if (!projectType) {
    return <div>Property [projectType] not specified</div>;
  }
  const steps = projectType.mainFacets;

  const handleStep = (tab: number): void => {
    setActiveTab(tab);
    onFacetClick(projectType.mainFacets[tab]);
  };
  if (!steps) return null;

  const tabCompleted = (itemlist: QueryItem[], facet: FacetType) => {
    return itemlist.filter((item) => item.facetId === facet.id).length > 0;
  };

  return (
    <div style={classes.root}>
      <Paper square style={{ backgroundColor: '#eeeeee' }}>
        <Tabs
          orientation='vertical'
          value={activeTab}
          indicatorColor='primary'
          textColor='primary'
          centered
        >
          {steps.map((facet, index) => (
            <ConditionalWrapper
              key={facet.id}
              condition={index === activeTab}
              // eslint-disable-next-line react/no-unstable-nested-components
              wrapper={(children) => (
                <Paper
                  elevation={6}
                  style={{
                    color: 'darkblue',
                    textAlign: 'center',
                    minWidth: '160px',
                  }}
                >
                  {children}
                </Paper>
              )}
            >
              <Tab
                key={facet.id}
                onClick={() => {
                  handleStep(index);
                }}
                label={facet.title}
                sx={index === activeTab ? classes.selectedTab : classes.tab}
                icon={
                  tabCompleted(searchQuery.selectedItems, facet) ? (
                    <CheckCircleIcon sx={classes.iconChecked} />
                  ) : (
                    <CheckCircleOutlineIcon sx={classes.icon} />
                  )
                }
              />
            </ConditionalWrapper>
          ))}
        </Tabs>
      </Paper>
    </div>
  );
};
export default FacetSelectionSideBar;
