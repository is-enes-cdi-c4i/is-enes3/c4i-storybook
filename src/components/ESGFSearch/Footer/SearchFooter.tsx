import React, { useEffect } from 'react';
import { Button } from '@mui/material';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import FolderIcon from '@mui/icons-material/Folder';
import OpenInBrowserIcon from '@mui/icons-material/OpenInBrowser';
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
import {
  OPERATION,
  Operation,
  PROJECTNAME,
  ProjectType,
  SearchQuery,
  SUBSETTING,
} from '../../../types/searchTypes';
import { OpenidUser } from '../../../types/dataTypes';
import { onlyRookNodesSelected } from '../Header/NodeSelection';
import { CustomizedTooltip } from '../../GenericUI/CustomizedTooltip';

export const PROJECTFACET = 'project';

const classes = {
  facetSelectorFooter: {
    gridArea: 'facetSelectorFooter',
    backgroundColor: '#f5f5f5',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '5px 15px',
    zIndex: 200,
    boxShadow:
      '0px -1px 2px -1px rgba(0, 0, 0, 0.2),0px -1px 3px 0px rgba(0, 0, 0, 0.14), 0px -1px 5px 0px rgba(0, 0, 0, 0.12)',
  },
  button: {
    marginLeft: '10px',
  },
  buttonDiv: {
    display: 'inline-block',
  },
};

type SearchFooterProps = {
  user: OpenidUser;
  setResultsWindow: ({
    open,
    operation,
    subsettingMode,
  }: {
    open: boolean;
    operation: Operation;
    subsettingMode: SUBSETTING;
  }) => void;
  project: ProjectType;
  selectedNodes: string[];
  searchQuery: SearchQuery;
};

export const SearchFooter = ({
  user,
  setResultsWindow,
  project,
  selectedNodes,
  searchQuery,
}: SearchFooterProps): React.ReactElement => {
  // Lock buttons if no facets are selected:
  const [buttonLocked, lockButtonNoFacet] = React.useState<boolean>(true);
  useEffect(() => {
    lockButtonNoFacet(
      !(
        searchQuery.selectedItems.filter(
          (facet) => facet.facetId !== PROJECTFACET
        ).length > 0
      )
    );
  }, [searchQuery.selectedItems]);

  /* CMIP 5 datasets contain a variable array, if multiple variable facets are selected,
  it becomes impossible to determine which one to send along in the request - lock button: */
  const [rookLocked, lockRookCMIP5] = React.useState<boolean>(false);
  useEffect(() => {
    const variableFacetCount =
      searchQuery.selectedItems.filter((facet) => facet.facetId === 'variable')
        .length || 0;
    if (project.title === PROJECTNAME.CMIP5 && variableFacetCount > 1) {
      lockRookCMIP5(true);
    } else {
      lockRookCMIP5(false);
    }
  }, [searchQuery.selectedItems, project.title]);

  return (
    <div style={classes.facetSelectorFooter as React.CSSProperties}>
      <div style={{ minWidth: 300 }} />
      <div>
        {user && user.notebookId ? (
          <Button
            color='primary'
            startIcon={<OpenInBrowserIcon />}
            onClick={(): void => {
              window.open(user.notebookURL, '_blank');
            }}
          >
            Open your notebook
          </Button>
        ) : (
          <Button disabled color='primary' startIcon={<OpenInBrowserIcon />}>
            Open your notebook
          </Button>
        )}
        <Button
          color='primary'
          startIcon={<FolderOpenIcon />}
          style={{
            marginRight: 10,
          }}
          onClick={(): void => {
            window.open(
              'https://gitlab.com/is-enes-cdi-c4i/notebooks',
              '_blank'
            );
          }}
        >
          Scientific Presets
        </Button>
      </div>
      <div
        style={{
          marginRight: 10,
          minWidth: 300,
        }}
      >
        {onlyRookNodesSelected(selectedNodes) ? (
          <CustomizedTooltip
            title='Using Rook WPS subsetting. For CMIP5 only one "variable" facet may be selected.'
            placement='top'
          >
            <Button
              id='ROOKWPS_Subsetting'
              variant='contained'
              color='primary'
              disabled={!user || !user.registered || rookLocked || buttonLocked}
              sx={classes.button}
              startIcon={<AccountTreeIcon />}
              onClick={(): void => {
                setResultsWindow({
                  open: true,
                  operation: OPERATION.SUBSETTING,
                  subsettingMode: SUBSETTING.ROOK,
                });
              }}
            >
              Subsetting
            </Button>
          </CustomizedTooltip>
        ) : (
          <CustomizedTooltip
            title='No subsetting possible with current node selection. Change node selection to enable subsetting.'
            placement='top'
          >
            <div style={classes.buttonDiv}>
              <Button
                id='OPENDAP_Subsetting'
                variant='contained'
                color='primary'
                disabled={!user || !user.registered || buttonLocked || true}
                sx={classes.button}
                startIcon={<AccountTreeIcon />}
                onClick={(): void => {
                  setResultsWindow({
                    open: true,
                    operation: OPERATION.SUBSETTING,
                    subsettingMode: SUBSETTING.OPENDAP,
                  });
                }}
              >
                Subsetting
              </Button>
            </div>
          </CustomizedTooltip>
        )}
        <Button
          variant='contained'
          color='primary'
          disabled={buttonLocked}
          sx={classes.button}
          startIcon={<FolderIcon />}
          onClick={(): void => {
            setResultsWindow({
              open: true,
              operation: OPERATION.DOWNLOAD,
              subsettingMode: SUBSETTING.NONE,
            });
          }}
        >
          Download
        </Button>
      </div>
    </div>
  );
};
