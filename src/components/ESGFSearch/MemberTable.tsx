import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Checkbox from '@mui/material/Checkbox';
import { FacetType, SearchQuery } from '../../types/searchTypes';
import { stableSort, getComparator, Order } from '../../utils/muiTableSorting';

const classes = {
  root: {
    width: '100%',
  },
  cell: {
    borderBottom: 'none',
  },
  table: {
    minWidth: 350,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
};

interface Data {
  id: string;
  title: string;
  numResults: number;
}
interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'title',
    numeric: false,
    disablePadding: false,
    label: 'Member Id',
  },
  { id: 'numResults', numeric: true, disablePadding: false, label: 'Datasets' },
];

interface EnhancedTableProps {
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => void;
  order: Order;
  orderBy: string;
}

const EnhancedTableHead = (props: EnhancedTableProps) => {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler =
    (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding='checkbox' sx={classes.cell} />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={classes.cell}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span style={classes.visuallyHidden as React.CSSProperties}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

type MemberTableProps = {
  rows: Data[];
  searchQuery: SearchQuery;
  facetType: FacetType;
  handleToggle: (
    toggle: string
  ) => (event: React.MouseEvent<HTMLTableRowElement>) => void;
};

export const MemberTable = ({
  rows,
  searchQuery,
  facetType,
  handleToggle,
}: MemberTableProps): React.ReactElement => {
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div style={classes.root}>
      <TableContainer>
        <Table
          sx={classes.table}
          aria-labelledby='tableTitle'
          size='small'
          aria-label='enhanced table'
        >
          <EnhancedTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: Data, index) => {
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={handleToggle(row.title)}
                    role='checkbox'
                    tabIndex={-1}
                    key={row.title}
                  >
                    <TableCell padding='checkbox' sx={classes.cell}>
                      <Checkbox
                        color='primary'
                        checked={
                          searchQuery &&
                          searchQuery.selectedItems.filter(
                            (selectedItem) =>
                              selectedItem.facetId === facetType.id &&
                              selectedItem.fieldId === row.id
                          ).length > 0
                        }
                        inputProps={{ 'aria-labelledby': labelId }}
                        disableRipple
                      />
                    </TableCell>
                    <TableCell
                      sx={classes.cell}
                      component='th'
                      id={labelId}
                      style={{ paddingLeft: '20px' }}
                      scope='row'
                      padding='none'
                    >
                      {row.title}
                    </TableCell>
                    <TableCell
                      sx={classes.cell}
                      align='right'
                      style={{ color: '#888' }}
                    >
                      ({row.numResults || 0})
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 20, 50]}
        component='div'
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </div>
  );
};
