import React, { useEffect } from 'react';
import { SearchQuery, ProjectType } from '../../types/searchTypes';
import { getFacetsInfo } from '../../utils/getFacetsInfo';

interface ESGFSearchProps {
  searchQuery: SearchQuery;
  project: ProjectType;
}

const GetFacetInfoDemo = ({
  searchQuery,
  project,
}: ESGFSearchProps): JSX.Element => {
  // Similar to componentDidMount and componentDidUpdate:
  const [facetInfo, setFacetInfo] = React.useState<string>(null);
  useEffect(() => {
    getFacetsInfo(searchQuery, project, null)
      .then((result) => {
        console.log(result);
        setFacetInfo(JSON.stringify(result, null, 2));
      })
      .catch((err) => console.error(err));
  });

  return <pre style={{ overflow: 'scroll', height: '600px' }}>{facetInfo}</pre>;
};
export default GetFacetInfoDemo;
