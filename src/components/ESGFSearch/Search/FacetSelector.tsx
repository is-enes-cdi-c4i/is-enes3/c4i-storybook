import React from 'react';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  CardActionArea,
  CardActions,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Container,
  Checkbox,
  Button,
  SvgIcon,
  Tooltip,
} from '@mui/material';
import { ESMVALBASEURL } from '../../App/ApplicationConfig';
import { MemberTable } from '../MemberTable';
import {
  FacetType,
  FacetFieldType,
  SearchQuery,
  FacetDescription,
} from '../../../types/searchTypes';
import { FacetInfo, FacetResult } from '../../../utils/getFacetsInfo';

const classes = {
  root: {
    flex: 1,
  },
  list: {
    margin: '0px',
  },
  listItem: {
    margin: '0px',
    padding: '0px',
  },
  listItemIcon: {
    minWidth: '0px',
    height: '1em',
  },
  icon: {
    marginRight: '16px',
  },
  cardGrid: {
    paddingTop: '0px',
    paddingBottom: '0px',
    maxWidth: 'auto',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '15em',
    minWidth: '12em',
  },
  cardMedia: {
    title: {
      fontSize: '1em',
    },
    paddingLeft: '3rem',
  },
  cardContent: {
    flexGrow: 0,
    overflow: 'auto',
    maxHeight: '450px',
  },
  numResults: {
    color: '#888',
  },
  tooltip: {
    fontSize: '0.75rem',
  },
};

export interface FacetSelectorProps {
  facetType: FacetType;
  searchFacetResults: FacetInfo | null;
  searchQuery: SearchQuery;
  enableAdvancedView: boolean;
  changeFacetField: (
    facetId: string,
    facetName: string,
    checked: boolean
  ) => void;
}

/**
 * For a certain facet (variable, model, experiment) and a certain facetfield (tas, hus) return the long description
 * @param facetId (variable, model, experiment)
 * @param fieldId (tas, hus)
 */
const getLongNames = (
  descriptions: FacetDescription,
  fieldId: string
): string => {
  if (!descriptions || !fieldId) return fieldId;
  return descriptions[fieldId] ? descriptions[fieldId].title : fieldId;
};

const FacetSelector = ({
  searchQuery,
  changeFacetField,
  facetType,
  searchFacetResults,
  enableAdvancedView,
}: FacetSelectorProps): JSX.Element => {
  const { config: facetConfiguration } = facetType;

  const handleToggle = (value: string) => (): void => {
    const checked =
      searchQuery.selectedItems.filter(
        (item) => item.facetId === facetType.id && item.fieldId === value
      ).length > 0;
    if (changeFacetField) {
      changeFacetField(facetType.id, value, !checked);
    }
  };

  const facetSearchResult =
    searchFacetResults && searchFacetResults[facetType.id];

  const renderList = (
    items: FacetFieldType[],
    searchQueryArg: SearchQuery,
    facetTypeArg: FacetType
  ): JSX.Element => {
    const augmentedItems = items.map((item) => {
      return {
        id: item.id,
        title: item.title,
        numResults: facetSearchResult ? facetSearchResult[item.id] : -1,
      };
    });

    const isMemberFacet = facetTypeArg.id === 'member_id';

    const sortedItems = augmentedItems.sort((a, b) => {
      return b.numResults - a.numResults;
    });

    return isMemberFacet ? (
      <div style={classes.root}>
        <MemberTable
          rows={augmentedItems}
          handleToggle={handleToggle}
          searchQuery={searchQueryArg}
          facetType={facetTypeArg}
        />
      </div>
    ) : (
      <div style={classes.root}>
        <List sx={classes.list}>
          {sortedItems.map((item) => {
            const labelId = `checkbox-list-label-${item.id}`;
            const numResultsForFacetField: number = facetSearchResult
              ? facetSearchResult[item.id]
              : -1;

            const numFacet =
              numResultsForFacetField > 0
                ? numResultsForFacetField
                : numResultsForFacetField || '-';

            if (numResultsForFacetField === 0) return undefined;
            return (
              <ListItem
                disabled={numResultsForFacetField === undefined}
                sx={classes.listItem}
                key={item.id}
                role={undefined}
                dense
                button
                onClick={handleToggle(item.id)}
              >
                <ListItemIcon sx={classes.listItemIcon}>
                  <Checkbox
                    edge='start'
                    checked={
                      searchQueryArg &&
                      searchQueryArg.selectedItems.filter(
                        (selectedItem) =>
                          selectedItem.facetId === facetTypeArg.id &&
                          selectedItem.fieldId === item.id
                      ).length > 0
                    }
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                    color='primary'
                  />
                </ListItemIcon>
                <ListItemText
                  id={labelId}
                  primary={`${item.id} - ${item.title}`}
                />
                <span style={classes.numResults}>{`(${numFacet})`}</span>
              </ListItem>
            );
          })}
        </List>
      </div>
    );
  };

  /**
   * Creates a FacetFieldType[] from the given facetSearchResult, facetSearchResult come from the ESGF search query.
   * @param facetDescriptions
   * @param facetSearchResultArg
   */
  const makeItemsFromFacetSearchResult = (
    facetDescriptions: FacetDescription,
    facetSearchResultArg: FacetResult
  ): FacetFieldType[] => {
    if (!facetSearchResultArg) return [];
    return Object.keys(facetSearchResultArg).map((facet) => {
      return { id: facet, title: getLongNames(facetDescriptions, facet) };
    });
  };
  const isModelFacet = facetType.id === 'source_id' || facetType.id === 'model';
  const selectedOfFacetType = searchQuery.selectedItems.filter(
    (selectedItem) => selectedItem.facetId === facetType.id
  );
  return (
    <Container sx={classes.cardGrid}>
      <Grid container sx={{ justifyContent: 'center' }} spacing={4}>
        {(enableAdvancedView
          ? [
              {
                title: facetType.title,
                description: 'all',
                tooltip: 'all',
                titleBGColor: facetConfiguration[0].titleBGColor,
                items: null, // Items as they come from the node
                descriptions: facetConfiguration[0].descriptions,
              },
            ]
          : facetConfiguration
        ).map((card) => (
          <Grid
            item
            key={card.title}
            xs={12}
            sm={6}
            md={enableAdvancedView ? 12 : facetType.gridMd || 3}
          >
            <Card sx={classes.card} raised>
              <CardActionArea>
                <SvgIcon
                  style={{
                    float: 'right',
                    overflow: 'none',
                    position: 'absolute',
                    color: 'white',
                    margin: '20px 30px auto 1rem',
                  }}
                >
                  {' '}
                  <path d={card.icon} />
                </SvgIcon>
                <CardHeader
                  sx={classes.cardMedia}
                  title={card.title}
                  style={{
                    backgroundColor: card.titleBGColor,
                    color: 'white',
                  }}
                />
                <CardContent sx={classes.cardContent}>
                  {card.items
                    ? renderList(card.items, searchQuery, facetType)
                    : renderList(
                        makeItemsFromFacetSearchResult(
                          card.descriptions,
                          facetSearchResult
                        ),
                        searchQuery,
                        facetType
                      )}
                </CardContent>
                {isModelFacet && (
                  <CardActions>
                    <Tooltip
                      sx={{ '& .MuiTooltip-tooltip': classes.tooltip }}
                      title='Interactive comparison of models’ bias and projected changes on Temperature and Precipitation variables.'
                    >
                      <Button
                        size='small'
                        color='primary'
                        onClick={(): void => {
                          const project = searchQuery.selectedItems.find(
                            (selectedItem) => selectedItem.facetId === 'project'
                          ).fieldId;
                          const url = `${ESMVALBASEURL}?project=${project}${selectedOfFacetType
                            .map(
                              (selectedItem) =>
                                `&dataset=${selectedItem.fieldId}`
                            )
                            .join('')}`;
                          window.open(url, '_blank');
                        }}
                      >
                        Compare model performance
                      </Button>
                    </Tooltip>
                  </CardActions>
                )}
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};
export default FacetSelector;
