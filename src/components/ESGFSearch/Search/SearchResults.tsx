import React, { useContext } from 'react';
import { Chip, LinearProgress, Typography, Divider } from '@mui/material';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
import { DATASETLIMIT } from '../../App/ApplicationConfig';
import {
  DatasetSelectionCtx,
  FileSelectionCtx,
} from '../../App/ApplicationContext';
import { DatasetTable } from '../ResultsTable/DatasetTable';
import { DuplicatesInSelectionAlert } from '../../GenericUI/Alerts';
import { CustomizedTooltip } from '../../GenericUI/CustomizedTooltip';
import {
  SearchQuery,
  ProjectType,
  Operation,
  PROJECTNAME,
} from '../../../types/searchTypes';

import {
  getSearchResults,
  SearchResultTypes,
} from '../../../utils/getFacetsInfo';

const classes = {
  infobar: {
    display: 'flex',
    padding: '8px',
  },
  infoContainer: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '5px',
  },
  text: {
    marginRight: '8px',
    marginLeft: '3px',
  },
  counter: {
    border: '1px solid grey',
    borderRadius: '5px',
    padding: '0 2px',
    minWidth: '20px',
    textAlign: 'center',
    backgroundColor: '#f8f8f8',
  },
  chip: {
    margin: '3px',
    backgroundColor: '#f5f5f5',
  },
  chipContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    flex: 1,
  },
  loadingBox: {
    padding: '32px',
  },
};
interface SearchResultsProps {
  searchQuery: SearchQuery;
  project: ProjectType;
  operation: Operation;
  nodes: string[];
}

const ProgressUI = () => {
  return (
    <div style={classes.loadingBox}>
      <Typography sx={classes.loadingBox} variant='h3' align='center'>
        Searching ...
      </Typography>
      <LinearProgress />
    </div>
  );
};

export const SeachCount = ({
  datasetCount,
  fileCount,
}: {
  datasetCount: number;
  fileCount: number;
}): JSX.Element => {
  return (
    <div style={classes.infoContainer}>
      <FolderOpenIcon style={{ color: '#6b6969' }} />
      <CustomizedTooltip
        title='Number of datasets currently selected (full datasets with all containing files).'
        placement='top'
      >
        <Typography sx={classes.text} variant='caption'>
          Datasets:
        </Typography>
      </CustomizedTooltip>
      <Typography sx={classes.counter} variant='caption'>
        {datasetCount}
      </Typography>

      <Divider
        orientation='vertical'
        flexItem
        style={{ color: '#6b6969', margin: '0 10px' }}
      />
      <InsertDriveFileIcon style={{ color: '#6b6969' }} />
      <CustomizedTooltip
        title='Number of files individually picked from datasets (without the full dataset being selected).'
        placement='top'
      >
        <Typography sx={classes.text} variant='caption'>
          Additional Files:
        </Typography>
      </CustomizedTooltip>
      <Typography sx={classes.counter} variant='caption'>
        {fileCount}
      </Typography>
    </div>
  );
};

const SearchResults = ({
  searchQuery,
  project,
  nodes,
  operation,
}: SearchResultsProps): JSX.Element => {
  const [searchResults, setSearchResult] =
    React.useState<SearchResultTypes>(null);
  const [datasetDuplicates, setDatasetDuplicates] = React.useState<string[]>(
    []
  );
  const [fileDuplicates, setFileDuplicates] = React.useState<string[]>([]);
  const { fileCollection } = useContext(FileSelectionCtx);
  const { datasetCollection } = useContext(DatasetSelectionCtx);

  const findDuplicates = (ids: string[]) => {
    const histogram = ids.reduce(
      (accumulator: { [key: string]: number }, el: string) => ({
        ...accumulator,
        [el]: (accumulator[el] || 0) + 1,
      }),
      {}
    );

    return Object.keys(histogram).filter((a) => histogram[a] > 1);
  };
  React.useEffect(() => {
    const ids = datasetCollection.map((dataset) => dataset.title);
    setDatasetDuplicates(findDuplicates(ids));
  }, [datasetCollection.length]);

  React.useEffect(() => {
    const ids = fileCollection.map((file) => file.title);
    setFileDuplicates(findDuplicates(ids));
  }, [fileCollection.length]);

  React.useEffect(() => {
    getSearchResults(searchQuery, project, nodes, DATASETLIMIT)
      .then((result) => {
        setSearchResult(result);
      })
      .catch((err) => console.error(err));
  }, [searchQuery]);

  const data = (searchResults && searchResults.results) || [];
  const datasetCount =
    (datasetCollection.length && datasetCollection.length) || 0;
  const fileCount = (fileCollection && fileCollection.length) || 0;
  return (
    <>
      <div style={classes.infobar}>
        <SeachCount datasetCount={datasetCount} fileCount={fileCount} />
        <div style={classes.chipContainer as React.CSSProperties}>
          {searchQuery.selectedItems.map((item) => {
            return (
              <Chip
                key={item.facetId}
                sx={classes.chip}
                label={`${item.facetId}:${item.fieldId}`}
                variant='outlined'
              />
            );
          })}
        </div>
      </div>
      {fileDuplicates.length || datasetDuplicates.length ? (
        <DuplicatesInSelectionAlert
          fileDuplicates={fileDuplicates}
          datasetDuplicates={datasetDuplicates}
        />
      ) : null}
      {searchResults ? (
        <DatasetTable
          operation={operation}
          searchQuery={searchQuery}
          nodes={nodes}
          data={{
            rows: data.map((d) => ({
              id: d.id,
              title: project.title === PROJECTNAME.CMIP6 ? d.title : d.id,
              projectName: project.title,
              fileNumber: d.fileNumber,
              version: d.version,
              size: d.size,
              citationUrl: d.citationUrl,
              instanceId: d.instanceId,
              dataNode: d.dataNode,
              startTime: d.startTime,
              endTime: d.endTime,
              checked: false,
              variable: d.variable ? d.variable : 'no variable',
            })),
          }}
        />
      ) : (
        <ProgressUI />
      )}
    </>
  );
};
export default SearchResults;
