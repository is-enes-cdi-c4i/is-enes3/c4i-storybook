import React from 'react';
import Chip from '@mui/material/Chip';
import { SearchQuery } from '../../../types/searchTypes';

const classes = {
  chip: {
    marginRight: '8px',
    marginBottom: '8px',
    backgroundColor: '#f5f5f5',
  },
  chipContainer: {
    padding: '12px',
    minHeight: '40px',
  },
};

interface RenderFacetsBarProps {
  searchQuery: SearchQuery;
  removeFacetField: (facetId: string, fieldId: string) => void;
}

export const ChipsBar = ({
  searchQuery,
  removeFacetField,
}: RenderFacetsBarProps): JSX.Element => {
  return (
    <div style={classes.chipContainer}>
      {searchQuery.selectedItems.map((item) => {
        return item.facetId === 'project' ? (
          <div />
        ) : (
          <Chip
            sx={classes.chip}
            key={item.facetId}
            label={`${item.facetId}:${item.fieldId}`}
            onDelete={(): void => {
              removeFacetField(item.facetId, item.fieldId);
            }}
            variant='outlined'
            color='primary'
          />
        );
      })}
    </div>
  );
};
