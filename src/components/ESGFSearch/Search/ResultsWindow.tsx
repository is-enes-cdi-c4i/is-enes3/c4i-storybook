import React, { useContext, useState } from 'react';
import {
  Typography,
  IconButton,
  Dialog,
  dialogClasses,
  DialogActions,
  DialogContent,
  Button,
  DialogTitle,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import GetAppIcon from '@mui/icons-material/GetApp';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import {
  DatasetSelectionCtx,
  emptyFileSelection,
  emptyDatasetSelection,
  FileSelectionCtx,
  NotebookFeedbackCtx,
} from '../../App/ApplicationContext';
import { DownloadUI } from '../Processing/DownloadUI';
import { NotebookUI } from '../Processing/NotebookUI';
import SearchResults from './SearchResults';
import { SubsettingForm } from '../Subsetting/SubsettingForm';
import GetFilesForQuery from '../GetFilesForQuery';
import {
  DEFAULTCOORDINATES,
  DEFAULTDATE,
  SubsetParameters,
  OpenidUser,
} from '../../../types/dataTypes';
import {
  Status,
  SearchQuery,
  ProjectType,
  OUTPUT,
  Operation,
  OPERATION,
  SUBSETTING,
} from '../../../types/searchTypes';

const classes = {
  root: {
    width: '100%',
    minHeight: '20vh',
  },
  closeButton: {
    position: 'absolute',
    right: '4px',
    top: '4px',
    color: '#9e9e9e',
  },
  dialogPaper: {
    minHeight: '90vh',
    maxHeight: '90vh',
    minWidth: '95vw',
    maxWidth: '95vw',
  },
  dialogTitle: {
    color: '#fff',
    backgroundColor: '#3f51b5',
    fontWeight: 400,
  },
  dialogContent: {
    padding: '0px',
  },
  dialogFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '8px',
    backgroundColor: '#f5f5f5',
  },
  button: {
    marginLeft: '10px',
  },
};
export interface ResultsWindowProps {
  handleClose: () => void;
  searchQuery: SearchQuery;
  projectType: ProjectType;
  operation: Operation;
  subsettingMode: SUBSETTING;
  nodes: string[];
  user: OpenidUser;
}
type ResultsTitleProps = {
  closeAndClear: () => void;
  operation: Operation;
};

const ResultsTitle = ({ closeAndClear, operation }: ResultsTitleProps) => {
  return (
    <DialogTitle id='simple-dialog-title' sx={classes.dialogTitle}>
      <Typography align='center'>
        {operation === OPERATION.SUBSETTING
          ? 'Subsetting Selection'
          : 'Search results'}
      </Typography>
      <IconButton
        aria-label='Close'
        sx={classes.closeButton}
        onClick={closeAndClear}
      >
        <CloseIcon style={{ color: 'white' }} />
      </IconButton>
    </DialogTitle>
  );
};

export const ResultsWindow = ({
  searchQuery,
  projectType,
  nodes,
  handleClose,
  operation,
  user,
  subsettingMode,
}: ResultsWindowProps): JSX.Element => {
  const { notebookProperties, setNotebookProperties } =
    useContext(NotebookFeedbackCtx);
  const { fileCollection, selectFiles } = useContext(FileSelectionCtx);
  const { datasetCollection, selectDatasets } = useContext(DatasetSelectionCtx);

  const [subsetFormOpen, setSubsetFormOpen] = React.useState<boolean>(false);
  const [subsettingParameters, setSubsettingParameters] =
    useState<SubsetParameters>({
      ...DEFAULTCOORDINATES,
      ...DEFAULTDATE,
      ...{ name: '', message: '' },
    });

  const closeAndClear = () => {
    selectFiles(emptyFileSelection);
    selectDatasets(emptyDatasetSelection);
    handleClose();
  };
  const queryLimit = 500;
  const datasetSelectionIsEmpty = !datasetCollection.length;
  const selectionIsEmpty = !fileCollection.length && datasetSelectionIsEmpty;

  return (
    <>
      <Dialog
        onClose={closeAndClear}
        aria-labelledby='Search Results'
        open
        sx={{ [`& .${dialogClasses.paper}`]: classes.dialogPaper }}
      >
        <ResultsTitle closeAndClear={closeAndClear} operation={operation} />
        <DialogContent sx={classes.dialogContent}>
          <SearchResults
            searchQuery={searchQuery}
            project={projectType}
            nodes={nodes}
            operation={operation}
          />
        </DialogContent>
        <DialogActions sx={classes.dialogFooter}>
          <Button onClick={closeAndClear} color='secondary'>
            Close
          </Button>
          <div>
            {operation === OPERATION.DOWNLOAD ? (
              <>
                <Button
                  disabled={selectionIsEmpty}
                  startIcon={<GetAppIcon />}
                  variant='contained'
                  sx={classes.button}
                  onClick={(): void => {
                    setNotebookProperties({
                      ...notebookProperties,
                      processingStatus: Status.IDLE,
                      redirectUrl: notebookProperties.redirectUrl,
                      dialogOpen: true,
                      output: OUTPUT.FILELIST,
                    });
                  }}
                  color='primary'
                >
                  Link list
                </Button>
                <Button
                  disabled={selectionIsEmpty || !user || !user.registered}
                  startIcon={<ExitToAppIcon />}
                  sx={classes.button}
                  variant='contained'
                  onClick={(): void => {
                    setNotebookProperties({
                      ...notebookProperties,
                      processingStatus: Status.IDLE,
                      redirectUrl: notebookProperties.redirectUrl,
                      dialogOpen: true,
                      output: OUTPUT.NOTEBOOK,
                    });
                  }}
                  color='primary'
                >
                  Export to notebook
                </Button>
              </>
            ) : (
              <Button
                disabled={datasetSelectionIsEmpty}
                startIcon={<AccountTreeIcon />}
                sx={classes.button}
                variant='contained'
                onClick={(): void => {
                  setSubsetFormOpen(true);
                }}
                color='primary'
              >
                Set Parameters
              </Button>
            )}
          </div>
        </DialogActions>
      </Dialog>
      {/*  FILE LIST  */}
      {notebookProperties.dialogOpen &&
        operation === OPERATION.DOWNLOAD &&
        notebookProperties.output === OUTPUT.FILELIST && (
          <div style={classes.root}>
            <GetFilesForQuery
              searchQuery={searchQuery}
              project={projectType}
              nodes={nodes}
              queryLimit={queryLimit}
              operation={OPERATION.DOWNLOAD}
            >
              {(fileListObject, completed, myQueryLimit) => (
                <DownloadUI {...{ completed, fileListObject, myQueryLimit }} />
              )}
            </GetFilesForQuery>
          </div>
        )}
      {/*  DOWNLOAD TO NOTEBOOK  */}
      {notebookProperties.dialogOpen &&
        operation === OPERATION.DOWNLOAD &&
        notebookProperties.output === OUTPUT.NOTEBOOK && (
          <div style={classes.root}>
            <GetFilesForQuery
              searchQuery={searchQuery}
              project={projectType}
              nodes={nodes}
              queryLimit={queryLimit}
              operation={OPERATION.DOWNLOAD}
            >
              {(fileListObject, completed, myQueryLimit) => (
                <NotebookUI
                  handleClose={handleClose}
                  {...{
                    fileListObject,
                    completed,
                    myQueryLimit,
                    operation,
                  }}
                  subsettingParameters={null}
                  subsettingMode={null}
                />
              )}
            </GetFilesForQuery>
          </div>
        )}
      {/*  SUBSETTING TO NOTEBOOK  */}
      {notebookProperties.dialogOpen && operation === OPERATION.SUBSETTING && (
        <div style={classes.root}>
          <GetFilesForQuery
            searchQuery={searchQuery}
            project={projectType}
            nodes={nodes}
            queryLimit={queryLimit}
            operation={OPERATION.SUBSETTING}
          >
            {(fileListObject, completed, myQueryLimit) => (
              <NotebookUI
                operation={operation}
                handleClose={handleClose}
                subsettingMode={subsettingMode}
                subsettingParameters={subsettingParameters}
                {...{ fileListObject, completed, myQueryLimit }}
              />
            )}
          </GetFilesForQuery>
        </div>
      )}
      {subsetFormOpen && (
        <SubsettingForm
          setSubsetFormOpen={setSubsetFormOpen}
          setSubsettingParameters={setSubsettingParameters}
          setNotebookProperties={setNotebookProperties}
          redirectUrl={notebookProperties.redirectUrl}
          subsettingMode={subsettingMode}
        />
      )}
      )
    </>
  );
};
export default ResultsWindow;
