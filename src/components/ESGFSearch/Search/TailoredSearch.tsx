import React, { useEffect } from 'react';
import produce from 'immer';

import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/lab/Alert';
import {
  DatasetSelectionProvider,
  FileSelectionProvider,
  NotebookPropertiesProvider,
} from '../../App/ApplicationContext';
import { ResultsWindow } from './ResultsWindow';
import { ChipsBar } from './ChipsBar';
import FacetSelector from './FacetSelector';
import FacetSelectionSideBar from '../Sidebar/FacetSelectionSideBar';
import { SearchFooter, PROJECTFACET } from '../Footer/SearchFooter';
import { SearchHeader } from '../Header/SearchHeader';
import { NodeSelection } from '../Header/NodeSelection';
import { ProjectSelection } from '../Header/ProjectSelection';
import { ESGFNodeList } from '../../../config/customSelections/datanodes';
import { defaultProjects } from '../../../config/projects';
import { getFacetsInfo, FacetInfo } from '../../../utils/getFacetsInfo';
import {
  FacetType,
  SearchQuery,
  Operation,
  OPERATION,
  ProjectType,
  SUBSETTING,
} from '../../../types/searchTypes';
import { OpenidUser } from '../../../types/dataTypes';

const Alert = (props): JSX.Element => {
  return <MuiAlert elevation={6} variant='filled' {...props} />;
};

const classes = {
  root: {
    background: '#fff',
    display: 'grid',
    gridGap: '0px',
    gridTemplateRows: '1fr 46px',
    gridTemplateColumns: '1fr',
    gridTemplateAreas: "'facetSelector' 'facetSelectorFooter'",
    height: '100%',
    width: '100%',
    overflow: 'hidden',
  },
  main: {
    width: '100%',
    boxShadow: '0px 0px 4px 1px #BDBDBD',
  },
  facetSelector: {
    marginTop: '45px',
    background: '#fff',
    gridArea: 'facetSelector',
    overflowY: 'auto',
    display: 'flex',
  },
  backdrop: {
    zIndex: 1201,
    color: '#fff',
    backgroundColor: 'rgba(128, 128, 128,0.2)',
  },
};

type ResultWindow = {
  open: boolean;
  operation: Operation;
  subsettingMode: SUBSETTING;
};

export type SearchProps = {
  user: OpenidUser;
  project: ProjectType;
};

const DEFAULTNODES = ESGFNodeList;

const TailoredSearch: React.FC<SearchProps> = ({
  user,
  project,
}: SearchProps) => {
  const [selectedFacet, setSelectedFacet] = React.useState<FacetType | null>(
    project && project.mainFacets[0]
  );

  const [searchQuery, setSearchQuery] = React.useState<SearchQuery>({
    selectedItems: [
      {
        facetId: PROJECTFACET,
        fieldId: defaultProjects[0].facetName,
        include: true,
      },
    ],
  });

  useEffect(() => {
    // Flush facet selection on project change
    if (
      project.title !==
      searchQuery.selectedItems.find((item) => item.facetId === PROJECTFACET)
        .fieldId
    ) {
      setSearchQuery({
        selectedItems: [
          {
            facetId: PROJECTFACET,
            fieldId: project.facetName,
            include: true,
          },
        ],
      });
    }
  }, [project.facetName]);

  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [selectedNodes, setSelectedNodes] = React.useState(DEFAULTNODES);
  const [openNodeSelection, setOpenNodeSelection] =
    React.useState<boolean>(false);
  const [openProjectSelection, setOpenProjectSelection] =
    React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>(null);
  const [resultsWindowState, setResultsWindow] = React.useState<ResultWindow>({
    open: false,
    operation: OPERATION.DOWNLOAD,
    subsettingMode: SUBSETTING.NONE,
  });
  // Simple view: Facets subselection as in config, Advanced: All facets available on node
  const [enableAdvancedView, setEnableAdvancedView] =
    React.useState<boolean>(false);
  const [searchFacetResults, setSearchFacetResults] =
    React.useState<FacetInfo | null>(null);

  React.useEffect(() => {
    if (project) {
      setIsLoading(true);
      setErrorMessage(null);
      getFacetsInfo(searchQuery, project, selectedNodes)
        .then((result) => {
          setSearchFacetResults(result);
        })
        .catch((e: string) => {
          console.error(e);
          setErrorMessage(`Unable to connect to search service (${e})`);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [searchQuery, selectedNodes]); // use Effect if nodes or facets change

  return (
    <div style={{ height: '100%', display: 'block' }}>
      <DatasetSelectionProvider>
        <FileSelectionProvider>
          <NotebookPropertiesProvider>
            <main
              style={
                selectedFacet
                  ? classes.root
                  : { ...classes.root, gridTemplateRows: '1fr' }
              }
            >
              <SearchHeader
                {...{
                  selectedNodes,
                  enableAdvancedView,
                  setEnableAdvancedView,
                  openProjectSelection,
                  setOpenProjectSelection,
                  openNodeSelection,
                  setOpenNodeSelection,
                  project,
                }}
              />
              <div style={classes.facetSelector as React.CSSProperties}>
                {/* Progress handling */}
                <Backdrop sx={classes.backdrop} open={isLoading}>
                  <CircularProgress />
                </Backdrop>
                {/* Error handling */}
                <Snackbar
                  open={errorMessage !== null}
                  autoHideDuration={6000}
                  onClose={(): void => {
                    setErrorMessage(null);
                  }}
                >
                  <div>
                    <Alert
                      onClose={(): void => {
                        setErrorMessage(null);
                      }}
                      severity='error'
                    >
                      {errorMessage && (
                        <div>{JSON.stringify(errorMessage)}</div>
                      )}
                    </Alert>
                  </div>
                </Snackbar>
                {resultsWindowState.open && (
                  <ResultsWindow
                    searchQuery={searchQuery}
                    projectType={project}
                    nodes={selectedNodes}
                    operation={resultsWindowState.operation}
                    subsettingMode={resultsWindowState.subsettingMode}
                    user={user}
                    handleClose={(): void => {
                      setResultsWindow((prevState) => ({
                        ...prevState,
                        open: false,
                      }));
                    }}
                  />
                )}
                <FacetSelectionSideBar
                  projectType={project}
                  onFacetClick={(facet: FacetType): void => {
                    setSelectedFacet(facet);
                  }}
                  searchQuery={searchQuery}
                />
                <div style={classes.main}>
                  <ProjectSelection
                    openProjectSelection={openProjectSelection}
                    setOpenProjectSelection={setOpenProjectSelection}
                  />
                  <NodeSelection
                    selectedNodes={selectedNodes}
                    setNodes={setSelectedNodes}
                    openNodeSelection={openNodeSelection}
                    setOpenNodeSelection={setOpenNodeSelection}
                  />
                  <ChipsBar
                    searchQuery={searchQuery}
                    removeFacetField={(
                      facetId: string,
                      fieldId: string
                    ): void => {
                      const selectedItems = searchQuery.selectedItems.filter(
                        (o) => o.facetId !== facetId || o.fieldId !== fieldId
                      );
                      setSearchQuery(
                        produce(searchQuery, (draft) => {
                          // eslint-disable-next-line no-param-reassign
                          draft.selectedItems = selectedItems;
                        })
                      );
                      if (selectedItems.length === 0) {
                        setSelectedFacet(null);
                      }
                    }}
                  />
                  <FacetSelector
                    facetType={selectedFacet}
                    searchFacetResults={searchFacetResults}
                    enableAdvancedView={enableAdvancedView}
                    searchQuery={searchQuery}
                    changeFacetField={(
                      facetId: string,
                      fieldId: string,
                      checked: boolean
                    ): void => {
                      setSearchQuery(
                        produce(searchQuery, (draft) => {
                          if (checked) {
                            draft.selectedItems.push({
                              facetId,
                              fieldId,
                              include: true,
                            });
                          } else {
                            // eslint-disable-next-line no-param-reassign
                            draft.selectedItems =
                              searchQuery.selectedItems.filter(
                                (o) =>
                                  o.facetId !== facetId || o.fieldId !== fieldId
                              );
                          }
                        })
                      );
                    }}
                  />
                </div>
              </div>
              <SearchFooter
                {...{
                  user,
                  setResultsWindow,
                  project,
                  selectedNodes,
                  searchQuery,
                }}
              />
            </main>
          </NotebookPropertiesProvider>
        </FileSelectionProvider>
      </DatasetSelectionProvider>
    </div>
  );
};
export default TailoredSearch;
