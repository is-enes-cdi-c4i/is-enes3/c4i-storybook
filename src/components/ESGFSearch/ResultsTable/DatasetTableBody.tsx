import React, { useContext } from 'react';
import {
  Collapse,
  TableBody,
  TableCell,
  TableRow,
  Checkbox,
  IconButton,
  Snackbar,
} from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import DescriptionIcon from '@mui/icons-material/Description';
import {
  FileSelectionCtx,
  DatasetSelectionCtx,
} from '../../App/ApplicationContext';
import { NestedFileTable } from './NestedFileTable';
import { DoiErrorAlert } from '../../GenericUI/Alerts';
import { getDOILink } from '../../../utils/resolveDOI';
import { resolveFilesOfDataset } from '../../../utils/getFacetsInfo';
import { parseSize } from '../../../utils/Swirrl/sizeCalculation';
import { stableSort, getComparator } from '../../../utils/muiTableSorting';
import {
  ESGFDataset,
  OPERATION,
  PROJECTNAME,
} from '../../../types/searchTypes';
import { TableProps, Data } from '../../../types/tableTypes';

const classes = {
  tableRow: {
    padding: '0px',
    margin: '0px',
  },
  tableCell: {
    padding: '0px',
    margin: '0px',
    lineHeight: '10px',
  },
};

export const DatasetTableBody: React.FC<TableProps> = ({
  orderBy,
  order,
  rows,
  page,
  rowsPerPage,
  dense,
  operation,
  searchQuery,
  nodes,
}: TableProps) => {
  const [open, setOpen] = React.useState([]);
  const [doiError, setDoiError] = React.useState(false);
  const [files, populateFiles] = React.useState([]);
  const { datasetCollection, selectDatasets } = useContext(DatasetSelectionCtx);
  const { fileCollection, selectFiles } = useContext(FileSelectionCtx);
  const esdocBaseUrl =
    'https://view.es-doc.org/?client_id=climate4impact_esgfsearch&renderMethod=datasetid&project=cmip5&id=';

  const openNestedTable = async (id: string) => {
    if (!open.includes(id)) {
      setOpen([id]);
      return resolveFilesOfDataset(id, operation, nodes).then((result) => {
        result.fileList.forEach((resultFile) => {
          // eslint-disable-next-line no-param-reassign
          resultFile.parent = id;
        });
        populateFiles(result.fileList);
      });
    }
    return setOpen(open.filter((elId) => !elId));
  };

  const handleSelectionClick = (
    dataset: ESGFDataset,
    datasetCtxCollection: ESGFDataset[]
  ): void => {
    const selection = datasetCtxCollection.some(
      (collItem) => collItem.id === dataset.id
    )
      ? datasetCtxCollection.filter((collItem) => collItem.id !== dataset.id) // Deselect and remove from ctx
      : datasetCtxCollection.concat(dataset); // Select and add to ctx
    selectDatasets(selection);
  };

  const displayDoiError = (state: boolean) => {
    setDoiError(state);
  };

  const formatVersion = (version: string): string =>
    `${version.slice(0, 4)}/${version.slice(4, 6)}/${version.slice(6, 8)}`;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  return (
    <TableBody>
      {stableSort(rows, getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: Data, index) => {
          const datasetSelected = datasetCollection.some(
            (dataset) => dataset.id === row.id
          );
          const deselectDataset = () =>
            datasetCollection.filter((dataset) => dataset.id !== row.id);
          const isRowExpanded = open.includes(row.id);
          const labelId = `enhanced-table-checkbox-${index}`;

          return (
            <>
              <TableRow
                hover
                role='checkbox'
                aria-checked={datasetSelected}
                tabIndex={-1}
                key={row.id}
                selected={datasetSelected}
                sx={classes.tableRow}
                style={{
                  backgroundColor: isRowExpanded ? '#F5F5F5' : '#FFFFFF',
                }}
              >
                <TableCell
                  padding='checkbox'
                  align='center'
                  sx={classes.tableCell}
                >
                  <Checkbox
                    onClick={() => {
                      handleSelectionClick(row, datasetCollection);
                      // When selecting the (complete) dataset, remove any of its files that were sepearatly selected (if applicable).
                      selectFiles(
                        fileCollection.filter((file) => file.parent !== row.id)
                      );
                    }}
                    checked={datasetSelected}
                    color='primary'
                    inputProps={{ 'aria-labelledby': labelId }}
                  />
                </TableCell>
                <TableCell>
                  {operation === OPERATION.DOWNLOAD && (
                    <IconButton
                      aria-label='expand row'
                      size='small'
                      // eslint-disable-next-line @typescript-eslint/no-misused-promises
                      onClick={() => openNestedTable(row.id)}
                    >
                      {isRowExpanded ? (
                        <KeyboardArrowUpIcon />
                      ) : (
                        <KeyboardArrowDownIcon />
                      )}
                    </IconButton>
                  )}
                </TableCell>
                <TableCell
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                  }}
                  sx={classes.tableCell}
                >
                  {row.title}
                </TableCell>
                <TableCell
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textAlign: 'center',
                  }}
                  sx={classes.tableCell}
                >
                  {row.dataNode}
                </TableCell>
                <TableCell
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textAlign: 'center',
                  }}
                  sx={classes.tableCell}
                >
                  {formatVersion(row.version) || 'n/a'}
                </TableCell>
                {row.projectName !== PROJECTNAME.CMIP5 && (
                  <TableCell
                    style={{
                      overflow: 'hidden',
                      whiteSpace: 'nowrap',
                      textAlign: 'center',
                    }}
                    sx={classes.tableCell}
                  >
                    {row.fileNumber}
                  </TableCell>
                )}
                <TableCell
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textAlign: 'center',
                  }}
                  sx={classes.tableCell}
                >
                  {parseSize(row.size)}
                </TableCell>
                <TableCell
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textAlign: 'center',
                  }}
                  sx={classes.tableCell}
                >
                  <div>
                    <IconButton
                      color='primary'
                      type='button'
                      onClick={
                        // eslint-disable-next-line no-nested-ternary
                        row.projectName === PROJECTNAME.CMIP6
                          ? async () => {
                              const url = await getDOILink(
                                row.citationUrl,
                                row.instanceId
                              );
                              // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                              url && row.instanceId
                                ? window.open(url, '_blank')
                                : displayDoiError(true);
                            }
                          : row.projectName === PROJECTNAME.CMIP5
                          ? () =>
                              row.instanceId
                                ? window.open(
                                    `${esdocBaseUrl}${row.instanceId}`,
                                    '_blank'
                                  )
                                : displayDoiError(true)
                          : () => null // e.g. for CORDEX
                      }
                    >
                      <DescriptionIcon />
                    </IconButton>
                    <Snackbar
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                      open={doiError}
                      autoHideDuration={5000}
                      onClose={() => displayDoiError(false)}
                    >
                      <DoiErrorAlert
                        onClose={() => displayDoiError(false)}
                        severity='error'
                      >
                        Metadata link could not be resolved
                      </DoiErrorAlert>
                    </Snackbar>
                  </div>
                </TableCell>
              </TableRow>
              {operation === OPERATION.DOWNLOAD && (
                <TableRow>
                  <TableCell
                    style={{
                      paddingBottom: 0,
                      paddingTop: 0,
                      backgroundColor: '#f6f6f6',
                    }}
                    colSpan={8}
                  >
                    <Collapse in={isRowExpanded} timeout='auto' unmountOnExit>
                      <NestedFileTable
                        files={files}
                        datasetSelected={datasetSelected}
                        deselectDataset={deselectDataset}
                        searchQuery={searchQuery}
                        projectName={row.projectName}
                      />
                    </Collapse>
                  </TableCell>
                </TableRow>
              )}
            </>
          );
        })}
      {emptyRows > 0 && (
        <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
          <TableCell colSpan={8} />
        </TableRow>
      )}
    </TableBody>
  );
};
