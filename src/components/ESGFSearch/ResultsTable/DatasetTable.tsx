import React from 'react';
import { TablePagination, Paper, Table, TableContainer } from '@mui/material';
import { DatasetTableHead } from './DatasetTableHead';
import { DatasetTableBody } from './DatasetTableBody';
import { Data, Order } from '../../../types/tableTypes';
import { Operation, SearchQuery } from '../../../types/searchTypes';

const classes = {
  paper: {
    width: '100%',
    marginBottom: '16px',
  },
  table: {
    minWidth: '750px',
  },
};

interface DataTableProps {
  data: { rows: Data[] };
  operation: Operation;
  searchQuery: SearchQuery;
  nodes: string[];
}

export const DatasetTable: React.FC<DataTableProps> = ({
  data,
  operation,
  searchQuery,
  nodes,
}: DataTableProps) => {
  const { rows } = data;
  const [order, setOrder] = React.useState<Order>('desc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('title');

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const dense = true;

  const handleRequestSort = (property: keyof Data): void => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number): void => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  return (
    <Paper sx={classes.paper}>
      <TableContainer>
        <Table
          sx={classes.table}
          aria-labelledby='tableTitle'
          aria-label='enhanced table'
        >
          <DatasetTableHead
            order={order}
            orderBy={orderBy}
            classes={classes}
            rows={rows}
            rowsPerPage={rowsPerPage}
            page={page}
            dense={dense}
            onRequestSort={handleRequestSort}
            operation={operation}
            searchQuery={searchQuery}
          />
          <DatasetTableBody
            order={order}
            orderBy={orderBy}
            classes={classes}
            rows={rows}
            rowsPerPage={rowsPerPage}
            page={page}
            dense={dense}
            operation={operation}
            searchQuery={searchQuery}
            nodes={nodes}
          />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 8, 10, 20, 25, 100]}
        component='div'
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};
