import React, { useContext } from 'react';
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Button,
} from '@mui/material';
import ListIcon from '@mui/icons-material/List';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import {
  DatasetSelectionCtx,
  FileSelectionCtx,
  emptyFileSelection,
  emptyDatasetSelection,
} from '../../App/ApplicationContext';
import { CustomizedTooltip } from '../../GenericUI/CustomizedTooltip';
import { TableProps, Data, HeadCell } from '../../../types/tableTypes';
import { PROJECTNAME } from '../../../types/searchTypes';

const classes = {
  visuallyHidden: {
    border: '0px',
    clip: 'rect(0 0 0 0)',
    height: '1px',
    margin: '-1px',
    overflow: 'hidden',
    padding: '0px',
    position: 'absolute',
    top: '20px',
    width: 1,
  },
};

export const DatasetTableHead: React.FC<TableProps> = ({
  orderBy,
  order,
  onRequestSort,
  rows,
}: TableProps) => {
  const { selectDatasets } = useContext(DatasetSelectionCtx);
  const { selectFiles } = useContext(FileSelectionCtx);

  const headCells: HeadCell[] = [
    {
      id: 'id',
      label: 'Dataset',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: '',
    },
    {
      id: 'dataNode',
      label: 'Mirror (Node)',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: '',
    },
    {
      id: 'version',
      label: 'Version',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: 'Latest update',
    },
    {
      id: 'fileNumber',
      label: 'Files',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: 'Number of links',
    },
    {
      id: 'size',
      label: 'Size',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: 'Total size of dataset',
    },
    {
      id: 'citationUrl',
      label: 'Doc',
      numeric: false,
      disablePadding: false,
      badge: false,
      description: 'Dataset Documentation',
    },
  ];

  if (rows[0].projectName === PROJECTNAME.CMIP5) {
    headCells.splice(3, 1);
  }

  const createSortHandler = (property: keyof Data): void => {
    onRequestSort(property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell>
          <CustomizedTooltip title='Select all datasets' placement='top'>
            <Button
              variant='text'
              startIcon={<ListIcon />}
              style={{ backgroundColor: '#f5f5f5', paddingRight: 8 }}
              size='small'
              color='primary'
              onClick={(): void => {
                selectDatasets(rows);
                selectFiles(emptyFileSelection);
              }}
            >
              All
            </Button>
          </CustomizedTooltip>
        </TableCell>
        <TableCell>
          <CustomizedTooltip title='Clear selected data' placement='top'>
            <Button
              variant='text'
              style={{ backgroundColor: '#f5f5f5', paddingRight: 8 }}
              color='primary'
              startIcon={<CheckCircleOutlineIcon />}
              size='small'
              onClick={(): void => {
                selectDatasets(emptyDatasetSelection);
                selectFiles(emptyFileSelection);
              }}
            >
              None
            </Button>
          </CustomizedTooltip>
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align='center'
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <CustomizedTooltip title={headCell.description} placement='top'>
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={
                  // eslint-disable-next-line no-nested-ternary
                  orderBy === headCell.id
                    ? order === 'asc'
                      ? 'asc'
                      : 'desc'
                    : 'asc'
                }
                onClick={(): void => {
                  createSortHandler(headCell.id);
                }}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span style={classes.visuallyHidden as React.CSSProperties}>
                    {order === 'desc'
                      ? 'sorted descending'
                      : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </CustomizedTooltip>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
