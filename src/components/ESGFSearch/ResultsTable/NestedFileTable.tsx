import React, { useContext } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  Checkbox,
  TableContainer,
  TableHead,
  TablePagination,
  Box,
} from '@mui/material';
import { FileSelectionCtx } from '../../App/ApplicationContext';
import { PROJECTNAME, SearchQuery } from '../../../types/searchTypes';
import { parseSize } from '../../../utils/Swirrl/sizeCalculation';
import { ESGFFileProperties, filterFiles } from '../../../utils/getFacetsInfo';

const handleSelectionClick = (
  item: ESGFFileProperties,
  collection: ESGFFileProperties[]
): ESGFFileProperties[] => {
  const selection = collection.find((element) => element.url === item.url)
    ? collection.filter((element) => element.url !== item.url)
    : collection.concat(item);

  return selection;
};

const TableHeader = () => (
  <TableHead>
    <TableRow>
      <TableCell>Select</TableCell>
      <TableCell>File</TableCell>
      <TableCell align='center'>Size</TableCell>
    </TableRow>
  </TableHead>
);

type NestedTableProps = {
  files: ESGFFileProperties[];
  datasetSelected: boolean;
  deselectDataset: () => void;
  searchQuery: SearchQuery;
  projectName: PROJECTNAME;
};

export const NestedFileTable = ({
  files,
  datasetSelected,
  deselectDataset,
  searchQuery,
  projectName,
}: NestedTableProps): React.ReactElement => {
  const [filePage, setFilePage] = React.useState(0);
  const [fileRowsPerPage, setFileRowsPerPage] = React.useState(10);
  const { fileCollection, selectFiles } = useContext(FileSelectionCtx);

  const handleFileChangePage = (event: unknown, newPage: number) => {
    setFilePage(newPage);
  };

  const handleFileChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setFileRowsPerPage(+event.target.value);
    setFilePage(0);
  };

  const variables = searchQuery.selectedItems
    .filter((item) => item.facetId === 'variable')
    .map((item) => item.fieldId);

  /*
    For CMIP5 we have to remove any files that do not match our 'variable' facet(s)
    As filenames are prefixed with the variable name, we just filter on that prefix
  */
  const matchingFiles =
    projectName && projectName === PROJECTNAME.CMIP5
      ? filterFiles(files, variables)
      : files;

  return (
    <Box margin={0} style={{ padding: '10px 40px' }}>
      <br />
      <TableContainer>
        <Table
          size='small'
          padding='none'
          aria-label='files'
          style={{ fontSize: '12' }}
        >
          <TableHeader />
          <TableBody>
            {matchingFiles &&
              matchingFiles
                .slice(
                  filePage * fileRowsPerPage,
                  filePage * fileRowsPerPage + fileRowsPerPage
                )
                .map((file, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;
                  const isItemSelected = fileCollection.some(
                    (element) => element.url === file.url
                  );
                  return (
                    <TableRow key={file.instanceId}>
                      {datasetSelected ? (
                        // If the (full) dataset is selected, lock selection of single files:
                        <Checkbox
                          disabled
                          checked
                          inputProps={{
                            'aria-label': 'disabled checked checkbox',
                          }}
                        />
                      ) : (
                        <Checkbox
                          onClick={() => {
                            // Dataset should be deselected when specifc files of it are selected.
                            deselectDataset();
                            selectFiles(
                              handleSelectionClick(file, fileCollection)
                            );
                          }}
                          checked={isItemSelected}
                          color='primary'
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      )}
                      <TableCell component='th' scope='row'>
                        {file.title}
                      </TableCell>
                      <TableCell align='center'>
                        {parseSize(file.size)}
                      </TableCell>
                    </TableRow>
                  );
                })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component='div'
        count={matchingFiles.length}
        rowsPerPage={fileRowsPerPage}
        page={filePage}
        onPageChange={handleFileChangePage}
        onRowsPerPageChange={handleFileChangeRowsPerPage}
      />
    </Box>
  );
};
