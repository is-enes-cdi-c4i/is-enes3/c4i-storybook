import React, { useEffect } from 'react';
import { SearchQuery, ProjectType } from '../../types/searchTypes';
import { getSearchResults } from '../../utils/getFacetsInfo';
import { DATASETLIMIT } from '../App/ApplicationConfig';

interface ESGFSearchProps {
  searchQuery: SearchQuery;
  project: ProjectType;
}

const GetSearchResultsDemo = ({
  searchQuery,
  project,
}: ESGFSearchProps): JSX.Element => {
  // Similar to componentDidMount and componentDidUpdate:
  const [facetInfo, setFacetInfo] = React.useState<string>(null);
  useEffect(() => {
    getSearchResults(searchQuery, project, null, DATASETLIMIT)
      .then((result) => {
        setFacetInfo(JSON.stringify(result, null, 2));
      })
      .catch((err) => console.error(err));
  });

  return <pre style={{ overflow: 'scroll', height: '600px' }}>{facetInfo}</pre>;
};
export default GetSearchResultsDemo;
