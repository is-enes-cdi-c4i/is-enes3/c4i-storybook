import React, { useEffect } from 'react';
import { getFilesForDatasets } from '../../utils/getFacetsInfo';
import { OPERATION } from '../../types/searchTypes';

interface GetFilesForDatasetDemoProps {
  datasetIds: string[];
}

const GetFilesForDatasetDemo = ({
  datasetIds,
}: GetFilesForDatasetDemoProps): JSX.Element => {
  const [facetInfo, setFacetInfo] = React.useState<string>(null);
  useEffect(() => {
    getFilesForDatasets(datasetIds, OPERATION.DOWNLOAD, null)
      .then((result) => {
        setFacetInfo(JSON.stringify(result, null, 2));
      })
      .catch((err) => console.error(err));
  });

  return <pre style={{ overflow: 'scroll', height: '600px' }}>{facetInfo}</pre>;
};
export default GetFilesForDatasetDemo;
