import React, { useContext } from 'react';
import { Button, Typography, Popover, popoverClasses } from '@mui/material';
import {
  NotebookFeedbackCtx,
  DatasetSelectionCtx,
  UserCtx,
} from '../../App/ApplicationContext';
import { DialogWrapper } from '../../GenericUI/DialogWrapper';
import { SharedUI } from './SharedUI';
import {
  NotebookResumeAlert,
  StageSizeAlert,
  WorkflowProgressAlert,
} from '../../GenericUI/Alerts';
import { COMPLETE, SubsetParameters } from '../../../types/dataTypes';
import {
  OPERATION,
  Operation,
  NotebookSizeProperties,
  SUBSETTING,
} from '../../../types/searchTypes';
import { ESGFFileList } from '../../../utils/getFacetsInfo';
import {
  handleCreateNotebook,
  handleResumeNotebook,
} from '../../../utils/Swirrl/createNotebook';
import { STAGELIMIT } from '../../App/ApplicationConfig';

const classes = {
  buttonGroup: {
    display: 'flex',
    justifyContent: 'end',
  },
  generateButton: {
    margin: '10px auto 0px auto',
    display: 'flex',
    minWidth: '150px',
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: '8px',
    backgroundColor: 'red',
    color: 'white',
  },
};

type NotebookProps = {
  completed: number;
  fileListObject: ESGFFileList[];
  myQueryLimit: number;
  operation: Operation;
  subsettingParameters: SubsetParameters;
  // eslint-disable-next-line react/no-unused-prop-types
  serviceUrl: string;
  // eslint-disable-next-line react/no-unused-prop-types
  notebookSize: NotebookSizeProperties;
  stageSize: number;
  subsettingMode: SUBSETTING;
};

export const ResumeNotebook = ({
  completed,
  fileListObject,
  myQueryLimit,
  notebookSize,
  stageSize,
  operation, // Use this to determine the message.
  subsettingParameters,
  subsettingMode,
}: NotebookProps): React.ReactElement => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const { notebookProperties, setNotebookProperties } =
    useContext(NotebookFeedbackCtx);
  const { datasetCollection } = useContext(DatasetSelectionCtx);
  const { user, setUser } = useContext(UserCtx);

  const handlePopoverOpen = (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ): void => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = (): void => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  return (
    <DialogWrapper
      title='Resume Notebook'
      size='md'
      buttonTitle={null}
      buttonCallback={null}
      buttonDisabled={null}
      clearOnClose={null}
    >
      <>
        <SharedUI
          {...{
            completed,
            fileListObject,
            myQueryLimit,
          }}
        />
        <NotebookResumeAlert
          url={user?.notebookURL || ''}
          notebookSize={notebookSize}
        />
        <WorkflowProgressAlert {...notebookSize} />
        <StageSizeAlert stageSize={stageSize} operation={operation} />
        <div style={classes.buttonGroup}>
          <Button
            aria-owns={open ? 'mouse-over-popover' : undefined}
            aria-haspopup='true'
            onMouseEnter={handlePopoverOpen}
            onMouseLeave={handlePopoverClose}
            sx={classes.generateButton}
            color='primary'
            variant='outlined'
            disabled={
              completed !== COMPLETE ||
              (operation === OPERATION.DOWNLOAD && stageSize > STAGELIMIT)
            }
            onClick={(): void => {
              if (operation === OPERATION.SUBSETTING) {
                handleCreateNotebook(
                  setNotebookProperties,
                  notebookProperties,
                  OPERATION.SUBSETTING,
                  fileListObject,
                  user,
                  setUser,
                  datasetCollection,
                  subsettingParameters,
                  subsettingMode
                );
              } else {
                handleCreateNotebook(
                  setNotebookProperties,
                  notebookProperties,
                  OPERATION.DOWNLOAD,
                  fileListObject,
                  user,
                  setUser
                );
              }
            }}
          >
            New Notebook
          </Button>
          <Popover
            id='mouse-over-popover'
            sx={{
              ...classes.popover,
              [`& .${popoverClasses.paper}`]: classes.paper,
            }}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            onClose={handlePopoverClose}
            disableRestoreFocus
          >
            <Typography>This will delete your old notebook.</Typography>
          </Popover>
          <Button
            sx={classes.generateButton}
            color='primary'
            variant='contained'
            disabled={
              completed !== COMPLETE ||
              (operation === OPERATION.DOWNLOAD && stageSize > STAGELIMIT)
            }
            onClick={(): void => {
              if (operation === OPERATION.SUBSETTING) {
                handleResumeNotebook(
                  setNotebookProperties,
                  notebookProperties,
                  OPERATION.SUBSETTING,
                  fileListObject,
                  user,
                  datasetCollection,
                  subsettingParameters,
                  subsettingMode
                );
              } else {
                handleResumeNotebook(
                  setNotebookProperties,
                  notebookProperties,
                  OPERATION.DOWNLOAD,
                  fileListObject,
                  user
                );
              }
            }}
          >
            Resume Notebook
          </Button>
        </div>
      </>
    </DialogWrapper>
  );
};

export const NewNotebook = ({
  completed,
  fileListObject,
  myQueryLimit,
  operation,
  subsettingParameters,
  subsettingMode,
  stageSize,
}: NotebookProps): React.ReactElement => {
  const { notebookProperties, setNotebookProperties } =
    useContext(NotebookFeedbackCtx);
  const { datasetCollection } = useContext(DatasetSelectionCtx);
  const { user, setUser } = useContext(UserCtx);

  return (
    <DialogWrapper
      title='New Notebook'
      buttonTitle='Generate Notebook'
      buttonDisabled={
        completed !== COMPLETE ||
        (operation === OPERATION.DOWNLOAD && stageSize > STAGELIMIT)
      }
      buttonCallback={
        operation === OPERATION.SUBSETTING
          ? (): void => {
              handleCreateNotebook(
                setNotebookProperties,
                notebookProperties,
                OPERATION.SUBSETTING,
                fileListObject,
                user,
                setUser,
                datasetCollection,
                subsettingParameters,
                subsettingMode
              );
            }
          : (): void => {
              handleCreateNotebook(
                setNotebookProperties,
                notebookProperties,
                OPERATION.DOWNLOAD,
                fileListObject,
                user,
                setUser
              );
            }
      }
      size={null}
      clearOnClose={null}
    >
      <>
        <SharedUI
          {...{
            completed,
            fileListObject,
            myQueryLimit,
          }}
        />
        <StageSizeAlert stageSize={stageSize} operation={operation} />
      </>
    </DialogWrapper>
  );
};
