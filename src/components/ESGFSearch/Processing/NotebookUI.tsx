import React, { useState, useContext } from 'react';
import { LinearProgress } from '@mui/material';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import {
  NotebookFeedbackCtx,
  notebookDefaultProperties,
  UserCtx,
} from '../../App/ApplicationContext';
import { NewNotebook, ResumeNotebook } from './NotebookDialog';
import {
  NotebookGeneratingAlert,
  NotebookReadyAlert,
  NotebookDownloadingAlert,
  NotebookWorkerAlert,
  NotebookErrorAlert,
} from '../../GenericUI/Alerts';
import { DialogWrapper } from '../../GenericUI/DialogWrapper';
import { getNotebookInfo } from '../../../utils/Swirrl/sizeCalculation';
import { ESGFFileList } from '../../../utils/getFacetsInfo';
import { SubsetParameters } from '../../../types/dataTypes';
import {
  Status,
  Operation,
  NotebookSizeProperties,
  SUBSETTING,
} from '../../../types/searchTypes';

type NotebookUIProps = {
  handleClose: () => void;
  fileListObject: ESGFFileList[];
  completed: number;
  myQueryLimit: number;
  operation: Operation;
  subsettingParameters: SubsetParameters;
  subsettingMode: SUBSETTING;
};

export const NotebookUI = ({
  handleClose,
  fileListObject,
  completed,
  myQueryLimit,
  operation,
  subsettingParameters,
  subsettingMode,
}: NotebookUIProps): React.ReactElement => {
  let stageSize = 0;
  if (fileListObject) {
    fileListObject.forEach((e) => {
      e.fileList.forEach((file) => {
        stageSize += file.size;
      });
    });
  }
  const [notebookSize, setNotebookSize] = useState<NotebookSizeProperties>();
  const { notebookProperties, setNotebookProperties } =
    useContext(NotebookFeedbackCtx);
  const { user } = useContext(UserCtx);
  const { notebookURL, sessionId } = user;

  switch (notebookProperties.processingStatus) {
    case Status.IDLE:
      if (!notebookSize) {
        setNotebookSize({
          currentSize: -1,
          currentStatus: '',
        });
        getNotebookInfo(user.notebookId, user.notebookURL)
          .then((info: NotebookSizeProperties) => {
            setNotebookSize(info);
          })
          .catch((err) => console.error(err));
      }
      if (notebookURL && sessionId) {
        return (
          <ResumeNotebook
            {...{
              completed,
              fileListObject,
              myQueryLimit,
              operation,
              subsettingParameters,
              notebookURL,
              notebookSize,
              stageSize,
              subsettingMode,
              serviceUrl: null,
            }}
          />
        );
      }
      return (
        <NewNotebook
          {...{
            completed,
            fileListObject,
            myQueryLimit,
            notebookSize,
            operation,
            subsettingParameters,
            subsettingMode,
            stageSize,
            serviceUrl: null,
          }}
        />
      );

    case Status.GENERATING:
      return (
        <DialogWrapper
          title='Generating'
          buttonTitle={null}
          buttonCallback={null}
          buttonDisabled={null}
          size={null}
          clearOnClose={null}
        >
          <>
            <LinearProgress />
            <br />
            <NotebookGeneratingAlert />
          </>
        </DialogWrapper>
      );
    case Status.ERROR:
      return (
        <DialogWrapper
          title='Error'
          buttonTitle={null}
          buttonCallback={null}
          buttonDisabled={null}
          size={null}
          clearOnClose={null}
        >
          <NotebookErrorAlert error={notebookProperties.error} />
        </DialogWrapper>
      );
    case Status.READY:
      return (
        <DialogWrapper
          title='Ready'
          buttonTitle='Open Notebook'
          clearOnClose
          buttonCallback={(): void => {
            const redirectUrlToken = notebookProperties.redirectUrl.split('?');
            window.open(
              `${redirectUrlToken[0]}/lab/tree/Welcome%20to%20SWIRRL.md?${redirectUrlToken[1]}`,
              '_blank'
            );
            setNotebookProperties(notebookDefaultProperties);
            handleClose();
          }}
          buttonDisabled={null}
          size={null}
        >
          <List>
            <Divider component='li' />
            <NotebookReadyAlert />
            <Divider component='li' />
            <NotebookWorkerAlert
              workerNumber={notebookProperties.workerNumber || 0}
            />
            <Divider component='li' />
            <NotebookDownloadingAlert />
            <Divider component='li' />
          </List>
        </DialogWrapper>
      );
    default:
      return <div />;
  }
};
