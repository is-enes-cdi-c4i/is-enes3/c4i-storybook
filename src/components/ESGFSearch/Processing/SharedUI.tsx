import React from 'react';
import { LinearProgress, lighten, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { ESGFFileList } from '../../../utils/getFacetsInfo';

const classes = {
  root: {
    width: '100%',
    minHeight: '20vh',
  },
  progressBar: {
    height: '8px',
    padding: '10px 0',
  },
  limit: {
    margin: '5px',
    minWidth: '220px',
  },
};

interface ProcessingProps {
  completed: number;
  fileListObject: ESGFFileList[];
}

interface FileProps {
  myQueryLimit: number;
}

const CustomLinearProgress = styled(LinearProgress)(() => ({
  height: 10,
  backgroundColor: lighten('#ff6c5c', 0.5),
}));

export const SharedUI = ({
  completed,
  fileListObject,
  myQueryLimit,
}: ProcessingProps & FileProps): JSX.Element => {
  const COMPLETE = 100; // Percentage
  const datasets = fileListObject
    ? fileListObject.filter((element) => element.datasetId !== '')
    : [];
  const datasetFileCount =
    fileListObject &&
    datasets.reduce((count, dataset) => count + dataset.fileList.length, 0);
  const fileCount = fileListObject
    ? fileListObject.find((element) => element.datasetId === '').fileList.length
    : 0;

  return (
    <>
      <div style={classes.progressBar}>
        <CustomLinearProgress variant='determinate' value={completed} />
      </div>
      {completed === COMPLETE ? (
        <Typography align='center'>
          {`Link collection ready (limited to ${myQueryLimit} datasets).
            Found ${fileListObject && datasets.length} datasets containing
             ${datasetFileCount} file links and ${fileCount} single file(s).`}
        </Typography>
      ) : (
        <Typography align='center'>
          Compiling filelist, {completed} % complete. (limited to {myQueryLimit}{' '}
          datasets)
        </Typography>
      )}
    </>
  );
};
