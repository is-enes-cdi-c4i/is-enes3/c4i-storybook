import React from 'react';
import FileSaver from 'file-saver';
import { SharedUI } from './SharedUI';
import { DialogWrapper } from '../../GenericUI/DialogWrapper';
import { ESGFFileList } from '../../../utils/getFacetsInfo';
import { generateMetalink } from '../../../utils/createMetaLink';

interface ProcessingProps {
  completed: number;
  fileListObject: ESGFFileList[];
  myQueryLimit: number;
}

export const DownloadUI = ({
  completed,
  fileListObject,
  myQueryLimit,
}: ProcessingProps): JSX.Element => {
  const COMPLETE = 100; // Percentage

  return (
    <DialogWrapper
      title='Generate link collection'
      buttonTitle='Download Metalink'
      buttonDisabled={completed !== COMPLETE}
      buttonCallback={(): void => {
        const blob = new Blob([generateMetalink(fileListObject)], {
          type: 'text/plain;charset=utf-8',
        });
        FileSaver.saveAs(blob, 'datasets.metalink');
      }}
      size={null}
      clearOnClose={null}
    >
      <SharedUI
        {...{
          completed,
          fileListObject,
          myQueryLimit,
        }}
      />
    </DialogWrapper>
  );
};
