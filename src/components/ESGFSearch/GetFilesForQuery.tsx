import React, { useEffect, useContext } from 'react';
import {
  SearchQuery,
  ProjectType,
  OPERATION,
  PROJECTNAME,
} from '../../types/searchTypes';
import {
  getSearchResults,
  getFilesForDatasets,
  ESGFFileList,
  filterFiles,
} from '../../utils/getFacetsInfo';
import {
  DatasetSelectionCtx,
  FileSelectionCtx,
} from '../App/ApplicationContext';
import { COMPLETE } from '../../types/dataTypes';

interface ESGFSearchProps {
  searchQuery: SearchQuery;
  project: ProjectType;
  nodes: string[];
  queryLimit: number;
  operation: OPERATION;
  children(
    fileListObject: ESGFFileList[],
    completed: number,
    myQueryLimit: number
  ): JSX.Element;
}

const GetFilesForQuery = ({
  searchQuery,
  project,
  nodes,
  queryLimit,
  operation,
  children,
}: ESGFSearchProps): JSX.Element => {
  const { datasetCollection } = useContext(DatasetSelectionCtx);
  const { fileCollection } = useContext(FileSelectionCtx);

  const [fileListObject, setFileListObject] =
    React.useState<ESGFFileList[]>(null);
  const [completed, setCompleted] = React.useState<number>(0);

  const variables = searchQuery.selectedItems
    .filter((item) => item.facetId === 'variable')
    .map((item) => item.fieldId);

  useEffect(() => {
    getSearchResults(searchQuery, project, nodes, queryLimit)
      .then((result) => {
        // Filtered and reduced to ids of selected datasets
        const datasetIds = result.results.reduce(
          (idCollection: string[], res) => {
            datasetCollection.some(
              (ctxItem) => res.id === ctxItem.id && idCollection.push(res.id)
            );
            return idCollection;
          },
          []
        );

        getFilesForDatasets(
          datasetIds,
          operation,
          nodes,
          (per: number): void => {
            setCompleted(parseInt(`${per}`, 10));
          }
        )
          .then((res) => {
            setCompleted(COMPLETE);
            setFileListObject([
              ...res.map((r: ESGFFileList) => ({
                datasetId: r.datasetId,
                fileList:
                  project.title === PROJECTNAME.CMIP5
                    ? filterFiles(r.fileList, variables)
                    : r.fileList,
              })),
              {
                // All files hand picked from a dataset go here:
                datasetId: '',
                variable: '',
                fileList: fileCollection,
              },
            ]);
          })
          .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  }, [
    searchQuery,
    project.facetName,
    nodes,
    queryLimit,
    datasetCollection.length,
    fileCollection.length,
  ]);

  return <>{children(fileListObject, completed, queryLimit)}</>;
};

export default GetFilesForQuery;
