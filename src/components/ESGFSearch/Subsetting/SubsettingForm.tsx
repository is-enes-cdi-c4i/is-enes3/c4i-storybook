import React, { useContext, useState } from 'react';
import {
  Typography,
  IconButton,
  Button,
  Dialog,
  dialogClasses,
  DialogTitle,
  DialogActions,
  DialogContent,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import {
  NotebookFeedbackCtx,
  notebookDefaultProperties,
} from '../../App/ApplicationContext';
import { ParameterSelector } from './ParameterSelector';
import {
  SubsetTemporalParameters,
  SubsetSpatialParameters,
  DEFAULTCOORDINATES,
  NODATE,
  OPENDAPMETA,
  ROOKMETA,
  SubsetParameters,
} from '../../../types/dataTypes';
import {
  NotebookProperties,
  Status,
  OUTPUT,
  SUBSETTING,
} from '../../../types/searchTypes';

const classes = {
  root: {
    minWidth: '100%',
    minHeight: '50vh',
  },
  closeButton: {
    position: 'absolute',
    right: '4px',
    top: '4px',
    color: '#9e9e9e',
  },
  dialogDownloadPaper: {
    minHeight: '30vh',
    minWidth: '65vw',
  },
  dialogTitle: {
    color: '#fff',
    backgroundColor: '#3f51b5',
    fontWeight: 400,
  },
  dialogContent: {
    padding: '0px',
  },
  dialogFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '8px',
    backgroundColor: '#f5f5f5',
  },
};

export type SubsetProps = {
  setSubsetFormOpen: (accessFormOpen: boolean) => void;
  setSubsettingParameters: (parameters: SubsetParameters) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  setNotebookProperties: (status: NotebookProperties) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  redirectUrl: string;
  subsettingMode: SUBSETTING;
};

export const SubsettingForm = ({
  setSubsetFormOpen,
  setSubsettingParameters,
  subsettingMode,
}: SubsetProps): React.ReactElement => {
  const [temporalParameters, setTemporalParameter] =
    useState<SubsetTemporalParameters>(NODATE);
  const [spatialParameters, setSpatialParameter] =
    useState<SubsetSpatialParameters>(DEFAULTCOORDINATES);
  const [rookOperation, setRookOperation] = useState<string>('average');

  const { notebookProperties, setNotebookProperties } =
    useContext(NotebookFeedbackCtx);

  const initiateProcessing = () => {
    const optionalTempParams =
      temporalParameters.start_time &&
      temporalParameters.stop_time &&
      temporalParameters;
    const optionalSpatParams =
      spatialParameters.minlat &&
      spatialParameters.minlon &&
      spatialParameters.maxlat &&
      spatialParameters.maxlon &&
      spatialParameters;
    const optionalRookOperation = rookOperation
      ? { operation: rookOperation }
      : null;
    const meta = rookOperation ? ROOKMETA : OPENDAPMETA;
    setSubsettingParameters({
      ...optionalTempParams,
      ...optionalSpatParams,
      ...optionalRookOperation,
      ...meta,
    });
    setSubsetFormOpen(false);
    setNotebookProperties({
      ...notebookProperties,
      processingStatus: Status.IDLE,
      redirectUrl: notebookProperties.redirectUrl,
      dialogOpen: true,
      output: OUTPUT.NOTEBOOK,
    });
  };

  return (
    <div style={classes.root}>
      <Dialog
        aria-labelledby='simple-dialog-title'
        open
        sx={{
          [`& .${dialogClasses.paper}`]: classes.dialogDownloadPaper,
        }}
        fullWidth
      >
        <DialogTitle id='simple-dialog-title' sx={classes.dialogTitle}>
          <Typography align='center'>Select Parameters</Typography>
          <IconButton
            aria-label='Close'
            sx={classes.closeButton}
            onClick={(): void => {
              setNotebookProperties(notebookDefaultProperties);
              setSubsetFormOpen(false);
            }}
          >
            <CloseIcon style={{ color: 'white' }} />
          </IconButton>
        </DialogTitle>
        <DialogContent sx={classes.dialogContent}>
          <ParameterSelector
            {...{
              temporalParameters,
              setTemporalParameter,
              spatialParameters,
              setSpatialParameter,
              subsettingMode,
              rookOperation,
              setRookOperation,
            }}
          />
        </DialogContent>
        <DialogActions sx={classes.dialogFooter}>
          <Button
            onClick={(): void => {
              setNotebookProperties(notebookDefaultProperties);
              setSubsetFormOpen(false);
            }}
            color='secondary'
          >
            Close
          </Button>
          <Button
            aria-haspopup='true'
            color='primary'
            variant='outlined'
            onClick={(): void => {
              initiateProcessing();
            }}
          >
            Process
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
