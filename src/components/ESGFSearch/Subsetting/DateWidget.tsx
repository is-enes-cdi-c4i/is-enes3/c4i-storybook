import React, { useState, useEffect } from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { Grid, TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { ESGFDataset } from '../../../types/searchTypes';
import { suggestTimeSpan } from '../../../utils/suggestTimeSpan';

const classes = {
  grid: {
    padding: '8px',
    textAlign: 'center',
  },
  timepicker: { maxWidth: '180px' },
  yearpicker: { maxWidth: '120px' },
};

enum PARAMETER {
  START = 'START',
  END = 'END',
}

type DateWidgetProp = {
  datasetsWithTimeSpan: ESGFDataset[];
  setTemporalParameter: ({
    start_time,
    stop_time,
  }: {
    start_time: string;
    stop_time: string;
  }) => void;
};

export const DateWidget = ({
  setTemporalParameter,
  datasetsWithTimeSpan,
}: DateWidgetProp): React.ReactElement => {
  const [advancedMode, advancedModeOn] = useState(false);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    advancedModeOn(event.target.checked);
  };

  // Suggest initial parameters and sync opendap parameter state
  const { proposedMinDate, proposedMaxDate } =
    suggestTimeSpan(datasetsWithTimeSpan);

  useEffect(() => {
    setTemporalParameter({
      start_time: proposedMinDate.replace(/-/g, ''),
      stop_time: proposedMaxDate.replace(/-/g, ''),
    });
  }, [proposedMinDate, proposedMaxDate]);
  /**
   * Date pickers require you to have a local state (that changes with selection) in date format
   * API call requires to have date as a string, this uses the setTemporalParameter hook from props.
   * Note that any change handler MUST be of type (date: Date) => void;
   */

  const proposeDateToPicker = () => {
    const proposedStartDate = new Date(proposedMinDate.slice(0, 10));
    const proposedEndDate = new Date(proposedMaxDate.slice(0, 10));
    return { proposedStartDate, proposedEndDate };
  };
  const proposeYearToPicker = () => {
    const proposedStartYear = new Date(`${proposedMinDate.slice(0, 4)}-01-01`);
    const proposedEndYear = new Date(`${proposedMaxDate.slice(0, 4)}-12-31`);
    return { proposedStartYear, proposedEndYear };
  };

  // Month and day are set fixed and cannot be changed:
  const [startYear, setLocalStartYear] = useState(
    proposeYearToPicker().proposedStartYear
  );
  const [endYear, setLocalEndYear] = useState(
    proposeYearToPicker().proposedEndYear
  );

  // Full control over day, month, year:
  const [startDate, setLocalStartDate] = useState(
    proposeDateToPicker().proposedStartDate
  );
  const [endDate, setLocalEndDate] = useState(
    proposeDateToPicker().proposedEndDate
  );

  const parseFullDate = (value: Date): string => {
    if (!value) {
      return null;
    }
    const day = `0${value.getDate()}`.slice(-2);
    const month = `0${value.getMonth() + 1}`.slice(-2);
    const year = value.getFullYear();
    return `${year}${month}${day}`;
  };

  const updateParameterState = (
    startDateString: string,
    endDateString: string
  ): void =>
    setTemporalParameter({
      start_time: startDateString.replace(/-/g, ''),
      stop_time: endDateString.replace(/-/g, ''),
    });

  const handleDateChange = (date: Date, parameter: PARAMETER) => {
    if (parameter === PARAMETER.START && date) {
      updateParameterState(parseFullDate(date), parseFullDate(endDate));
      setLocalStartDate(new Date(date));
    }

    if (parameter === PARAMETER.END && date) {
      updateParameterState(parseFullDate(startDate), parseFullDate(date));
      setLocalEndDate(new Date(date));
    }
  };

  const handleYearChange = (date: Date, parameter: PARAMETER) => {
    if (parameter === PARAMETER.START && date) {
      const startDateString = `${date.getFullYear()}-01-01`;
      updateParameterState(startDateString, parseFullDate(endYear));
      setLocalStartYear(new Date(date));
    }
    if (parameter === PARAMETER.END && date) {
      const endDateString = `${date.getFullYear()}-12-31`;
      updateParameterState(parseFullDate(startYear), endDateString);
      setLocalEndYear(new Date(date));
    }
  };

  return (
    <Grid container sx={{ justifyContent: 'space-around' }} item xs={12}>
      <Grid
        component='label'
        container
        alignItems='center'
        xs={12}
        sx={classes.grid}
      >
        <FormControlLabel
          style={{ padding: '5px 0' }}
          control={
            <Switch
              color='primary'
              checked={advancedMode}
              onChange={handleChange}
              name='advancedMode'
            />
          }
          label='Advanced'
        />
      </Grid>
      {advancedMode ? (
        <>
          <Grid item xs={6} sx={classes.grid}>
            <DatePicker
              label='Start date'
              value={startDate}
              inputFormat='dd/MM/yyyy'
              toolbarPlaceholder='01/01/2020'
              minDate={new Date('0000-01-01')}
              maxDate={new Date('5000-12-31')}
              onChange={(date: Date) => handleDateChange(date, PARAMETER.START)}
              views={['year', 'month', 'day']}
              openTo='year'
              renderInput={(params) => (
                <TextField
                  sx={classes.timepicker}
                  {...params}
                  required
                  variant='outlined'
                />
              )}
            />
          </Grid>
          <Grid item xs={6} sx={classes.grid}>
            <DatePicker
              label='End date'
              value={endDate}
              inputFormat='dd/MM/yyyy'
              toolbarPlaceholder='31/12/2020'
              minDate={new Date('0000-01-01')}
              maxDate={new Date('5000-12-31')}
              onChange={(date: Date) => handleDateChange(date, PARAMETER.END)}
              views={['year', 'month', 'day']}
              openTo='year'
              renderInput={(params) => (
                <TextField
                  sx={classes.timepicker}
                  {...params}
                  required
                  variant='outlined'
                />
              )}
            />
          </Grid>
        </>
      ) : (
        <>
          <Grid item xs={6} sx={classes.grid}>
            <DatePicker
              label='Start year'
              value={startYear}
              minDate={new Date('0000-01-01')}
              maxDate={new Date('5000-12-31')}
              onChange={(date: Date) => handleYearChange(date, PARAMETER.START)}
              views={['year']}
              renderInput={(params) => (
                <TextField
                  sx={classes.yearpicker}
                  {...params}
                  required
                  variant='outlined'
                />
              )}
            />
          </Grid>
          <Grid item xs={6} sx={classes.grid}>
            <DatePicker
              label='End year'
              value={endYear}
              minDate={new Date('0000-01-01')}
              maxDate={new Date('5000-12-31')}
              onChange={(date: Date) => handleYearChange(date, PARAMETER.END)}
              views={['year']}
              renderInput={(params) => (
                <TextField
                  sx={classes.yearpicker}
                  {...params}
                  required
                  variant='outlined'
                />
              )}
            />
          </Grid>
        </>
      )}
    </Grid>
  );
};
