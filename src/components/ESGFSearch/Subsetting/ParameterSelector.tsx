import React, { useContext } from 'react';
import {
  Typography,
  Grid,
  Accordion,
  AccordionDetails,
  Switch,
  AccordionSummary,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import EventNoteIcon from '@mui/icons-material/EventNote';
import ExploreIcon from '@mui/icons-material/Explore';
import SettingsIcon from '@mui/icons-material/Settings';
import { DatasetSelectionCtx } from '../../App/ApplicationContext';
import { DateWidget } from './DateWidget';
import { CoordinateWidget } from './CoordinateWidget';
import {
  NoTimespanAlert,
  SomeTimespanAlert,
  ParameterUnsetAlert,
  SubsettingAlert,
} from '../../GenericUI/Alerts';
import {
  CombinedParameterHooks,
  NOCOORDINATES,
  NODATE,
  DEFAULTCOORDINATES,
} from '../../../types/dataTypes';
import { ESGFDataset, SUBSETTING } from '../../../types/searchTypes';

const classes = {
  root: {
    width: '100%',
    minHeight: '50vh',
  },
  heading: {
    minWidth: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  description: {
    color: 'rgba(0, 0, 0, 0.6)',
    textAlign: 'center',
    paddingBottom: '30px',
    marginTop: '-20px',
  },
  title: {
    fontSize: '18px',
    marginLeft: '8px',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  checkboxLabel: {
    position: 'absolute',
    left: '10px',
    top: '10px',
  },
  textFieldWrapper: { display: 'flex', justifyContent: 'center' },
};

export const ParameterSelector = ({
  temporalParameters,
  setTemporalParameter,
  spatialParameters,
  setSpatialParameter,
  subsettingMode,
  rookOperation,
  setRookOperation,
}: CombinedParameterHooks & {
  subsettingMode: SUBSETTING;
}): React.ReactElement => {
  const [spatialParametersEnabled, setSpatialParametersEnabled] =
    React.useState(true);
  const [temporalParametersEnabled, setTemporalParametersEnabled] =
    React.useState(true);

  const toggleSpatial = () => {
    if (spatialParametersEnabled) {
      setSpatialParametersEnabled(false);
      setSpatialParameter(NOCOORDINATES);
    } else {
      setSpatialParametersEnabled(true);
      setSpatialParameter(DEFAULTCOORDINATES);
    }
  };
  const toggleTemporal = () => {
    if (temporalParametersEnabled) {
      setTemporalParametersEnabled(false);
      setTemporalParameter(NODATE);
    } else {
      setTemporalParametersEnabled(true);
      setTemporalParameter(temporalParameters);
    }
  };

  const { datasetCollection } = useContext(DatasetSelectionCtx);

  const datasetsWithTimeSpan = datasetCollection
    .filter((dataset) => dataset.startTime && dataset.endTime)
    .reduce(
      (withTimeSpan: ESGFDataset[], dataset) => withTimeSpan.concat(dataset),
      []
    );

  const allDatasetsHaveTimespan =
    datasetsWithTimeSpan.length - datasetCollection.length === 0;
  const timeSpanAvailable = datasetsWithTimeSpan.length > 0;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRookOperation((event.target as HTMLInputElement).value);
  };

  return (
    <div style={classes.root}>
      <SubsettingAlert />
      {subsettingMode === SUBSETTING.ROOK && (
        <Accordion key='facetSelection' expanded onChange={null}>
          <AccordionSummary
            expandIcon={null}
            aria-controls='facetSelection'
            id='facetSelection'
          >
            <Typography sx={classes.heading}>
              <SettingsIcon />
              <div style={classes.title}>Operation Selection</div>
            </Typography>
          </AccordionSummary>
          <AccordionDetails sx={classes.details}>
            <Grid item xs={12}>
              <Typography sx={classes.description}>
                Operation to be used for rook subsetting
              </Typography>
            </Grid>
            <div style={classes.textFieldWrapper}>
              <FormControl component='fieldset'>
                <RadioGroup
                  aria-label='operation'
                  name='operation-selection'
                  value={rookOperation}
                  onChange={handleChange}
                  row
                >
                  <FormControlLabel
                    value='average'
                    control={<Radio color='primary' />}
                    label='Average'
                  />
                  <FormControlLabel
                    value='noop'
                    control={<Radio color='primary' />}
                    label='No Op'
                  />
                </RadioGroup>
              </FormControl>
            </div>
          </AccordionDetails>
        </Accordion>
      )}

      <Accordion key='spatialParameters' expanded onChange={null}>
        <AccordionSummary
          expandIcon={null}
          aria-controls='spatialParameters'
          id='spatialParameters'
        >
          <Typography sx={classes.heading}>
            <Switch
              checked={spatialParametersEnabled}
              onChange={toggleSpatial}
              color='primary'
              name='spatialParameters'
              inputProps={{ 'aria-label': 'primary checkbox' }}
              sx={classes.checkboxLabel}
            />
            <ExploreIcon />
            <div style={classes.title}>Spatial Parameters</div>
          </Typography>
        </AccordionSummary>
        <AccordionDetails sx={classes.details}>
          <Grid item xs={12}>
            <Typography sx={classes.description}>
              Coordinates of the bounding box in lat / lon format
            </Typography>
          </Grid>
          {spatialParametersEnabled && (
            <CoordinateWidget
              spatialParameters={spatialParameters}
              setSpatialParameter={setSpatialParameter}
            />
          )}
          {!spatialParametersEnabled && <ParameterUnsetAlert />}
        </AccordionDetails>
      </Accordion>
      <Accordion key='temporalParameters' expanded onChange={null}>
        <AccordionSummary
          expandIcon={null}
          aria-controls='temporalParameters'
          id='temporalParameters'
        >
          <Typography sx={classes.heading}>
            <Switch
              checked={temporalParametersEnabled}
              onChange={toggleTemporal}
              color='primary'
              name='temporalParametersEnabled'
              inputProps={{ 'aria-label': 'primary checkbox' }}
              sx={classes.checkboxLabel}
            />
            <EventNoteIcon />
            <div style={classes.title}>Temporal Parameters</div>
          </Typography>
        </AccordionSummary>
        <AccordionDetails sx={classes.details}>
          <Grid item xs={12}>
            <Typography sx={classes.description}>
              Full years or specific dates in advanced mode
            </Typography>
          </Grid>
          {/* eslint-disable-next-line no-nested-ternary */}
          {timeSpanAvailable && temporalParametersEnabled ? (
            <>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateWidget
                  setTemporalParameter={setTemporalParameter}
                  datasetsWithTimeSpan={datasetsWithTimeSpan}
                />
              </LocalizationProvider>
              {allDatasetsHaveTimespan ? <div /> : <SomeTimespanAlert />}
            </>
          ) : temporalParametersEnabled ? (
            <NoTimespanAlert />
          ) : (
            <ParameterUnsetAlert />
          )}
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
