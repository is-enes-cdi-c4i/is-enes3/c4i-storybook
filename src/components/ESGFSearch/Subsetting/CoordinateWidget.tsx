import React, { useState } from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import {
  Grid,
  TextField,
  Typography,
  List,
  ListItem,
  Link,
} from '@mui/material';
import { SpatialParameterHook } from '../../../types/dataTypes';

const classes = {
  grid: {
    padding: '8px',
    textAlign: 'center',
  },
  description: {
    color: 'rgba(0, 0, 0, 0.6)',
    textAlign: 'center',
    paddingBottom: '30px',
    marginTop: '-20px',
  },
};

type Event = {
  target: {
    id: string;
    value: string;
  };
};

type FieldErrors = {
  minlat?: string;
  maxlat?: string;
  minlon?: string;
  maxlon?: string;
};

const isNumber = (input: string) => /^-?\d+(\.\d+)?$/.test(input);

export const CoordinateWidget = ({
  spatialParameters,
  setSpatialParameter,
}: SpatialParameterHook): React.ReactElement => {
  const [interactiveMode, interactiveModeOn] = useState(false);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    interactiveModeOn(event.target.checked);
  };
  // Add some timeout before setting state in the inputs
  const changeParameter = (event: Event, parameters = spatialParameters) => {
    if (event.target.id === 'bbox') {
      const newParameters = {
        minlat: '',
        maxlat: '',
        minlon: '',
        maxlon: '',
      };
      [
        newParameters.minlat = '',
        newParameters.minlon = '',
        newParameters.maxlat = '',
        newParameters.maxlon = '',
      ] = event.target.value.split(',');
      setSpatialParameter(newParameters);
    } else {
      setSpatialParameter({
        ...parameters,
        [event.target.id]: event.target.value,
      });
    }
  };

  const fieldErrors: FieldErrors = {};

  Object.keys(spatialParameters).forEach((key) => {
    if (!spatialParameters[key]) {
      fieldErrors[key] = 'This is a required field';
      return;
    }
    if (!isNumber(spatialParameters[key] as string)) {
      fieldErrors[key] = 'Must be a number';
      return;
    }
    if (key === 'minlat' || key === 'maxlat') {
      if (Math.abs(Number(spatialParameters[key])) > 90) {
        fieldErrors[key] = 'Value has to be between -90 and 90';
      }
    }
    if (key === 'minlon' || key === 'maxlon') {
      if (Math.abs(Number(spatialParameters[key])) > 180) {
        fieldErrors[key] = 'Value has to be between -180 and 180';
      }
    }
  });

  return (
    <Grid container sx={{ justifyContent: 'space-around' }} item xs={12}>
      <Grid
        component='label'
        container
        alignItems='center'
        xs={12}
        sx={classes.grid}
      >
        <FormControlLabel
          style={{ padding: '5px 0' }}
          control={
            <Switch
              color='primary'
              checked={interactiveMode}
              onChange={handleChange}
              name='interactiveMode'
            />
          }
          label='Interactive'
        />
      </Grid>
      {interactiveMode ? (
        <>
          <Grid item xs={4} />
          <Grid item xs={5}>
            <Typography sx={classes.description}>
              <List dense>
                <ListItem>
                  - Go to&nbsp;
                  <Link
                    href='http://bboxfinder.com/'
                    target='_blank'
                    rel='noopener'
                  >
                    http://bboxfinder.com/
                  </Link>
                  .
                </ListItem>
                <ListItem>
                  - Select &quot;Lat / Lng&quot; on the bottom right.
                </ListItem>
                <ListItem>- Draw a rectangle.</ListItem>
                <ListItem>
                  - Copy paste the coordinates behind &quot;Box&quot; on the
                  bottom left below.
                </ListItem>
              </List>
            </Typography>
          </Grid>
          <Grid item xs={3} />
          <Grid item xs={12} sx={classes.grid}>
            <TextField
              id='bbox'
              label='Bounding box'
              variant='outlined'
              value={`${spatialParameters.minlat},${spatialParameters.minlon},${spatialParameters.maxlat},${spatialParameters.maxlon}`}
              error={
                !!fieldErrors.minlat ||
                !!fieldErrors.minlon ||
                !!fieldErrors.maxlat ||
                !!fieldErrors.maxlon
              }
              helperText={
                (!!fieldErrors.minlat ||
                  !!fieldErrors.minlon ||
                  !!fieldErrors.maxlat ||
                  !!fieldErrors.maxlon) &&
                'Wrongly formatted bbox string.'
              }
              onChange={changeParameter}
              required
            />
          </Grid>
        </>
      ) : (
        <>
          <Grid item xs={6} sx={classes.grid}>
            <TextField
              id='minlat'
              label='Min. Latitude'
              variant='outlined'
              value={spatialParameters.minlat}
              error={!!fieldErrors.minlat}
              helperText={fieldErrors.minlat}
              onChange={changeParameter}
              required
            />
          </Grid>
          <Grid item xs={6} sx={classes.grid}>
            <TextField
              id='maxlat'
              label='Max. Latitude'
              variant='outlined'
              value={spatialParameters.maxlat}
              error={!!fieldErrors.maxlat}
              helperText={fieldErrors.maxlat}
              onChange={changeParameter}
              required
            />
          </Grid>
          <Grid item xs={6} sx={classes.grid}>
            <TextField
              id='minlon'
              label='Min. Longitude'
              variant='outlined'
              value={spatialParameters.minlon}
              error={!!fieldErrors.minlon}
              helperText={fieldErrors.minlon}
              onChange={changeParameter}
              required
            />
          </Grid>
          <Grid item xs={6} sx={classes.grid}>
            <TextField
              id='maxlon'
              label='Max. Longitude'
              variant='outlined'
              value={spatialParameters.maxlon}
              error={!!fieldErrors.maxlon}
              helperText={fieldErrors.maxlon}
              onChange={changeParameter}
              required
            />
          </Grid>
        </>
      )}
    </Grid>
  );
};
