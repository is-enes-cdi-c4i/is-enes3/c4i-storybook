/* eslint-disable @typescript-eslint/explicit-function-return-type */
const configDefault = {};

export const getConfig = () => {
  let c;
  if (typeof config === 'undefined') {
    // eslint-disable-next-line no-undef
    c = Object.assign({}, configDefault);
  } else {
    // eslint-disable-next-line no-undef
    c = Object.assign({}, configDefault, config || {});
  }
  return c;
};
