import { ESGFDataset } from '../types/searchTypes';

export type TimeRange = {
  proposedMinDate: string;
  proposedMaxDate: string;
};

const sortByStartTime = (a: ESGFDataset, b: ESGFDataset): number =>
  // eslint-disable-next-line no-nested-ternary
  Date.parse(a.startTime) > Date.parse(b.startTime)
    ? 1
    : Date.parse(a.startTime) < Date.parse(b.startTime)
    ? -1
    : 0;

const sortByEndTime = (a: ESGFDataset, b: ESGFDataset): number =>
  // eslint-disable-next-line no-nested-ternary
  Date.parse(a.endTime) < Date.parse(b.endTime)
    ? 1
    : Date.parse(a.endTime) > Date.parse(b.endTime)
    ? -1
    : 0;

const suggestDate = (datasets: ESGFDataset[], sortBy: string): string => {
  if (datasets.length === 0) return '';
  const target =
    sortBy === 'START'
      ? datasets.sort(sortByStartTime)[0].startTime
      : datasets.sort(sortByEndTime)[0].endTime;
  return target.slice(0, 10);
};

export const suggestTimeSpan = (datasets: ESGFDataset[]): TimeRange => {
  const proposedMinDate = suggestDate(datasets, 'START');
  const proposedMaxDate = suggestDate(datasets, 'END');

  return { proposedMinDate, proposedMaxDate };
};
