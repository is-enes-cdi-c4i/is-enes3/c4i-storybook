type DOIDataType = {
  format: string;
  value: {
    handle: string;
    index: number;
    permissions: string;
  };
};

type DOIValues = {
  index: number;
  type: string;
  data: {
    format: string;
    value: DOIDataType | string;
  };
  ttl: number;
  timestamp: string;
};

type DOIResponse = {
  responseCode: string;
  handle: string;
  values: DOIValues[];
};

type PartialJsonResponse = {
  identifier: {
    identifierType: string;
    id: string;
  };
};

const resolveDOIHandle = async (handle: string): Promise<string> => {
  // Use the doi.org api to resolve the handle and get a doi object, containing a link to the dataset description.
  const url = `https://doi.org/api/handles/${handle}`;

  const response = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => res.text());

  return response;
};

const retrieveDOIHandle = async (
  citationUrls: string[],
  instanceId: string
): Promise<string> => {
  // Get a DOI handle from the dataset using the DKRZ CERA service (or a link from citationsUrls as fallback)
  const doiURL = `https://www.wdc-climate.de/ui/cerarest/exportcmip6/?input=${instanceId}&wt=json`;
  const url = citationUrls.filter((urlEl) =>
    urlEl.includes('www.wdc-climate.de')
  )
    ? doiURL
    : citationUrls[0];

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const response: PartialJsonResponse = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => res.json());

  return response.identifier.id;
};

export const getDOILink = async (
  citationUrls: string[],
  instanceId: string
): Promise<string> => {
  const handle = await retrieveDOIHandle(citationUrls, instanceId);
  if (handle) {
    const doiResponse = await resolveDOIHandle(handle);
    try {
      const doiObject = JSON.parse(doiResponse) as DOIResponse;
      const res = doiObject.values[1].data.value as string;
      return res;
    } catch (error) {
      console.log(error);
    }
  }
  return null;
};
