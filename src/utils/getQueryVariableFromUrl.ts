/**
 * Get the url parameter by key. Returns the url parameter when found or null when not found
 * @param key The key to search for.
 */
export const getQueryParameter = (key: string): string => {
  const query = window.location.search.substring(1);
  const vars = query.split('&');
  for (let i = 0; i < vars.length; i += 1) {
    const pair = vars[i].split('=');
    if (pair[0] === key) {
      return pair[1];
    }
  }
  return null;
};
