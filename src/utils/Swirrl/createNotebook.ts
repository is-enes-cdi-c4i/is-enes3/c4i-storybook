/* eslint-disable no-await-in-loop */
import { PollUntil } from 'poll-until-promise';
import { generateSwirrlJson } from './createSwirrlJson';
import { generateSubsettingJson } from './createOpendapJson';
import { ESGFFileList } from '../getFacetsInfo';
import {
  Status,
  NotebookProperties,
  Operation,
  OPERATION,
  SUBSETTING,
  ESGFDataset,
} from '../../types/searchTypes';
import { BASEURL } from '../../components/App/ApplicationConfig';
import {
  createNotebook,
  deleteStaleNotebook,
  checkSnapshotExists,
  deleteStaleSession,
  runWorkflow,
} from './swirrlApiMethods';
import { OpenidUser, SubsetParameters } from '../../types/dataTypes';

// No types available for poll-until-promise, so these rules need to be disabled:
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */

type SwirrlNotebook = {
  id: string;
  userRequestedLibraries: string[];
} & SwirrlAddress;

type SwirrlAddress = {
  sessionId: string;
  serviceURL: string;
};

const throttleRequests = (ms: number) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

const generateMessage = (
  hasSnapshot: boolean,
  oldSessionId: string
): string => {
  if (oldSessionId) {
    if (hasSnapshot) {
      return 'SWIRRL has deleted your old notebook. All associated data will be kept for use in created snapshot.';
    }
    return 'SWIRRL has deleted your old notebook and all associated data.';
  }
  return null;
};

export const handleCreateNotebook = (
  setNotebookProperties: (newState: NotebookProperties) => void,
  notebookProperties: NotebookProperties,
  operationInUse: Operation,
  fileListObject: ESGFFileList[],
  user: OpenidUser,
  setUser: (user: OpenidUser) => void,
  datasetCollection?: ESGFDataset[],
  subsettingParameters?: SubsetParameters,
  subsettingMode?: SUBSETTING
): void => {
  let notebookURL = '';
  const statusCodes = [];

  setNotebookProperties({
    ...notebookProperties,
    processingStatus: Status.GENERATING,
    redirectUrl: '',
  });

  if (user && user.notebookId && user.sessionId) {
    deleteStaleNotebook(user.notebookId)
      .then(async () => {
        const hasSnapshot = await checkSnapshotExists(user.sessionId);
        await deleteStaleSession(user.sessionId, hasSnapshot);
      })
      .catch((e) => console.log(e));
  }

  createNotebook()
    .then((createNotebookResponse) => {
      const notebook: SwirrlNotebook = JSON.parse(
        createNotebookResponse
      ) as SwirrlNotebook;

      if (notebook) {
        notebookURL = notebook.serviceURL;
        const oldSessionId = notebook.sessionId;

        const payloads =
          operationInUse === OPERATION.SUBSETTING
            ? generateSubsettingJson(
                fileListObject,
                datasetCollection,
                subsettingParameters,
                notebook.sessionId,
                subsettingMode
              )
            : generateSwirrlJson(fileListObject, notebook.sessionId);

        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const pollUntilPromise = new PollUntil();

        pollUntilPromise
          .stopAfter(10 * 60 * 1000)
          .tryEvery(3000)
          .execute(async () => {
            if (
              (await fetch(`${BASEURL}/notebook/${notebook.id}`, {
                method: 'GET',
                mode: 'cors',
                credentials: 'include',
                referrerPolicy: 'no-referrer-when-downgrade',
              }).then((response) => response.status)) === 200
            ) {
              // Update React State to have the new notebook:
              setUser({
                ...user,
                notebookId: notebook.id,
                notebookURL: notebook.serviceURL,
                sessionId: notebook.sessionId,
              });
              return true;
            }
            return false;
          })
          .then(async () => {
            // eslint-disable-next-line no-restricted-syntax
            for (const payload of payloads) {
              await throttleRequests(15000);
              const response = await runWorkflow(
                payload,
                operationInUse,
                subsettingMode
              );
              statusCodes.push(response);
              if (response !== 200) {
                console.error(
                  `Request failed with ${response} on: ${JSON.stringify(
                    payload
                  )}`
                );
              }
            }
          })
          .then(async () => {
            const hasSnapshot = await checkSnapshotExists(user.sessionId);

            if (statusCodes.every((statusCode) => statusCode === 200)) {
              setNotebookProperties({
                ...notebookProperties,
                processingStatus: Status.READY,
                redirectUrl: notebookURL,
                message: generateMessage(hasSnapshot, oldSessionId),
                workerNumber: payloads.length || 0,
              });
            } else {
              setNotebookProperties({
                ...notebookProperties,
                processingStatus: Status.ERROR,
                redirectUrl: '',
                error: ' (One or more workflows could not be started)',
              });
            }
          })
          .catch((err) => console.error(err));
      }
    })
    .catch((err) => {
      setNotebookProperties({
        ...notebookProperties,
        processingStatus: Status.ERROR,
        redirectUrl: '',
        error: '(SWIRRL url not reachable)',
      });
      console.error(err);
    });
};

export const handleResumeNotebook = (
  setNotebookProperties: (newState: NotebookProperties) => void,
  notebookProperties: NotebookProperties,
  operationInUse: Operation,
  fileListObject: ESGFFileList[],
  user: OpenidUser,
  datasetCollection?: ESGFDataset[],
  subsettingParameters?: SubsetParameters,
  subsettingMode?: SUBSETTING
): void => {
  const { sessionId } = user;
  const serviceURL = user.notebookURL;

  const payloads =
    operationInUse === OPERATION.SUBSETTING
      ? generateSubsettingJson(
          fileListObject,
          datasetCollection,
          subsettingParameters,
          sessionId,
          subsettingMode
        )
      : generateSwirrlJson(fileListObject, sessionId);

  const statusCodes = [];

  setNotebookProperties({
    ...notebookProperties,
    processingStatus: Status.GENERATING,
    redirectUrl: serviceURL,
  });

  Promise.resolve(
    JSON.stringify({
      sessionId,
      serviceURL,
    })
  )
    .then(async () => {
      // eslint-disable-next-line no-restricted-syntax
      for (const payload of payloads) {
        await throttleRequests(5000);
        const response = await runWorkflow(
          payload,
          operationInUse,
          subsettingMode
        );
        statusCodes.push(response);
        if (response !== 200) {
          console.error(
            `Request failed with ${response} on: ${JSON.stringify(payload)}`
          );
        }
      }
    })
    .then(() =>
      statusCodes.some((statusCode) => statusCode === 200)
        ? setNotebookProperties({
            ...notebookProperties,
            processingStatus: Status.READY,
            redirectUrl: serviceURL,
            workerNumber: payloads.length || 0,
          })
        : setNotebookProperties({
            ...notebookProperties,
            processingStatus: Status.ERROR,
            redirectUrl: '',
            error: ' (One or more workflows could not be started)',
          })
    )
    .catch((err) => {
      setNotebookProperties({
        ...notebookProperties,
        processingStatus: Status.ERROR,
        redirectUrl: '',
        error: '(SWIRRL url not reachable)',
      });
      console.error(err);
    });
};
