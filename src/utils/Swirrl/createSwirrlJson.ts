import YAML from 'yaml';
import { ESGFFileList } from '../getFacetsInfo';
import { FILELIMIT } from '../../components/App/ApplicationConfig';

export type SwirrlPayload = {
  sessionId: string;
  inputs: string;
};

/**
 * Generate a XML Metalink document based on the selectedData
 * @param selectedData An array of ESGFFileList
 */
export const generateSwirrlJson = (
  selectedData: ESGFFileList[],
  sessionId: string
): SwirrlPayload[] => {
  const urlListCollection = selectedData
    .filter((data) => data.fileList.length > 0)
    .map((data) =>
      data.fileList.map((item) => {
        const { url, title } = item;

        return url && item ? { url, filename: title } : {};
      })
    );

  const swirllJsonCollection = urlListCollection.map((urlList) => {
    const yamlObj =
      FILELIMIT && typeof FILELIMIT === 'number'
        ? { links: urlList.splice(0, FILELIMIT) }
        : { links: urlList };

    const b64EncodedFileList = btoa(YAML.stringify(yamlObj)) || '';

    const payloadObj = {
      sessionId,
      inputs: b64EncodedFileList,
    };
    return payloadObj as SwirrlPayload;
  });
  return swirllJsonCollection;
};
