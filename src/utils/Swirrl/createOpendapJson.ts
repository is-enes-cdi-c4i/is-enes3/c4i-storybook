import YAML from 'yaml';
import { ESGFFileList } from '../getFacetsInfo';
import { SwirrlPayload } from './createSwirrlJson';
import { SubsetParameters } from '../../types/dataTypes';
import { ESGFDataset, SUBSETTING } from '../../types/searchTypes';

/**
 * Generate a XML Metalink document based on the selectedData
 * @param selectedData An array of ESGFFileList
 */
export const generateSubsettingJson = (
  selectedData: ESGFFileList[],
  datasetCollection: ESGFDataset[],
  parameters: SubsetParameters,
  sessionId: string,
  subsettingMode: SUBSETTING
): SwirrlPayload[] => {
  if (subsettingMode === SUBSETTING.OPENDAP) {
    const chunkedPayloads = selectedData
      .filter((data) => data.fileList.length > 0)
      .map((data) => ({
        files: data.fileList.map(
          (item, i) => item.url + (i === data.fileList.length - 1 ? '' : '\\n')
        ),
      }));

    const opendapJsonCollection = chunkedPayloads.map((payload) => {
      const yamlObj = { ...parameters, links: payload.files };

      const b64EncodedFileList = btoa(YAML.stringify(yamlObj)) || '';
      const payloadObj = {
        sessionId,
        inputs: b64EncodedFileList,
      };
      return payloadObj;
    });
    return opendapJsonCollection;
  }

  if (subsettingMode === SUBSETTING.ROOK) {
    const rookJsonCollection = datasetCollection.map((payload) => {
      const start = parameters.start_time;
      const stop = parameters.stop_time;

      const subsettingParameters = {
        ...parameters,
        start_time: `${start.substring(0, 4)}-${start.substring(
          4,
          6
        )}-${start.substring(6, 8)}`,
        stop_time: `${stop.substring(0, 4)}-${stop.substring(
          4,
          6
        )}-${stop.substring(6, 8)}`,
      };

      let rookurl = '';
      if (
        payload.dataNode === 'esgf1.dkrz.de' ||
        payload.dataNode === 'esgf3.dkrz.de'
      ) {
        rookurl = 'http://rook.dkrz.de/wps';
      } else if (payload.dataNode === 'esgf-node2.cmcc.it') {
        rookurl = 'https://ddshub.cmcc.it/wps';
      } else {
        return null;
      }

      const yamlObj = {
        ...subsettingParameters,
        datasetid: payload.instanceId,
        parameter: payload.variable[0] || 'NOT FOUND',
        rookurl,
      };
      const b64EncodedFileList = btoa(YAML.stringify(yamlObj)) || '';

      const payloadObj = {
        sessionId,
        inputs: b64EncodedFileList,
      };
      return payloadObj;
    });
    return rookJsonCollection;
  }
  return null;
};
