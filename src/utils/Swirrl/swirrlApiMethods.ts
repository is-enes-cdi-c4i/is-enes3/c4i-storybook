import { BASEURL } from '../../components/App/ApplicationConfig';
import { SwirrlPayload } from './createSwirrlJson';
import {
  Operation,
  OPERATION,
  Activity,
  Graph,
  SUBSETTING,
} from '../../types/searchTypes';

enum WFURL {
  DOWNLOAD = 'download',
  OPENDAP = 'opendap',
  ROOK = 'rookwps',
}

export const createNotebook = async (): Promise<string> => {
  const url = `${BASEURL}/notebook`;
  const payload = JSON.stringify({});

  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    referrerPolicy: 'no-referrer-when-downgrade',
    body: payload,
  }).then((res) => res.text());

  return response;
};

export const deleteStaleNotebook = async (
  notebookId: string
): Promise<Response> => {
  if (notebookId) {
    const url = `${BASEURL}/notebook/${notebookId}`;

    return fetch(url, {
      method: 'DELETE',
      mode: 'cors',
      credentials: 'include',
      referrerPolicy: 'no-referrer-when-downgrade',
    });
  }
  return null;
};

export const checkSnapshotExists = async (
  sessionId: string
): Promise<boolean> => {
  if (sessionId) {
    const activities: Activity[] = (await fetch(
      `${BASEURL}/provenance/session/${sessionId}/activities`,
      {
        credentials: 'include',
        mode: 'cors',
      }
    ).then((res) =>
      res.status === 200 ? res.json() : undefined
    )) as Activity[];
    if (activities) {
      return (activities['@graph'] as Graph[]).some(
        (graph) => graph['@type'].indexOf('swirrl:CreateSnapshot') > -1
      );
    }
  }
  return null;
};

export const deleteStaleSession = async (
  sessionId: string,
  hasSnapshot: boolean
): Promise<Response> => {
  const url = `${BASEURL}/session/${sessionId}`;

  if (sessionId && !hasSnapshot) {
    return fetch(url, {
      method: 'DELETE',
      mode: 'cors',
      credentials: 'include',
      referrerPolicy: 'no-referrer-when-downgrade',
    });
  }
  return null;
};

export const runWorkflow = async (
  payload: SwirrlPayload,
  operation: Operation,
  subsettingMode: SUBSETTING
): Promise<number> => {
  const wfurl =
    // eslint-disable-next-line no-nested-ternary
    operation === OPERATION.DOWNLOAD
      ? WFURL.DOWNLOAD
      : subsettingMode === SUBSETTING.OPENDAP
      ? WFURL.OPENDAP
      : WFURL.ROOK;
  const url = `${BASEURL}/workflow/${wfurl}/run/`;
  const body = JSON.stringify(payload);

  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    body,
  }).then((res) => res.status);

  return response;
};

export const getNotebook = async (notebookId: string): Promise<string> => {
  const url = `${BASEURL}/notebook/${notebookId}`;

  const response = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    referrerPolicy: 'no-referrer-when-downgrade',
  }).then((res) => res.text());

  return response;
};
