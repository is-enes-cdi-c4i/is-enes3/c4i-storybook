import { getNotebook } from './swirrlApiMethods';
import {
  Notebook,
  NotebookSizeProperties,
  Stage,
  Content,
} from '../../types/searchTypes';

const getStageSize = async (
  stage: string,
  serviceUrlToken: string[]
): Promise<number> => {
  const contents: Stage = (await fetch(
    `${serviceUrlToken[0]}/api/contents/data/staginghistory/${stage}?${serviceUrlToken[1]}`
  )
    .then((res) => res.json())
    .catch((e) => console.log(e))) as Stage;

  const fileinfo: Content = (await fetch(
    `${serviceUrlToken[0]}/api/contents/data/staginghistory/${stage}/swirrl_fileinfo.json?${serviceUrlToken[1]}`
  )
    .then((res) => res.json())
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    .then((data: { content: string }) => JSON.parse(data.content))
    .catch((err) => console.error(err))) as Content;

  const filenames = fileinfo.files
    .filter((file) => file.state !== 'unchanged')
    .map((file) => file.filename);

  return contents.content.reduce(
    (size, file) => (filenames.includes(file.name) && file.size) + size,
    0
  );
};

const getNotebookDataSize = async (serviceUrl: string): Promise<number> => {
  const serviceUrlToken = serviceUrl.split('?');
  const staginghistory: Stage = (await fetch(
    `${serviceUrlToken[0]}/api/contents/data/staginghistory?${serviceUrlToken[1]}`,
    {
      cache: 'no-store',
    }
  )
    .then((res) => (res.status === 200 ? res.json() : undefined))
    .catch((err) => console.error(err))) as Stage;
  if (staginghistory) {
    const stages = staginghistory.content.map((stage) => stage.name);
    let size = 0;
    while (stages.length > 0) {
      const sizePromises = stages
        .splice(0, 30)
        .map((stage) => getStageSize(stage, serviceUrlToken));
      // eslint-disable-next-line no-await-in-loop
      const sizes = await Promise.all<number>(sizePromises);
      // eslint-disable-next-line @typescript-eslint/no-loop-func
      sizes.forEach((s) => {
        size += s;
      });
    }
    return size;
  }
  return 0;
};

export const parseSize = (size: number): string => {
  if (size < 0) return '';
  if (size < 1000) return `${size} B`;
  let remainder = size;
  // eslint-disable-next-line no-restricted-syntax
  for (const prefix of 'kMGTPEZ'.split('')) {
    remainder /= 1000;
    if (remainder / 1000 < 1) {
      return `${+remainder.toFixed(2)} ${prefix}B`;
    }
  }
  return `${+remainder.toFixed(2)} YB`;
};

export const getNotebookInfo = async (
  notebookId: string,
  notebookURL: string
): Promise<NotebookSizeProperties> => {
  const currentSizePromise = getNotebookDataSize(notebookURL);
  const notebookPromise = getNotebook(notebookId);
  const currentSize = await currentSizePromise;
  const notebook: Notebook = JSON.parse(await notebookPromise) as Notebook;
  const currentStatus = notebook.notebookStatus;

  return {
    currentSize,
    currentStatus,
  };
};
