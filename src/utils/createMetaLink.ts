import { ESGFFileList, ESGFFileProperties } from './getFacetsInfo';

/**
 * Generate a XML Metalink document based on the filelist
 * @param filelist An array of ESGFFileList
 */
export const generateMetalink = (filelist: ESGFFileList[]): string => {
  let metalink = '<metalink xmlns="urn:ietf:params:xml:ns:metalink">\n';
  const allFiles = filelist.flatMap((dataset) => dataset.fileList);
  const reducer = (
    groups: { [key: string]: ESGFFileProperties[] },
    item: ESGFFileProperties
  ) => {
    const val = item.instanceId;
    // eslint-disable-next-line no-param-reassign
    groups[val] = groups[val] || [];
    groups[val].push(item);
    return groups;
  };
  const groups = allFiles.reduce(reducer, {});

  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const group in groups) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const first = groups[group][0];

    metalink += `\t<file name="${first.title}">\n\t\t<size>${first.size}</size>\n\t\t<hash type="${first.checksumType}">${first.checksum}</hash>\n`;

    let file: ESGFFileProperties;
    // eslint-disable-next-line no-restricted-syntax
    for (file of groups[group]) {
      metalink += `\t\t<url priority="1">${file.url}</url>\n`;
    }
    metalink += '\t</file>\n\n';
  }
  metalink += '</metalink>';
  return metalink;
};
