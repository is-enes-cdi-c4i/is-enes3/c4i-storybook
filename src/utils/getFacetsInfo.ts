import { getConfig } from './configReader';
import {
  SearchQuery,
  ProjectType,
  FacetDoc,
  DocumentDoc,
  FacetResponse,
  ESGFDataset,
  DatasetResponse,
  OPERATION,
} from '../types/searchTypes';
import { DATASETVERSION } from '../components/App/ApplicationConfig';

type Config = {
  esgfsearch: string;
};
const config: Config = getConfig();

export interface FacetResult {
  [key: string]: number;
}

export interface FacetInfo {
  [key: string]: FacetResult;
}

type FacetQueryObj = {
  url: string;
  facetId: string;
};
type SingleFacetData = {
  data: FacetInfo;
  facetId: string;
};

type Result = {
  facetId: string;
  data: {
    facet_counts: {
      [key: string]: number;
    };
    response: {
      [key: string]: number;
    };
    responseHeader: {
      [key: string]: number;
    };
  };
};

const buildNodeParameter = (selectedNodes: string[]) => {
  switch (selectedNodes.length) {
    case 0:
      return null;
    case 1:
      return `&data_node=${selectedNodes[0]}`;
    default:
      return `&data_node=${selectedNodes.join(',')}`;
  }
};

/** Remove "project" as it is fixed. Flatten all other. */
const generateFacetArray = (project: ProjectType): string[] =>
  project.mainFacets
    .map((facet) => facet.id)
    .filter((facetId) => facetId !== 'project');

/**
 * For a searchquery and project find all the options for the otherfacets and the number of results
 * @param searchQuery
 * @param project
 */
export const getFacetsInfo = (
  searchQuery: SearchQuery,
  project: ProjectType,
  selectedNodes: string[]
): Promise<FacetInfo> => {
  return new Promise((resolve, reject) => {
    const mainFacets = generateFacetArray(project);
    const esgfsearchurl = config.esgfsearch;
    const facets = mainFacets.join(',');
    const nodes = buildNodeParameter(selectedNodes);

    const mainFacetToQuery: FacetQueryObj[] = [];

    mainFacets.forEach((thisFacet) => {
      let url = `${esgfsearchurl}format=application%2Fsolr%2Bjson&facets=${facets}${nodes}&limit=0${DATASETVERSION}`;

      /**   Append to this facet's url parameters from the selection, only if they come from other facets.
            This ensures that further members of this facet can be selected and other main facets update on the selection. */
      const allOtherFacetsinSelection = searchQuery.selectedItems.filter(
        (facet) => facet.facetId !== thisFacet
      );
      allOtherFacetsinSelection.forEach((facet) => {
        url += `&${facet.facetId}=${facet.fieldId}`;
      });

      mainFacetToQuery.push({ url, facetId: thisFacet });
    });

    Promise.all(
      mainFacetToQuery.map((facet: FacetQueryObj): Promise<SingleFacetData> => {
        return fetch(facet.url)
          .then((res) => res.json())
          .then((data: FacetInfo) => ({
            data,
            facetId: facet.facetId,
          }));
      })
    )
      .then((mFacets: Result[]): void => {
        const facetInfo: FacetInfo = {};

        mFacets.forEach((mainFacet: Result) => {
          const facetResult: FacetResult = {};
          const facetToProcess = mainFacet.data.facet_counts.facet_fields[
            mainFacet.facetId
          ] as number[];

          if (!facetToProcess) return;

          // Array with key & count dissolved, e.g. ["1hr", 10, "3hrs", 20], hence:
          for (let j = 0; j < facetToProcess.length / 2; j += 1) {
            facetResult[facetToProcess[j * 2]] = facetToProcess[j * 2 + 1];
          }
          facetInfo[mainFacet.facetId] = facetResult;
        });
        resolve(facetInfo);
      })
      .catch((e) => {
        console.error(e);
        reject(e);
      });
  });
};

export interface SearchResultTypes {
  query: SearchQuery;
  results: ESGFDataset[];
}
/**
 * For a searchquery and project find all the search results
 * @param searchQuery
 * @param project
 */
export const getSearchResults = (
  searchQuery: SearchQuery,
  project: ProjectType,
  selectedNodes: string[],
  limit: number
): Promise<SearchResultTypes> => {
  return new Promise((resolve, reject) => {
    const allFacets = project.mainFacets;
    const facetsToQuery = allFacets.map((facet) => {
      return facet.id;
    });
    facetsToQuery.push('project');
    const facets = facetsToQuery.join(',');
    const nodes = buildNodeParameter(selectedNodes);

    const esgfsearchurl = config.esgfsearch;
    let urlToQuery = `${esgfsearchurl}format=application%2Fsolr%2Bjson&facets=${facets}${nodes}&limit=${limit}${DATASETVERSION}`;
    searchQuery.selectedItems.forEach((facet) => {
      urlToQuery += `&${facet.facetId}=${facet.fieldId}`;
    });

    fetch(urlToQuery)
      .then((res) => res.json())
      .then((result: FacetResponse) => {
        resolve({
          query: searchQuery,
          results: result.response.docs.map(
            (doc: FacetDoc): ESGFDataset => ({
              title: doc.title,
              id: doc.id,
              fileNumber: doc.number_of_files,
              version: doc.version,
              size: doc.size,
              citationUrl: doc.citation_url,
              instanceId: doc.instance_id,
              dataNode: doc.data_node,
              startTime: doc.datetime_start ? doc.datetime_start : '',
              endTime: doc.datetime_stop ? doc.datetime_stop : '',
              // eslint-disable-next-line no-nested-ternary
              variable: doc.variable
                ? doc.variable
                : doc.variable_id
                ? doc.variable_id
                : '',
            })
          ),
        });
      })
      .catch((e) => {
        console.error(e);
        reject(e);
      });
  });
};

export const promiseAllProgress = (
  listOfPromises: Promise<unknown>[],
  progressPercentageCallback: (progress: number) => number
): Promise<unknown> => {
  let d = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-expressions
  progressPercentageCallback && progressPercentageCallback(0);
  // eslint-disable-next-line no-restricted-syntax
  for (const p of listOfPromises) {
    // eslint-disable-next-line @typescript-eslint/no-loop-func
    p.then(() => {
      d += 1;
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      progressPercentageCallback &&
        progressPercentageCallback((d * 100) / listOfPromises.length);
    }).catch((err) => console.error(err));
  }

  return Promise.all(listOfPromises);
};

export interface ESGFFileProperties {
  title: string;
  instanceId: string;
  size: number;
  url: string;
  checksum: string;
  checksumType: string;
  replica: string;
  parent?: string;
  variable?: string;
}
export interface ESGFFileList {
  datasetId: string;
  variable?: string;
  fileList: ESGFFileProperties[];
}

export const filterFiles = (
  files: ESGFFileProperties[],
  variables: string[]
): ESGFFileProperties[] => {
  if (variables.length === 0) {
    return files;
  }
  const results = files.filter((file) =>
    variables.some((variable) =>
      // Mind the underscore here, to prevent false results, e.g. with tas vs tasmin
      file.title.slice(0, variable.length + 1).includes(`${variable}_`)
    )
  );
  return results;
};

/**
 * Returns the contents of an ESGF dataset, like the list of files.
 * @param dataset Dataset object with an id property containing the id of the dataset.
 */
export const resolveFilesOfDataset = (
  id: string,
  operation: OPERATION,
  selectedNodes: string[]
): Promise<ESGFFileList> => {
  return new Promise((resolve, reject) => {
    const esgfsearchurl = config.esgfsearch;
    const nodes = buildNodeParameter(selectedNodes);

    const urlToQuery = `${esgfsearchurl}format=application%2Fsolr%2Bjson&type=File&dataset_id=${id}&limit=1000${nodes}&${DATASETVERSION}`;

    fetch(urlToQuery)
      .then((res) => res.json())
      .then((result: DatasetResponse) => {
        const results = result.response.docs.map((doc: DocumentDoc) => {
          const urlHTTPServerIndex: number = doc.url.findIndex((url: string) =>
            url.includes(
              operation === OPERATION.SUBSETTING ? '|OPENDAP' : '|HTTPServer'
            )
          );
          let httpServerURL: string = null;
          if (urlHTTPServerIndex !== -1) {
            httpServerURL = doc.url[urlHTTPServerIndex].substring(
              0,
              doc.url[urlHTTPServerIndex].indexOf('|')
            );
            if (
              operation === OPERATION.SUBSETTING &&
              httpServerURL.substring(httpServerURL.length - 5) === '.html'
            ) {
              httpServerURL = httpServerURL.substring(
                0,
                httpServerURL.length - 5
              );
            }
          }

          const fileProps: ESGFFileProperties = {
            instanceId: doc.instance_id,
            size: doc.size,
            url: httpServerURL,
            checksum: doc.checksum[0],
            checksumType: doc.checksum_type[0],
            replica: doc.replica,
            title: doc.title,
            variable: doc.variable,
          };
          return fileProps;
        });

        const dataSetContents: ESGFFileList = {
          datasetId: id,
          fileList: results,
        };
        resolve(dataSetContents);
      })
      .catch((e) => {
        console.error(e);
        reject(e);
      });
  });
};

/**
 * Returns the contents of multiple ESGF datasets, like the list of files.
 * @param dataset Dataset object with an id property containing the id of the dataset.
 * @param progressPercentageCallback Callback which indicates how far the list is completed.
 */
export const getFilesForDatasets = (
  datasetIds: string[],
  operation: OPERATION,
  nodes: string[],
  progressPercentageCallback?: (progress: number) => void
): Promise<ESGFFileList[]> => {
  return new Promise((resolve, reject) => {
    promiseAllProgress(
      datasetIds.map((id) => resolveFilesOfDataset(id, operation, nodes)),
      progressPercentageCallback as (progress: number) => number
    )
      .then(resolve)
      .catch(reject);
  });
};
