FROM node:18-bullseye-slim

WORKDIR /c4i-storybook
ADD . /c4i-storybook

#RUN yarn audit --groups dependencies
RUN yarn --prod --frozen-lockfile
RUN yarn run build

FROM nginx
WORKDIR /usr/share/nginx/html/c4i-storybook/
COPY --from=0 /c4i-storybook/dist /usr/share/nginx/html/c4i-frontend/

COPY favicon.ico /usr/share/nginx/html/c4i-frontend/favicon.ico

COPY Docker/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

# Set the command to start the node server.
CMD echo "var config = { \
  \"baseUrl\": \"${BASE_URL}\", \
  \"esgfsearch\": \"${C4INGINXESGFSEARCH}\", \
  \"frontendContentURL\": \"${C4IDOCS}\", \
  \"frontendPath\": \"${C4IFRONTENDPATH}\" \
  }" > /usr/share/nginx/html/c4i-frontend/config.js \
  && echo "Starting NGINX" && nginx -g 'daemon off;'
